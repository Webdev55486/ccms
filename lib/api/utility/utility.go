/**
 * medfilm-ccms
 * Copyright (C) Sunstate AB - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Simon Wikstrand <simon@sunstate.se>, 2016-12-15
 */

package utility

import (
	"github.com/unrolled/render"
	"net/http"
)

type ErrorData struct {
	Status  int                    `json:"status"`
	Message string                 `json:"message"`
	Node    string                 `json:"node"`
	Data    map[string]interface{} `json:"data"`
}

type CustomData struct {
	Data map[string]interface{}
}

func SendError(err error, w http.ResponseWriter, req *http.Request) {
	CustomData{
		Data: map[string]interface{}{
			"errorMessage": err.Error(),
		},
	}.NotFound(w, req)
}

func (data CustomData) OK(w http.ResponseWriter, req *http.Request) {
	message := ErrorData{
		Status:  http.StatusOK,
		Message: "OK",
		Data:    data.Data,
	}

	r := render.New()

	r.JSON(w, http.StatusOK, message)
}

func (data CustomData) NotFound(w http.ResponseWriter, req *http.Request) {

	message := ErrorData{
		Status:  http.StatusNotFound,
		Message: "Not Found",
		Data:    data.Data,
	}

	r := render.New()

	r.JSON(w, http.StatusOK, message)
}

func (data CustomData) PreconditionFailed(w http.ResponseWriter, req *http.Request) {

	message := ErrorData{
		Status:  http.StatusPreconditionFailed,
		Message: "Precondition Failed",
		Data:    data.Data,
	}

	r := render.New()

	r.JSON(w, http.StatusOK, message)
}

func (data CustomData) Unauthorized(w http.ResponseWriter, req *http.Request) {

	message := ErrorData{
		Status:  http.StatusUnauthorized,
		Message: "Unauthorized",
		Data:    data.Data,
	}

	r := render.New()

	r.JSON(w, http.StatusOK, message)
}

func (data CustomData) MethodNotAllowed(w http.ResponseWriter, req *http.Request) {

	message := ErrorData{
		Status:  http.StatusMethodNotAllowed,
		Message: "Method Not Allowed",
		Data:    data.Data,
	}

	r := render.New()

	r.JSON(w, http.StatusOK, message)
}

func (data CustomData) InternalServerError(w http.ResponseWriter, req *http.Request) {

	message := ErrorData{
		Status:  http.StatusInternalServerError,
		Message: "Internal Server Error",
		Data:    data.Data,
	}

	r := render.New()

	r.JSON(w, http.StatusOK, message)
}
