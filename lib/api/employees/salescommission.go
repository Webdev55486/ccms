package employees

import (
	"database/sql"
	"net/http"

	"github.com/MedFilm/ccms/lib/api/utility"
	"github.com/MedFilm/ccms/lib/authentication"
	"github.com/MedFilm/ccms/lib/database"
	"github.com/MedFilm/ccms/lib/tools"
)

type salescommissionCountry struct {
	ID   int               `json:"id"`
	Name string            `json:"name"`
	Rows []salescommission `json:"rows"`
}
type paid struct {
	Kam    bool `json:"kam"`
	Assist bool `json:"assist"`
}
type salescommission struct {
	ID         int                 `json:"id"`
	CountryID  int                 `json:"country_id"`
	Date       database.NullString `json:"date"`
	Hospital   database.NullString `json:"hospital"`
	Items      database.NullString `json:"items"`
	OrderValue database.NullInt    `json:"order_value"`
	KAM        database.NullString `json:"kam"`
	Assist     database.NullString `json:"assist"`
	Paid       paid                `json:"paid"`
}

func (s *salescommissionCountry) getCommisions() error {
	var commissions []salescommission
	err := database.PerformQuery(`
		select
			id,
			date,
			hospital,
			items,
			order_value,
			kam,
			assist,
			paid_kam,
			paid_assist,
			country_id
		from
			salescommission
		where
			country_id=?
		order by
			createdAt desc
	`, func(rows *sql.Rows) {
		for rows.Next() {
			commission := salescommission{}
			rows.Scan(
				&commission.ID,
				&commission.Date,
				&commission.Hospital,
				&commission.Items,
				&commission.OrderValue,
				&commission.KAM,
				&commission.Assist,
				&commission.Paid.Kam,
				&commission.Paid.Assist,
				&commission.CountryID,
			)
			commissions = append(commissions, commission)
		}
	}, s.ID)
	if err != nil {
		return err
	}
	s.Rows = commissions
	return nil
}

// ReadSalesCommissions : Read all salescommissions
func ReadSalesCommissions(w http.ResponseWriter, r *http.Request) {
	permission := authentication.GetPermissions(r, "employees")
	if permission == 0 {
		utility.CustomData{}.Unauthorized(w, r)
		return
	}

	var countries []salescommissionCountry
	err := database.PerformQuery(`
		select
			id,
			name
		from
			salescommission_country
	`, func(rows *sql.Rows) {
		for rows.Next() {
			country := salescommissionCountry{}
			rows.Scan(&country.ID, &country.Name)
			err := country.getCommisions()
			if err != nil {
				utility.SendError(err, w, r)
				return
			}
			countries = append(countries, country)
		}
	})

	if err != nil {
		utility.SendError(err, w, r)
		return
	}
	utility.CustomData{Data: map[string]interface{}{
		"result": countries,
	}}.OK(w, r)
}

// CreateCountry : create new commissionable country
func CreateCountry(w http.ResponseWriter, r *http.Request) {
	permission := authentication.GetPermissions(r, "employees")
	if permission < 2 {
		utility.CustomData{}.Unauthorized(w, r)
		return
	}

	country := salescommissionCountry{}
	err := tools.DecodeJson(&country, r)
	if err != nil {
		utility.SendError(err, w, r)
		return
	}

	err = database.PerformQuery(`
		insert into salescommission_country(name)
		values(?)
	`, func(rows *sql.Rows) {}, country.Name)

	if err != nil {
		utility.SendError(err, w, r)
		return
	}

	utility.CustomData{}.OK(w, r)
}

// CreateSalesCommission : create new commission row
func CreateSalesCommission(w http.ResponseWriter, r *http.Request) {
	permission := authentication.GetPermissions(r, "employees")
	if permission < 2 {
		utility.CustomData{}.Unauthorized(w, r)
		return
	}
	commission := salescommission{}
	err := tools.DecodeJson(&commission, r)
	if err != nil {
		utility.SendError(err, w, r)
		return
	}
	err = database.PerformQuery(`
		insert into salescommission
			(date, hospital, items, order_value, kam, assist, paid_kam, paid_assist, country_id)
		values 
			(?, ?, ?, ?, ?, ?, ?, ?, ?)
	`, func(rows *sql.Rows) {},
		commission.Date,
		commission.Hospital,
		commission.Items,
		commission.OrderValue,
		commission.KAM,
		commission.Assist,
		commission.Paid.Kam,
		commission.Paid.Assist,
		commission.CountryID,
	)
	if err != nil {
		utility.SendError(err, w, r)
		return
	}
	utility.CustomData{}.OK(w, r)
}

// UpdateSalesCommission : update commission row
func UpdateSalesCommission(w http.ResponseWriter, r *http.Request) {
	permission := authentication.GetPermissions(r, "employees")
	if permission < 2 {
		utility.CustomData{}.Unauthorized(w, r)
		return
	}

	commission := salescommission{}
	err := tools.DecodeJson(&commission, r)
	if err != nil {
		utility.SendError(err, w, r)
		return
	}

	err = database.PerformQuery(`
		update salescommission
		set
			date=?,
			hospital=?,
			items=?,
			order_value=?,
			kam=?,
			assist=?,
			paid_kam=?,
			paid_assist=?
		where
			id=?
	`, func(rows *sql.Rows) {},
		commission.Date,
		commission.Hospital,
		commission.Items,
		commission.OrderValue,
		commission.KAM,
		commission.Assist,
		commission.Paid.Kam,
		commission.Paid.Assist,
		commission.ID,
	)
	if err != nil {
		utility.SendError(err, w, r)
		return
	}
	utility.CustomData{}.OK(w, r)
}
