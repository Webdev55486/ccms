package meeting

import (
	"database/sql"
	"net/http"

	"github.com/MedFilm/ccms/lib/api/utility"
	"github.com/MedFilm/ccms/lib/authentication"
	"github.com/MedFilm/ccms/lib/database"
	"github.com/MedFilm/ccms/lib/tools"
)

// Get all meetings
func Get(w http.ResponseWriter, r *http.Request) {
	if permission := authentication.GetPermissions(r, "employees"); permission < 1 {
		utility.CustomData{}.Unauthorized(w, r)
		return
	}
	var meetings []Meeting
	if err := database.PerformQuery(QueryGet, func(rows *sql.Rows) {
		for rows.Next() {
			meeting := Meeting{}
			meeting.Fill(rows)
			meetings = append(meetings, meeting)
		}
	}); err != nil {
		utility.SendError(err, w, r)
		return
	}
	utility.CustomData{Data: map[string]interface{}{
		"meetings": meetings,
	}}.OK(w, r)
}

// Post a meeting
func Post(w http.ResponseWriter, r *http.Request) {
	if permission := authentication.GetPermissions(r, "employees"); permission < 2 {
		utility.CustomData{}.Unauthorized(w, r)
		return
	}
	meeting := Meeting{}
	if err := tools.DecodeJson(&meeting, r); err != nil {
		utility.SendError(err, w, r)
		return
	}
	if err := database.PerformQuery(QueryPost, func(rows *sql.Rows) {},
		meeting.Clinic,
		meeting.Country,
		meeting.Date,
		meeting.KAM,
		meeting.Completed,
		meeting.Canceled,
		meeting.Comments,
		meeting.BookedBy,
	); err != nil {
		utility.SendError(err, w, r)
		return
	}
	utility.CustomData{}.OK(w, r)
}

// Put a meeting with id
func Put(w http.ResponseWriter, r *http.Request) {
	if permission := authentication.GetPermissions(r, "employees"); permission < 2 {
		utility.CustomData{}.Unauthorized(w, r)
		return
	}
	meeting := Meeting{}
	if err := tools.DecodeJson(&meeting, r); err != nil {
		utility.SendError(err, w, r)
		return
	}
	if err := database.PerformQuery(QueryPut, func(rows *sql.Rows) {},
		meeting.Clinic,
		meeting.Country,
		meeting.Date,
		meeting.KAM,
		meeting.Completed,
		meeting.Canceled,
		meeting.Comments,
		meeting.BookedBy,
		meeting.ID,
	); err != nil {
		utility.SendError(err, w, r)
		return
	}
	utility.CustomData{}.OK(w, r)
}

// Delete a meeting with id
func Delete(w http.ResponseWriter, r *http.Request) {
	if permission := authentication.GetPermissions(r, "employees"); permission < 2 {
		utility.CustomData{}.Unauthorized(w, r)
		return
	}
	id, err := tools.GetIdFromUrl(r)
	if err != nil {
		utility.SendError(err, w, r)
		return
	}
	if err := database.PerformQuery(QueryDelete, func(rows *sql.Rows) {},
		id,
	); err != nil {
		utility.SendError(err, w, r)
		return
	}
	utility.CustomData{}.OK(w, r)
}
