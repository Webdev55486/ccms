package meeting

// QueryGet sql
const QueryGet = `
select
	id,
	clinic,
	country,
	date,
	kam,
	completed,
	canceled,
	comments,
	booked_by
from
	meeting
`

// QueryPost sql
const QueryPost = `
insert into meeting (
	clinic, country,
	date, kam, completed,
	canceled, comments, booked_by
) values (?,?,?,?,?,?,?,?)
`

// QueryPut sql
const QueryPut = `
update meeting
set
	clinic=?,
	country=?,
	date=?,
	kam=?,
	completed=?,
	canceled=?,
	comments=?,
	booked_by=?
where
	id=?
`

// QueryDelete sql
const QueryDelete = `
delete from meeting
where id=?
`
