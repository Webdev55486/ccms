package meeting

import "database/sql"

// Meeting datastructure
type Meeting struct {
	ID        int    `json:"id"`
	Clinic    string `json:"clinic"`
	Country   string `json:"country"`
	Date      string `json:"date"`
	KAM       string `json:"kam"`
	BookedBy  string `json:"booked_by"`
	Completed bool   `json:"completed"`
	Canceled  bool   `json:"canceled"`
	Comments  string `json:"comments"`
}

// Fill meeting object with data from row
func (m *Meeting) Fill(rows *sql.Rows) {
	rows.Scan(
		&m.ID,
		&m.Clinic,
		&m.Country,
		&m.Date,
		&m.KAM,
		&m.Completed,
		&m.Canceled,
		&m.Comments,
		&m.BookedBy,
	)
}
