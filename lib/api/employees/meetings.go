package employees

import (
	"database/sql"
	"net/http"

	"github.com/MedFilm/ccms/lib/api/utility"
	"github.com/MedFilm/ccms/lib/authentication"
	"github.com/MedFilm/ccms/lib/database"
	"github.com/MedFilm/ccms/lib/tools"
)

type meetingCountry struct {
	ID       int       `json:"id"`
	Name     string    `json:"name"`
	Meetings []meeting `json:"meetings"`
}

type meeting struct {
	ID               int                 `json:"id"`
	MeetingCountryID int                 `json:"meeting_country_id"`
	ParentID         database.NullInt    `json:"parent_id"`
	PlannedDate      string              `json:"planned_date"`
	Clinic           string              `json:"clinic"`
	Completed        bool                `json:"completed"`
	MovedToDate      database.NullString `json:"moved_to_date"`
	Comment          string              `json:"comment"`
	KAM              string              `json:"kam"`
}

func (c *meetingCountry) getMeetings() error {
	meetings := make([]meeting, 0)
	err := database.PerformQuery(`
		select
			id,
			meeting_country_id,
			parent_id,
			planned_date,
			clinic,
			completed,
			moved_to_date,
			comment,
			kam
		from
			meeting
		where	
			meeting_country_id=?
	`, func(rows *sql.Rows) {
		for rows.Next() {
			m := meeting{}
			rows.Scan(
				&m.ID,
				&m.MeetingCountryID,
				&m.ParentID,
				&m.PlannedDate,
				&m.Clinic,
				&m.Completed,
				&m.MovedToDate,
				&m.Comment,
				&m.KAM,
			)
			meetings = append(meetings, m)
		}
	}, c.ID)

	c.Meetings = meetings
	return err
}

func ReadMeetingsCountrys(w http.ResponseWriter, r *http.Request) {
	if permission := authentication.GetPermissions(r, "employees"); permission == 0 {
		utility.CustomData{}.Unauthorized(w, r)
		return
	}
	countries := make([]meetingCountry, 0)
	err := database.PerformQuery(`
		select
			id,
			name
		from
			meeting_country	
	`, func(rows *sql.Rows) {
		for rows.Next() {
			country := meetingCountry{}
			rows.Scan(&country.ID, &country.Name)
			if err := country.getMeetings(); err != nil {
				utility.SendError(err, w, r)
				return
			}
			countries = append(countries, country)
		}
	})
	if err != nil {
		utility.SendError(err, w, r)
		return
	}

	utility.CustomData{Data: map[string]interface{}{
		"result": countries,
	}}.OK(w, r)
}

func UpdateMeeting(w http.ResponseWriter, r *http.Request) {
	if permission := authentication.GetPermissions(r, "employees"); permission < 2 {
		utility.CustomData{}.Unauthorized(w, r)
		return
	}

	m := meeting{}
	if err := tools.DecodeJson(&m, r); err != nil {
		utility.SendError(err, w, r)
		return
	}

	err := database.PerformQuery(`
		update meeting
		set
			parent_id=?,
			planned_date=?,
			clinic=?,
			completed=?,
			moved_to_date=?,
			comment=?,
			kam=?
		where
			id=?	
	`, func(rows *sql.Rows) {},
		m.ParentID,
		m.PlannedDate,
		m.Clinic,
		m.Completed,
		m.MovedToDate,
		m.Comment,
		m.KAM,
		m.ID,
	)
	if err != nil {
		utility.SendError(err, w, r)
		return
	}

	utility.CustomData{}.OK(w, r)
}
func CreateMeeting(w http.ResponseWriter, r *http.Request) {
	if permission := authentication.GetPermissions(r, "employees"); permission < 2 {
		utility.CustomData{}.Unauthorized(w, r)
		return
	}
	m := meeting{}
	if err := tools.DecodeJson(&m, r); err != nil {
		utility.SendError(err, w, r)
		return
	}
	err := database.PerformQuery(`
		insert into meeting(
			meeting_country_id,
			parent_id,
			planned_date,
			clinic,
			completed,
			moved_to_date,
			comment,
			kam
		) values (?, ?, ?, ?, ?, ?, ?, ?)
	`, func(rows *sql.Rows) {},
		&m.MeetingCountryID,
		&m.ParentID,
		&m.PlannedDate,
		&m.Clinic,
		&m.Completed,
		&m.MovedToDate,
		&m.Comment,
		&m.KAM,
	)

	if err != nil {
		utility.SendError(err, w, r)
		return
	}

	utility.CustomData{}.OK(w, r)
}
func CreateMeetingCountry(w http.ResponseWriter, r *http.Request) {
	if permission := authentication.GetPermissions(r, "employees"); permission < 2 {
		utility.CustomData{}.Unauthorized(w, r)
		return
	}
	country := meetingCountry{}
	if err := tools.DecodeJson(&country, r); err != nil {
		utility.SendError(err, w, r)
		return
	}
	err := database.PerformQuery(`
		insert into meeting_country (
			name
		) values (?)
		`, func(rows *sql.Rows) {}, country.Name)

	if err != nil {
		utility.SendError(err, w, r)
		return
	}

	utility.CustomData{}.OK(w, r)
}
