package employees

import (
	"database/sql"
	"net/http"

	"github.com/MedFilm/ccms/lib/api/utility"
	"github.com/MedFilm/ccms/lib/authentication"
	"github.com/MedFilm/ccms/lib/database"
	"github.com/MedFilm/ccms/lib/tools"
)

type drivinglogPerson struct {
	ID   int          `json:"id"`
	Name string       `json:"name"`
	Logs []drivinglog `json:"logs"`
}
type drivinglog struct {
	ID                 int    `json:"id"`
	DrivinglogPersonID int    `json:"drivinglog_person_id"`
	Type               string `json:"type"`
	Date               string `json:"date"`
	TravelRoute        string `json:"travel_route"`
	Errand             string `json:"errand"`
	MileageStart       int    `json:"mileage_start"`
	MileageEnd         int    `json:"mileage_end"`
}

func (d *drivinglogPerson) getLogs() error {
	var logs []drivinglog
	err := database.PerformQuery(`
		select
			id,
			drivinglog_person_id,
			type,
			date,
			travel_route,
			errand,
			mileage_start,
			mileage_end
		from
			drivinglog
		where
			drivinglog_person_id=?
		order by
			createdAt desc
	`, func(rows *sql.Rows) {
		for rows.Next() {
			log := drivinglog{}
			rows.Scan(
				&log.ID,
				&log.DrivinglogPersonID,
				&log.Type,
				&log.Date,
				&log.TravelRoute,
				&log.Errand,
				&log.MileageStart,
				&log.MileageEnd,
			)
			logs = append(logs, log)
		}
	}, d.ID)
	if err != nil {
		return err
	}
	d.Logs = logs
	return nil
}

// ReadDrivingLogs : gets all drivinglogs
func ReadDrivingLogs(w http.ResponseWriter, r *http.Request) {
	permission := authentication.GetPermissions(r, "employees")
	if permission == 0 {
		utility.CustomData{}.Unauthorized(w, r)
		return
	}

	var persons []drivinglogPerson
	err := database.PerformQuery(`
		select
			id,
			name
		from
			drivinglog_person
		order by
			createdAt
	`, func(rows *sql.Rows) {
		for rows.Next() {
			person := drivinglogPerson{}
			rows.Scan(&person.ID, &person.Name)
			person.getLogs()
			persons = append(persons, person)
		}
	})
	if err != nil {
		utility.SendError(err, w, r)
		return
	}
	utility.CustomData{Data: map[string]interface{}{
		"result": persons,
	}}.OK(w, r)
}

// CreateDrivinglogPerson : creates a person for drivinglogs
func CreateDrivinglogPerson(w http.ResponseWriter, r *http.Request) {
	if permission := authentication.GetPermissions(r, "employees"); permission < 2 {
		utility.CustomData{}.Unauthorized(w, r)
		return
	}

	person := drivinglogPerson{}
	err := tools.DecodeJson(&person, r)
	if err != nil {
		utility.SendError(err, w, r)
		return
	}

	err = database.PerformQuery(`
		insert into drivinglog_person(name)
		values(?)
	`, func(rows *sql.Rows) {}, person.Name)

	if err != nil {
		utility.SendError(err, w, r)
		return
	}
	utility.CustomData{}.OK(w, r)
}

// CreateDrivinglog : creates a drivinglog
func CreateDrivinglog(w http.ResponseWriter, r *http.Request) {
	if permission := authentication.GetPermissions(r, "employees"); permission < 2 {
		utility.CustomData{}.Unauthorized(w, r)
		return
	}

	log := drivinglog{}
	err := tools.DecodeJson(&log, r)
	if err != nil {
		utility.SendError(err, w, r)
		return
	}

	err = database.PerformQuery(`
		insert into drivinglog
		(
			drivinglog_person_id,
			type,
			date,
			travel_route,
			errand,
			mileage_start,
			mileage_end
		) values (?, ?, ?, ?, ?, ?, ?)
	`, func(rows *sql.Rows) {},
		log.DrivinglogPersonID,
		log.Type,
		log.Date,
		log.TravelRoute,
		log.Errand,
		log.MileageStart,
		log.MileageEnd,
	)
	if err != nil {
		utility.SendError(err, w, r)
		return
	}
	utility.CustomData{}.OK(w, r)
}

// UpdateDrivinglog : Updates drivinglog
func UpdateDrivinglog(w http.ResponseWriter, r *http.Request) {
	if permission := authentication.GetPermissions(r, "employees"); permission < 2 {
		utility.CustomData{}.Unauthorized(w, r)
		return
	}
	log := drivinglog{}
	err := tools.DecodeJson(&log, r)
	if err != nil {
		utility.SendError(err, w, r)
		return
	}

	err = database.PerformQuery(`
		update drivinglog
		set
			type=?,
			date=?,
			travel_route=?,
			errand=?,
			mileage_start=?,
			mileage_end=?
		where
			id=?
	`, func(rows *sql.Rows) {},
		log.Type,
		log.Date,
		log.TravelRoute,
		log.Errand,
		log.MileageStart,
		log.MileageEnd,
		log.ID,
	)

	if err != nil {
		utility.SendError(err, w, r)
		return
	}

	utility.CustomData{}.OK(w, r)
}
