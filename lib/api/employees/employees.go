package employees

import (
	"database/sql"
	"errors"
	"fmt"
	"net/http"
	"time"

	"strconv"

	"github.com/MedFilm/ccms/lib/api/utility"
	"github.com/MedFilm/ccms/lib/authentication"
	"github.com/MedFilm/ccms/lib/database"
	"github.com/MedFilm/ccms/lib/tools"
	"github.com/gorilla/mux"
)

type Suggestion struct {
	Id      int64  `json:"id"`
	Heading string `json:"heading"`
	Content string `json:"content"`
	Author  string `json:"author"`
	Date    string `json:"date"`
}

type Timelog struct {
	Id         int64  `json:"id"`
	EmployeeId int64  `json:"employeeId"`
	Type       int64  `json:"type"`
	Year       int64  `json:"year"`
	Month      int64  `json:"month"`
	Day        int64  `json:"day"`
	Start      string `json:"start"`
	Break      string `json:"break"`
	End        string `json:"end"`
	Note       string `json:"note"`
}

type MonthSummary struct {
	EmployeeId      int64   `json:"employeeId"`
	Year            int64   `json:"year"`
	Month           int64   `json:"month"`
	SickDays        int64   `json:"sickDays"`
	VacationDays    int64   `json:"vacationDays"`
	DaysOff         int64   `json:"daysOff"`
	NumOfDaysWorked int64   `json:"numOfDaysWorked"`
	Workdays        int64   `json:"workdays"`
	Workhours       int64   `json:"workhours"`
	HoursWorked     float64 `json:"hoursWorked"`
}

type timelogRequestBody struct {
	EmployeeId int64 `json:"employeeId"`
	Year       int64 `json:"year"`
	Month      int64 `json:"month"`
}

type TodoItem struct {
	Id       int64  `json:"id"`
	Label    string `json:"label"`
	Content  string `json:"content"`
	Color    string `json:"color"`
	GroupID  int64  `json:"group_id"`
	Open     int8   `json:"open"`
	Priority int64  `json:"priority"`
}
type TodoGroup struct {
	Id         int64      `json:"id"`
	Label      string     `json:"label"`
	CategoryID int64      `json:"category_id"`
	Priority   int64      `json:"priority"`
	Items      []TodoItem `json:"items"`
}
type TodoCategory struct {
	Id     int64       `json:"id"`
	Label  string      `json:"label"`
	Groups []TodoGroup `json:"groups"`
}

func GetTodos(w http.ResponseWriter, req *http.Request) {
	// Check permission of request
	permission := authentication.GetPermissions(req, "employees")
	if permission < 1 {
		utility.CustomData{}.Unauthorized(w, req)
		return
	}

	// Initialize top layer struct
	var categories []TodoCategory

	// Collect categories
	err := database.PerformQuery(`
		select
			id,
			label
		from
			todo_category
	`, func(rows *sql.Rows) {
		for rows.Next() {
			// Initialize category
			category := TodoCategory{}
			rows.Scan(&category.Id, &category.Label)

			// Collect groups for category
			err := database.PerformQuery(`
				select
					id,
					label,
					category_id,
					priority
				from
					todo_group
				where
					category_id=?
				order by
					priority
			`, func(rows *sql.Rows) {
				for rows.Next() {
					// Initialize group
					group := TodoGroup{}
					rows.Scan(&group.Id, &group.Label, &group.CategoryID, &group.Priority)

					// Collect items for group
					err := database.PerformQuery(`
						select
							id,
							label,
							content,
							group_id,
							color,
							isOpen,
							priority
						from
							todo_item
						where
							group_id=?
						order by
							priority
					`, func(rows *sql.Rows) {
						for rows.Next() {
							// Initialize item
							item := TodoItem{}
							rows.Scan(&item.Id, &item.Label, &item.Content, &item.GroupID, &item.Color, &item.Open, &item.Priority)

							// Append to parent struct
							group.Items = append(group.Items, item)
						}
					}, group.Id)

					if err != nil {
						utility.SendError(err, w, req)
						return
					}

					// Append to parent struct
					category.Groups = append(category.Groups, group)
				}

			}, category.Id)

			if err != nil {
				utility.SendError(err, w, req)
				return
			}

			// Append to top level struct
			categories = append(categories, category)
		}
	})

	if err != nil {
		utility.SendError(err, w, req)
		return
	}

	utility.CustomData{Data: map[string]interface{}{
		"result": categories,
	}}.OK(w, req)
}

func UpdateTodo(w http.ResponseWriter, req *http.Request) {
	// Check permission of request
	permission := authentication.GetPermissions(req, "employees")
	if permission < 1 {
		utility.CustomData{}.Unauthorized(w, req)
		return
	}

	// Get data from request
	item := TodoItem{}
	err := tools.DecodeJson(&item, req)

	if err != nil {
		utility.SendError(err, w, req)
		return
	}

	id, err := tools.GetIdFromUrl(req)
	if err != nil {
		utility.SendError(err, w, req)
		return
	}

	err = database.PerformQuery(`
		update
			todo_item
		set
			label=?,
			content=?,
			group_id=?,
			color=?,
			isOpen=?,
			priority=?
		where
			id=?
	`, func(rows *sql.Rows) {}, &item.Label, &item.Content, &item.GroupID, &item.Color, &item.Open, &item.Priority, id)

	if err != nil {
		utility.SendError(err, w, req)
		return
	}

	utility.CustomData{}.OK(w, req)
}
func UpdateGroup(w http.ResponseWriter, req *http.Request) {
	// Check permission of request
	permission := authentication.GetPermissions(req, "employees")
	if permission < 1 {
		utility.CustomData{}.Unauthorized(w, req)
		return
	}
	fmt.Println("UPDATING GROUP")
	// Get data from request
	group := TodoGroup{}
	err := tools.DecodeJson(&group, req)

	if err != nil {
		utility.SendError(err, w, req)
		return
	}

	id, err := tools.GetIdFromUrl(req)
	if err != nil {
		utility.SendError(err, w, req)
		return
	}

	err = database.PerformQuery(`
		update
			todo_group
		set
			label=?,
			priority=?
		where
			id=?
	`, func(rows *sql.Rows) {}, &group.Label, &group.Priority, id)

	if err != nil {
		utility.SendError(err, w, req)
		return
	}

	utility.CustomData{}.OK(w, req)
}

func DeleteTodo(w http.ResponseWriter, req *http.Request) {
	// Check permission of request
	permission := authentication.GetPermissions(req, "employees")
	if permission < 1 {
		utility.CustomData{}.Unauthorized(w, req)
		return
	}

	// Get id from requst
	id, err := tools.GetIdFromUrl(req)
	if err != nil {
		utility.SendError(err, w, req)
		return
	}

	err = database.PerformQuery(`
		delete from
			todo_item
		where
			id=?
	`, func(rows *sql.Rows) {}, id)

	if err != nil {
		utility.SendError(err, w, req)
		return
	}

	utility.CustomData{}.OK(w, req)
}
func DeleteTodoGroup(w http.ResponseWriter, req *http.Request) {
	// Check permission of request
	permission := authentication.GetPermissions(req, "employees")
	if permission < 1 {
		utility.CustomData{}.Unauthorized(w, req)
		return
	}

	// Get id from requst
	id, err := tools.GetIdFromUrl(req)
	if err != nil {
		utility.SendError(err, w, req)
		return
	}

	err = database.PerformQuery(`
		delete from
			todo_group
		where
			id=?
	`, func(rows *sql.Rows) {}, id)

	if err != nil {
		utility.SendError(err, w, req)
		return
	}

	utility.CustomData{}.OK(w, req)
}
func DeleteTodoCategory(w http.ResponseWriter, req *http.Request) {
	// Check permission of request
	permission := authentication.GetPermissions(req, "employees")
	if permission < 1 {
		utility.CustomData{}.Unauthorized(w, req)
		return
	}

	// Get id from requst
	id, err := tools.GetIdFromUrl(req)
	if err != nil {
		utility.SendError(err, w, req)
		return
	}

	err = database.PerformQuery(`
		delete from
			todo_category
		where
			id=?
	`, func(rows *sql.Rows) {}, id)

	if err != nil {
		utility.SendError(err, w, req)
		return
	}

	utility.CustomData{}.OK(w, req)
}

func InsertTodo(w http.ResponseWriter, req *http.Request) {
	// Check permission of request
	permission := authentication.GetPermissions(req, "employees")
	if permission < 1 {
		utility.CustomData{}.Unauthorized(w, req)
		return
	}

	// Collect data from request
	item := TodoItem{}
	err := tools.DecodeJson(&item, req)
	if err != nil {
		utility.SendError(err, w, req)
		return
	}

	err = database.PerformQuery(`
		insert into todo_item (
			label, content, color, group_id, priority
		) values (?, ?, ?, ?, ?)
	`, func(rows *sql.Rows) {}, item.Label, item.Content, item.Color, item.GroupID, item.Priority)

	if err != nil {
		utility.SendError(err, w, req)
		return
	}

	utility.CustomData{}.OK(w, req)
}
func InsertTodoGroup(w http.ResponseWriter, req *http.Request) {
	// Check permission of request
	permission := authentication.GetPermissions(req, "employees")
	if permission < 1 {
		utility.CustomData{}.Unauthorized(w, req)
		return
	}

	// Collect data from request
	group := TodoGroup{}
	err := tools.DecodeJson(&group, req)
	if err != nil {
		utility.SendError(err, w, req)
		return
	}

	err = database.PerformQuery(`
		insert into todo_group (
			label, category_id, priority
		) values (?, ?, ?)
	`, func(rows *sql.Rows) {}, group.Label, group.CategoryID, group.Priority)

	if err != nil {
		utility.SendError(err, w, req)
		return
	}

	utility.CustomData{}.OK(w, req)
}
func InsertTodoCategory(w http.ResponseWriter, req *http.Request) {
	// Check permission of request
	permission := authentication.GetPermissions(req, "employees")
	if permission < 1 {
		utility.CustomData{}.Unauthorized(w, req)
		return
	}

	// Collect data from request
	category := TodoCategory{}
	err := tools.DecodeJson(&category, req)
	if err != nil {
		utility.SendError(err, w, req)
		return
	}

	err = database.PerformQuery(`
		insert into todo_category (
			label
		) values (?)
	`, func(rows *sql.Rows) {}, category.Label)

	if err != nil {
		utility.SendError(err, w, req)
		return
	}

	utility.CustomData{}.OK(w, req)
}

func GetTimelogs(w http.ResponseWriter, req *http.Request) {

	// Get permission of requesting user
	permission := authentication.GetPermissions(req, "employees")
	if permission == 0 {
		utility.CustomData{}.Unauthorized(w, req)
		return
	}

	// Grab params from request
	queryVars := timelogRequestBody{}

	employeeIdStr, err := getVarFromRequest(req, "employeeId")

	if err != nil {
		fmt.Println("Error getting employeeId")
		utility.CustomData{}.PreconditionFailed(w, req)
		return
	}

	yearStr, err := getVarFromRequest(req, "year")

	if err != nil {
		fmt.Println("Error getting year")
		utility.CustomData{}.PreconditionFailed(w, req)
		return
	}

	monthStr, err := getVarFromRequest(req, "month")

	if err != nil {
		fmt.Println("Error getting month")
		utility.CustomData{}.PreconditionFailed(w, req)
		return
	}

	// Parse params
	employeeId, err := strconv.ParseInt(employeeIdStr, 10, 64)

	if err != nil {
		fmt.Println("Error parsing employeeId")
		utility.CustomData{}.PreconditionFailed(w, req)
		return
	}

	year, err := strconv.ParseInt(yearStr, 10, 64)

	if err != nil {
		fmt.Println("Error parsing year")
		utility.CustomData{}.PreconditionFailed(w, req)
		return
	}

	month, err := strconv.ParseInt(monthStr, 10, 64)

	if err != nil {
		fmt.Println("Error parsing month")
		utility.CustomData{}.PreconditionFailed(w, req)
		return
	}

	// Fill query struct
	queryVars.EmployeeId = employeeId
	queryVars.Year = year
	queryVars.Month = month

	// Check if month exists
	err = database.PerformQuery(
		`
		SELECT
			*
		FROM
			timelog
		WHERE
			month=? AND employee=? AND year=?
		LIMIT 1
		`,
		func(rows *sql.Rows) {
			if !rows.Next() {

				// No dates found, create month
				dates, err := addDates(yearStr, monthStr)
				if err != nil {
					utility.SendError(err, w, req)
					return
				}

				for _, date := range dates {
					var Type int64
					weekday := date.Weekday()

					if weekday == time.Sunday || weekday == time.Saturday {
						Type = 1
					} else {
						Type = 2
					}

					err = database.PerformQuery(
						`
						INSERT INTO timelog (
							employee, type, year,
							month, day, start,
							break, end, note
						) VALUES (
							?, ?, ?,
							?, ?, ?,
							?, ?, ?
						)
						`,
						func(sql *sql.Rows) {},
						&queryVars.EmployeeId,
						Type,
						date.Year(),
						date.Month(),
						date.Day(),
						"0:00",
						"0:00",
						"0:00",
						"",
					)

					if err != nil {
						utility.SendError(err, w, req)
						return
					}

				}
			}
		},
		&queryVars.Month,
		&queryVars.EmployeeId,
		&queryVars.Year,
	)

	if err != nil {
		utility.SendError(err, w, req)
		return
	}

	// Initiate top layer struct
	var timelogs []Timelog

	// Perform query
	err = database.PerformQuery(
		`
		SELECT 
			id,
			type,
			year,
			month,
			day,
			start,
			break,
			end,
			note
		FROM
			timelog
		WHERE
			employee=? AND year=? AND month=?
		ORDER BY
			day
		`,
		func(rows *sql.Rows) {
			// Fetch and assign timelogs
			for rows.Next() {

				timelog := Timelog{}

				rows.Scan(
					&timelog.Id,
					&timelog.Type,
					&timelog.Year,
					&timelog.Month,
					&timelog.Day,
					&timelog.Start,
					&timelog.Break,
					&timelog.End,
					&timelog.Note,
				)

				timelogs = append(timelogs, timelog)
			}
		},
		&queryVars.EmployeeId,
		&queryVars.Year,
		&queryVars.Month,
	)

	if err != nil {
		utility.SendError(err, w, req)
		return
	}

	utility.CustomData{Data: map[string]interface{}{
		"result": timelogs,
	}}.OK(w, req)
}

func addDates(y string, m string) ([]time.Time, error) {
	start, err := time.Parse("2006-1-2", y+"-"+m+"-1")
	if err != nil {
		return nil, err
	}
	var dates []time.Time

	for d := start; d.Month() == start.Month(); d = d.AddDate(0, 0, 1) {
		dates = append(dates, d)
	}

	return dates, nil
}

func UpdateTimelog(w http.ResponseWriter, req *http.Request) {

	// Get permission of requesting user
	permission := authentication.GetPermissions(req, "employees")

	if permission == 0 {
		utility.CustomData{}.Unauthorized(w, req)
		return
	}

	// Get id from request
	id, err := tools.GetIdFromUrl(req)
	if err != nil {
		utility.SendError(err, w, req)
		return
	}

	// Create struct for the values
	timelog := Timelog{}

	// Get values from request
	err = tools.DecodeJson(&timelog, req)
	if err != nil {
		utility.SendError(err, w, req)
		return
	}

	err = database.PerformQuery(
		`
		UPDATE
			timelog
		SET
			type=?,
			start=?,
			break=?,
			end=?,
			note=?,
			updatedAt=?
		WHERE
			id=?
		`,
		func(rows *sql.Rows) {},
		&timelog.Type,
		&timelog.Start,
		&timelog.Break,
		&timelog.End,
		&timelog.Note,
		time.Now().Local().Format("2006-01-02"),
		id,
	)

	if err != nil {
		utility.SendError(err, w, req)
		return
	}

	utility.CustomData{}.OK(w, req)

}

func getVarFromRequest(req *http.Request, key string) (string, error) {
	vars := mux.Vars(req)
	if val, ok := vars[key]; ok {
		return val, nil
	}
	return "", errors.New("Could not find value with specified key")
}

func GetSuggestions(w http.ResponseWriter, req *http.Request) {

	// Get permission of requesting user
	permission := authentication.GetPermissions(req, "employees")
	if permission == 0 {
		utility.CustomData{}.Unauthorized(w, req)
		return
	}

	// Initiate top layer struct
	var resultSuggestions []Suggestion

	err := database.PerformQuery(
		`
		SELECT
			id,
			heading,
			content,
			author,
			createdAt
		FROM
			system_improvements
		ORDER BY 
			createdAt
		`,
		func(rows *sql.Rows) {
			// Fetch and assign all countries
			for rows.Next() {

				suggestion := Suggestion{}

				rows.Scan(
					&suggestion.Id,
					&suggestion.Heading,
					&suggestion.Content,
					&suggestion.Author,
					&suggestion.Date,
				)

				resultSuggestions = append(resultSuggestions, suggestion)
			}
		},
	)

	if err != nil {
		utility.SendError(err, w, req)
		return
	}

	utility.CustomData{Data: map[string]interface{}{
		"result": resultSuggestions,
	}}.OK(w, req)
}

func InsertSuggestion(w http.ResponseWriter, req *http.Request) {

	// Get permission of requesting user
	permission := authentication.GetPermissions(req, "employees")
	if permission < 1 {
		utility.CustomData{}.Unauthorized(w, req)
		return
	}

	suggestion := Suggestion{}

	// Get suggestion from request
	err := tools.DecodeJson(&suggestion, req)
	if err != nil {
		utility.SendError(err, w, req)
		return
	}

	err = database.PerformQuery("INSERT INTO system_improvements (heading, content, author, createdAt) VALUES (?, ?, ?, ?)", func(rows *sql.Rows) {},
		suggestion.Heading,
		suggestion.Content,
		suggestion.Author,
		suggestion.Date,
	)

	if err != nil {
		utility.SendError(err, w, req)
		return
	}

	utility.CustomData{}.OK(w, req)
}

func DeleteSuggestion(w http.ResponseWriter, req *http.Request) {

	// Get permission of requesting user
	permission := authentication.GetPermissions(req, "employees")
	if permission < 2 {
		utility.CustomData{}.Unauthorized(w, req)
		return
	}

	id, err := tools.GetIdFromUrl(req)
	if err != nil {
		utility.SendError(err, w, req)
		return
	}

	err = database.PerformQuery(
		`
		DELETE FROM
			system_improvements
		WHERE
			id=?
		`,
		func(rows *sql.Rows) {},
		id,
	)

	if err != nil {
		utility.SendError(err, w, req)
		return
	}

	utility.CustomData{}.OK(w, req)
}

func GetSummary(w http.ResponseWriter, req *http.Request) {
	permission := authentication.GetPermissions(req, "employees")
	if permission == 0 {
		utility.CustomData{}.Unauthorized(w, req)
		return
	}

	summary := MonthSummary{}

	// Collect values
	employeeIdStr, err := getVarFromRequest(req, "employeeId")
	if err != nil {
		utility.SendError(err, w, req)
		return
	}
	monthStr, err := getVarFromRequest(req, "month")
	if err != nil {
		utility.SendError(err, w, req)
		return
	}
	yearStr, err := getVarFromRequest(req, "year")
	if err != nil {
		utility.SendError(err, w, req)
		return
	}

	// Parse values
	employeeId, err := strconv.ParseInt(employeeIdStr, 10, 64)
	if err != nil {
		utility.SendError(err, w, req)
		return
	}
	month, err := strconv.ParseInt(monthStr, 10, 64)
	if err != nil {
		utility.SendError(err, w, req)
		return
	}
	year, err := strconv.ParseInt(yearStr, 10, 64)
	if err != nil {
		utility.SendError(err, w, req)
		return
	}

	// Save data
	summary.EmployeeId = employeeId
	summary.Month = month
	summary.Year = year

	// Query for info
	err = database.PerformQuery(`
		select
			SUM((((hour(end)-hour(start)-hour(break))*60)+(minute(end)-minute(start)-minute(break))) / 60) as HOURSWORKED
		from
			timelog
		where
			employee=?
			and
			year=?
			and
			month=?
			and
			type!=4
			and
			type!=6
			and
			type!=5
	`, func(rows *sql.Rows) {
		for rows.Next() {
			rows.Scan(
				&summary.HoursWorked,
			)
		}
	}, summary.EmployeeId, summary.Year, summary.Month)
	if err != nil {
		utility.SendError(err, w, req)
		return
	}

	err = database.PerformQuery(`
	select
		count(id) as WORKDAYS,
		count(id) * 8 as WORKHOURS
	from
		timelog
	where
		employee=?
		and
		year=?
		and
		month=?
		and
		type=2
	`, func(rows *sql.Rows) {
		for rows.Next() {
			rows.Scan(
				&summary.Workdays,
				&summary.Workhours,
			)
		}
	}, summary.EmployeeId, summary.Year, summary.Month)
	if err != nil {
		utility.SendError(err, w, req)
		return
	}

	err = database.PerformQuery(`
		select
			COUNT(id) as NUMOFDAYSWORKED
		from
			timelog
		where
			# Workdays
			employee=?
			and
			year=?
			and
			month=?
			and
			type=2
			and
			day<=day(curdate())
			and
			month=month(curdate())
		or
			employee=?
			and
			year=?
			and
			month=?
			and
			type=2
			and
			month!=month(curdate())
		or
			# Weekends (both end and start)
			employee=?
			and
			year=?
			and
			month=?
			and
			type=1
			and
			end!="00:00:00"
			and
			start!="00:00:00"
		or
			# Weekends (end)
			employee=?
			and
			year=?
			and
			month=?
			and
			type=1
			and
			end!="00:00:00"
		or
			# Weekends (start)
			employee=?
			and
			year=?
			and
			month=?
			and
			type=1
			and
			start!="00:00:00"
		or
			# Holiday (both end and start)
			employee=?
			and
			year=?
			and
			month=?
			and
			type=3
			and
			end!="00:00:00"
			and
			start!="00:00:00"
		or
			# Holiday (end)
			employee=?
			and
			year=?
			and
			month=?
			and
			type=3
			and
			end!="00:00:00"
		or
			# Holiday (start)
			employee=?
			and
			year=?
			and
			month=?
			and
			type=3
			and
			start!="00:00:00"
	`, func(rows *sql.Rows) {
		for rows.Next() {
			rows.Scan(
				&summary.NumOfDaysWorked,
			)
		}
	},
		summary.EmployeeId, summary.Year, summary.Month,
		summary.EmployeeId, summary.Year, summary.Month,
		summary.EmployeeId, summary.Year, summary.Month,
		summary.EmployeeId, summary.Year, summary.Month,
		summary.EmployeeId, summary.Year, summary.Month,
		summary.EmployeeId, summary.Year, summary.Month,
		summary.EmployeeId, summary.Year, summary.Month,
		summary.EmployeeId, summary.Year, summary.Month,
	)
	if err != nil {
		utility.SendError(err, w, req)
		return
	}

	// SICKDAYS
	err = database.PerformQuery(`
		select
			count(id) as SICKDAYS
		from
			timelog
		where
			employee=?
			and
			year=?
			and
			month=?
			and
			type=6;
	`, func(rows *sql.Rows) {
		for rows.Next() {
			rows.Scan(
				&summary.SickDays,
			)
		}
	},
		summary.EmployeeId, summary.Year, summary.Month,
	)
	if err != nil {
		utility.SendError(err, w, req)
		return
	}
	// DAYSOFF
	err = database.PerformQuery(`
		select
			count(id) as DAYSOFF
		from
			timelog
		where
			employee=?
			and
			year=?
			and
			month=?
			and
			type=5;
	`, func(rows *sql.Rows) {
		for rows.Next() {
			rows.Scan(
				&summary.DaysOff,
			)
		}
	},
		summary.EmployeeId, summary.Year, summary.Month,
	)
	if err != nil {
		utility.SendError(err, w, req)
		return
	}
	// VACATION DAYS
	err = database.PerformQuery(`
		select
			count(id) as VACATIONDAYS
		from
			timelog
		where
			employee=?
			and
			year=?
			and
			month=?
			and
			type=4;
	`, func(rows *sql.Rows) {
		for rows.Next() {
			rows.Scan(
				&summary.VacationDays,
			)
		}
	},
		summary.EmployeeId, summary.Year, summary.Month,
	)
	if err != nil {
		utility.SendError(err, w, req)
		return
	}

	utility.CustomData{Data: map[string]interface{}{
		"result": summary,
	}}.OK(w, req)
}
