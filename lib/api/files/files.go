package files

import (
	"bufio"
	"database/sql"
	"errors"
	"github.com/MedFilm/ccms/lib/api/utility"
	"github.com/MedFilm/ccms/lib/authentication"
	"github.com/MedFilm/ccms/lib/database"
	"github.com/gorilla/mux"
	"github.com/nu7hatch/gouuid"
	"net/http"
	"os"
	"strconv"
)

func PrepareFileForFilm(w http.ResponseWriter, req *http.Request) {
	// Get permission of requesting user
	permission := authentication.GetPermissions(req, "website")

	if permission < 2 {
		utility.CustomData{}.Unauthorized(w, req)
		return
	}

	var (
		err      error
		id       int64
		name     string
		language string
		val      string
		ok       bool
		guuid    string
	)

	// Check for id in params
	vars := mux.Vars(req)
	val, ok = vars["id"]
	if !ok {
		utility.SendError(errors.New("Could not find Id"), w, req)
		return
	}

	id, err = strconv.ParseInt(val, 10, 64)
	if err != nil {
		utility.SendError(err, w, req)
		return
	}

	name, ok = vars["name"]
	if !ok {
		utility.SendError(errors.New("Could not find name"), w, req)
		return
	}

	language, ok = vars["language"]
	if !ok {
		utility.SendError(errors.New("Could not find name"), w, req)
		return
	}

	err = database.DBCon.QueryRow(
		`
		SELECT
			filename
		FROM
			film_files
		WHERE
			name = ? AND language = ? AND film_id = ?
		LIMIT 1
		`,
		name,
		language,
		id,
	).Scan(
		&guuid,
	)

	if guuid == "" {
		guuid, err = newFilmFile(id, name, language)
		if err != nil {
			utility.SendError(err, w, req)
			return
		}
	} else {
		if _, err = os.Stat("./files/" + guuid); !os.IsNotExist(err) {
			err = os.Remove("./files/" + guuid)
			if err != nil {
				utility.SendError(err, w, req)
				return
			}

			guuid, err = newFilmFileId(guuid)
			if err != nil {
				utility.SendError(err, w, req)
				return
			}
		}
	}

	utility.CustomData{
		Data: map[string]interface{}{
			"id": guuid,
		},
	}.OK(w, req)
}

func PrepareFileForSubscriptionFilm(w http.ResponseWriter, req *http.Request) {
	// Get permission of requesting user
	permission := authentication.GetPermissions(req, "website")

	if permission < 2 {
		utility.CustomData{}.Unauthorized(w, req)
		return
	}

	var (
		err      error
		subId    int64
		name     string
		language string
		val      string
		ok       bool
		guuid    string
	)

	// Check for id in params
	vars := mux.Vars(req)
	val, ok = vars["id"]
	if !ok {
		utility.SendError(errors.New("Could not find Id"), w, req)
		return
	}

	subId, err = strconv.ParseInt(val, 10, 64)
	if err != nil {
		utility.SendError(err, w, req)
		return
	}

	name, ok = vars["name"]
	if !ok {
		utility.SendError(errors.New("Could not find name"), w, req)
		return
	}

	language, ok = vars["language"]
	if !ok {
		utility.SendError(errors.New("Could not find name"), w, req)
		return
	}

	err = database.DBCon.QueryRow(
		`
		SELECT
			filename
		FROM
			film_files
		WHERE
			name = ? AND language = ? AND subscription_id = ?
		LIMIT 1
		`,
		name,
		language,
		subId,
	).Scan(
		&guuid,
	)

	if guuid == "" {
		guuid, err = newSubscriptionFilmFile(subId, name, language)
		if err != nil {
			utility.SendError(err, w, req)
			return
		}
	} else {
		if _, err = os.Stat("./files/" + guuid); !os.IsNotExist(err) {
			err = os.Remove("./files/" + guuid)
			if err != nil {
				utility.SendError(err, w, req)
				return
			}

			guuid, err = newFilmFileId(guuid)
			if err != nil {
				utility.SendError(err, w, req)
				return
			}
		}
	}

	utility.CustomData{
		Data: map[string]interface{}{
			"id": guuid,
		},
	}.OK(w, req)
}

func newFilmFile(id int64, name string, language string) (string, error) {
	identifier, err := uuid.NewV4()
	if err != nil {
		return "", err
	}

	_, err = database.DBCon.Exec(
		`
		INSERT INTO
			film_files (film_id, language, name, filename)
			VALUES (?, ?, ?, ?)
		`,
		id,
		language,
		name,
		identifier.String(),
	)

	if err != nil {
		return "", err
	}

	return identifier.String(), nil
}

func newSubscriptionFilmFile(subId int64, name string, language string) (string, error) {
	identifier, err := uuid.NewV4()
	if err != nil {
		return "", err
	}

	var filmId int64
	err = database.DBCon.QueryRow(
		`
		SELECT
			f.id
		FROM
			film f
		INNER JOIN
			subscriptions sub ON f.id = sub.film_id AND sub.id = ?
		`,
		subId,
	).Scan(
		&filmId,
	)

	if err != nil {
		return "", err
	}

	_, err = database.DBCon.Exec(
		`
		INSERT INTO
			film_files (film_id, subscription_id, language, name, filename)
			VALUES (?, ?, ?, ?, ?)
		`,
		filmId,
		subId,
		language,
		name,
		identifier.String(),
	)

	if err != nil {
		return "", err
	}

	return identifier.String(), nil
}

func newFilmFileId(guuid string) (string, error) {
	newGuuid, err := uuid.NewV4()
	if err != nil {
		return "", err
	}

	err = database.PerformQuery(
		`
		UPDATE
			film_files
		SET
			filename = ?
		WHERE
			filename = ?
		`,
		func(rows *sql.Rows) {},
		newGuuid.String(),
		guuid,
	)

	return newGuuid.String(), err
}

func UploadFile(w http.ResponseWriter, req *http.Request) {
	// Get permission of requesting user
	permission := authentication.GetPermissions(req, "website")

	if permission < 2 {
		utility.CustomData{}.Unauthorized(w, req)
		return
	}

	err := req.ParseMultipartForm(5 * 1024 * 1024)
	if err != nil {
		utility.SendError(err, w, req)
		return
	}

	// Check for id in params
	vars := mux.Vars(req)
	id, ok := vars["id"]
	if !ok {
		utility.SendError(errors.New("Could not find Id"), w, req)
		return
	}

	dst, err := os.OpenFile("./files/"+string(id), os.O_CREATE|os.O_RDWR, 0660)
	if err != nil {
		utility.SendError(err, w, req)
		return
	}

	rcn, err := strconv.ParseInt(req.MultipartForm.Value["resumableChunkNumber"][0], 10, 64)
	rcs, err := strconv.ParseInt(req.MultipartForm.Value["resumableChunkSize"][0], 10, 64)
	rccs, err := strconv.ParseInt(req.MultipartForm.Value["resumableCurrentChunkSize"][0], 10, 64)
	if err != nil {
		utility.SendError(err, w, req)
		return
	}

	m := req.MultipartForm.File["file"]
	for i := range m {
		chunkBufferBytes := make([]byte, rccs)

		file, err := m[i].Open()
		if err != nil {
			utility.SendError(err, w, req)
			return
		}

		reader := bufio.NewReader(file)
		_, err = reader.Read(chunkBufferBytes)
		if err != nil {
			utility.SendError(err, w, req)
			return
		}
		dst.WriteAt(chunkBufferBytes, ((rcn - 1) * rcs))
		dst.Sync()
	}

	dst.Close()
	utility.CustomData{}.OK(w, req)
}
