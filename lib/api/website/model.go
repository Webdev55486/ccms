/**
 * ccms
 * Copyright (C) Sunstate AB - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Simon Wikstrand <simon@sunstate.se>, 2017-01-26
 */

package website

import (
	"database/sql"
	"github.com/MedFilm/ccms/lib/database"
	"github.com/MedFilm/ccms/lib/tools"
	"os"
)

type wrapArticles struct {
	Data []article `json:"data"`
}

type wrapArticle struct {
	Data article `json:"article"`
}

type wrapImageSet struct {
	Data imageSet `json:"imageSet"`
}

type wrapLogotypes struct {
	Data []Logotypes `json:"data"`
}

type article struct {
	Id          int64   `json:"id"`
	Published   bool    `json:"published"`
	Type        string  `json:"type"`
	Language    string  `json:"language"`
	Title       *string `json:"title"`
	Slug        *string `json:"slug"`
	Description *string `json:"description"`
	Blurb       *string `json:"blurb"`
	Text        *string `json:"text"`
	Thumbnail   *string `json:"image"`
	Header      *string `json:"header"`
	Created     string  `json:"created"`
	Updated     string  `json:"updated"`
}

type imageSet struct {
	Header    string `json:"header"`
	Thumbnail string `json:"thumbnail"`
}

type LanguageFile struct {
	Data string `json:"data"`
}

type Logotypes struct {
	Id int64 `json:"id"`
	Logo string `json:"logo"`
	Order *int64 `json:"order"`
	Active int64 `json:"active"`
}

func (w *wrapArticles) insertData(rows *sql.Rows) error {
	for rows.Next() {
		var parent article

		rows.Scan(
			&parent.Id,
			&parent.Published,
			&parent.Type,
			&parent.Language,
			&parent.Title,
			&parent.Slug,
			&parent.Description,
			&parent.Blurb,
			&parent.Text,
			&parent.Thumbnail,
			&parent.Header,
			&parent.Created,
			&parent.Updated,
		)

		w.Data = append(w.Data, parent)
	}

	return nil
}

func (i *imageSet) get(articleId int64) error {
	var (
		header    string
		thumbnail string
	)

	err := database.DBCon.QueryRow(
		`
		SELECT
			thumbnail,
			header
		FROM
			articles
		WHERE
			id = ?
		`,
		articleId,
	).Scan(
		&thumbnail,
		&header,
	)

	if err != nil {
		return err
	}

	i.Header, _ = tools.ConvImageToBase64("./images/article/" + header)
	i.Thumbnail, _ = tools.ConvImageToBase64("./images/article/" + thumbnail)

	return nil
}

func (i *imageSet) save(articleId int64) error {
	var err error

	var (
		header    string
		thumbnail string
	)

	database.DBCon.QueryRow(
		`
		SELECT
			header,
			thumbnail
		FROM
			articles
		WHERE
			id = ?
		`,
		articleId,
	).Scan(
		&header,
		&thumbnail,
	)

	if header != "" {
		err = os.Remove("./images/article/" + header)
	}

	if thumbnail != "" {
		err = os.Remove("./images/article/" + thumbnail)
	}

	headerString := tools.RandStringRunes(13) + ".png"
	thumbString := tools.RandStringRunes(13) + ".png"

	if i.Header != "" {
		err = tools.ConvImageToBinary("./images/article/"+headerString, i.Header)
		_, err = database.DBCon.Exec(
			`
			UPDATE
				articles
			SET
				header = ?
			WHERE
				id = ?
			`,
			headerString,
			articleId,
		)
	}

	if i.Thumbnail != "" {
		err = tools.ConvImageToBinary("./images/article/"+thumbString, i.Thumbnail)
		_, err = database.DBCon.Exec(
			`
			UPDATE
				articles
			SET
				thumbnail = ?
			WHERE
				id = ?
			`,
			thumbString,
			articleId,
		)
	}

	if err != nil {
		return err
	}

	return nil
}