package website

import (
	"net/http"
	"github.com/MedFilm/ccms/lib/authentication"
	"github.com/MedFilm/ccms/lib/api/utility"
	"io/ioutil"
)

func GetLanguageFile (w http.ResponseWriter, req *http.Request) {
	// Get permission of requesting user
	permission := authentication.GetPermissions(req, "website")

	if permission == 0 {
		utility.CustomData{}.Unauthorized(w, req)
		return
	}

	file, err := ioutil.ReadFile("./language/locale.js")
	if err != nil {
		utility.SendError(err, w, req)
		return
	}

	w.Header().Set("Content-Type", "text/plain")
	w.Write(file)
}

func SaveLanguageFile (w http.ResponseWriter, req *http.Request) {
	// Get permission of requesting user
	permission := authentication.GetPermissions(req, "website")

	if permission < 2 {
		utility.CustomData{}.Unauthorized(w, req)
		return
	}

	body, err := ioutil.ReadAll(req.Body)
	if err != nil {
		utility.SendError(err, w, req)
		return
	}

	err = ioutil.WriteFile("./language/locale.js", body, 0644)
	if err != nil {
		utility.SendError(err, w, req)
		return
	}

	utility.CustomData{}.OK(w, req)
}