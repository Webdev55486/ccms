package website

import (
	"database/sql"
	"errors"
	"github.com/MedFilm/ccms/lib/api/utility"
	"github.com/MedFilm/ccms/lib/authentication"
	"github.com/MedFilm/ccms/lib/database"
	"github.com/MedFilm/ccms/lib/tools"
	"github.com/gorilla/mux"
	"github.com/nu7hatch/gouuid"
	"github.com/unrolled/render"
	"io"
	"io/ioutil"
	"net/http"
	"os"
)

func GetStaticContent(w http.ResponseWriter, req *http.Request) {
	// Get permission of requesting user
	permission := authentication.GetPermissions(req, "website")

	if permission == 0 {
		utility.CustomData{}.Unauthorized(w, req)
		return
	}

	var (
		identifier string
		language   string
		ok         bool
	)
	// Check for id in params
	vars := mux.Vars(req)
	identifier, ok = vars["id"]
	if !ok {
		utility.SendError(errors.New("No identifier"), w, req)
		return
	}

	language, ok = vars["language"]
	if !ok {
		utility.SendError(errors.New("No language"), w, req)
		return
	}

	var content string

	err := database.PerformQuery(
		`
		SELECT
		  content
		FROM
		  page_content
		WHERE
			identifier = ? AND language = ?
		`,
		func(rows *sql.Rows) {
			if rows.Next() {
				rows.Scan(&content)
			}
		},
		identifier,
		language,
	)

	if err != nil {
		utility.SendError(err, w, req)
		return
	}

	utility.CustomData{
		Data: map[string]interface{}{
			"content": content,
		},
	}.OK(w, req)
}

func SetStaticContent(w http.ResponseWriter, req *http.Request) {
	// Get permission of requesting user
	permission := authentication.GetPermissions(req, "website")

	if permission < 2 {
		utility.CustomData{}.Unauthorized(w, req)
		return
	}

	var (
		identifier string
		language   string
		ok         bool
	)

	// Check for id in params
	vars := mux.Vars(req)
	identifier, ok = vars["id"]
	if !ok {
		utility.SendError(errors.New("No identifier"), w, req)
		return
	}

	language, ok = vars["language"]
	if !ok {
		utility.SendError(errors.New("No language"), w, req)
		return
	}

	object := make(map[string]string)

	err := tools.DecodeJson(&object, req)
	if err != nil {
		utility.SendError(err, w, req)
		return
	}

	var count int
	err = database.DBCon.QueryRow("SELECT count(*) FROM page_content WHERE identifier = ? AND language = ?", identifier, language).Scan(&count)
	if count > 0 {
		err = database.PerformQuery(
			`
			UPDATE
				page_content
			SET
				content = ?
			WHERE
				identifier = ? AND language =?
			`,
			func(rows *sql.Rows) {

			},
			object["content"],
			identifier,
			language,
		)
		if err != nil {
			utility.SendError(err, w, req)
			return
		}
	} else {
		err = database.PerformQuery(
			`
			INSERT INTO
				page_content
				(identifier, language, content)
				VALUES(?, ?, ?)
			`, func(rows *sql.Rows) {},
			identifier,
			language,
			object["content"],
		)
		if err != nil {
			utility.SendError(err, w, req)
			return
		}
	}

	if err != nil {
		utility.SendError(err, w, req)
		return
	}

	utility.CustomData{}.OK(w, req)
}

type image struct {
	Link string `json:"link"`
}

func UploadImage(w http.ResponseWriter, req *http.Request) {
	// Get permission of requesting user
	permission := authentication.GetPermissions(req, "website")

	if permission < 2 {
		utility.CustomData{}.Unauthorized(w, req)
		return
	}

	err := req.ParseMultipartForm(5 * 1024 * 1024)
	if err != nil {
		utility.SendError(err, w, req)
		return
	}

	m := req.MultipartForm.File["image_param"]
	result := image{}

	for i := range m {

		id, err := uuid.NewV4()
		if err != nil {
			utility.SendError(err, w, req)
			return
		}

		dst, err := os.Create("./images/static/" + id.String())
		if err != nil {
			utility.SendError(err, w, req)
			return
		}

		file, err := m[i].Open()
		if err != nil {
			utility.SendError(err, w, req)
			return
		}

		if _, err := io.Copy(dst, file); err != nil {
			utility.SendError(err, w, req)
			return
		}

		result.Link = id.String()
	}

	r := render.New()

	r.JSON(w, http.StatusOK, result)
}

func GetImage(w http.ResponseWriter, req *http.Request) {

	// Check for id in params
	vars := mux.Vars(req)
	identifier, ok := vars["id"]
	if !ok {
		utility.SendError(errors.New("No identifier"), w, req)
		return
	}

	file, err := ioutil.ReadFile("./images/static/" + identifier)
	if err != nil {
		utility.SendError(err, w, req)
		return
	}

	w.Header().Set("Content-Type", "image/png")
	w.Write(file)
}
