package website

import (
	"database/sql"
	"errors"
	"github.com/MedFilm/ccms/lib/api/utility"
	"github.com/MedFilm/ccms/lib/authentication"
	"github.com/MedFilm/ccms/lib/database"
	"github.com/MedFilm/ccms/lib/tools"
	"github.com/gorilla/mux"
	"net/http"
	"strconv"
)

func GetArticles(w http.ResponseWriter, req *http.Request) {
	var err error

	// Get permission of requesting user
	permission := authentication.GetPermissions(req, "website")

	if permission == 0 {
		utility.CustomData{}.Unauthorized(w, req)
		return
	}

	// Check for id in params
	vars := mux.Vars(req)
	Type, ok := vars["type"]
	if !ok {
		utility.SendError(errors.New("No type"), w, req)
		return
	}

	Language, ok := vars["language"]
	if !ok {
		utility.SendError(errors.New("No type"), w, req)
		return
	}

	var result wrapArticles

	err = database.PerformQuery(
		`
		SELECT
			id,
			published,
			type,
			language,
			title,
			slug,
			description,
			blurb,
			text,
			thumbnail,
			header,
			created,
			updated
		FROM
			articles
		WHERE
			published = 1 AND type = ? AND language = ?
		ORDER BY
			created DESC
		`,
		func(rows *sql.Rows) {
			result.insertData(rows)
		},
		Type,
		Language,
	)

	if err != nil {
		utility.SendError(err, w, req)
		return
	}

	utility.CustomData{Data: map[string]interface{}{
		"result": result.Data,
	}}.OK(w, req)
}

func GetArticle(w http.ResponseWriter, req *http.Request) {
	// Get permission of requesting user
	permission := authentication.GetPermissions(req, "website")

	if permission == 0 {
		utility.CustomData{}.Unauthorized(w, req)
		return
	}

	// Check for id in params
	vars := mux.Vars(req)
	id, ok := vars["id"]
	if !ok {
		utility.SendError(errors.New("No ID"), w, req)
		return
	}

	var result article

	err := database.DBCon.QueryRow(
		`
		SELECT
			id,
			published,
			type,
			language,
			title,
			slug,
			description,
			blurb,
			text,
			thumbnail,
			header,
			created,
			updated
		FROM
			articles
		WHERE
			id = ?
		`,
		id,
	).Scan(
		&result.Id,
		&result.Published,
		&result.Type,
		&result.Language,
		&result.Title,
		&result.Slug,
		&result.Description,
		&result.Blurb,
		&result.Text,
		&result.Thumbnail,
		&result.Header,
		&result.Created,
		&result.Updated,
	)

	if err != nil {
		utility.SendError(err, w, req)
		return
	}

	utility.CustomData{Data: map[string]interface{}{
		"result": result,
	}}.OK(w, req)
}

func GetImageSet(w http.ResponseWriter, req *http.Request) {
	// Get permission of requesting user
	permission := authentication.GetPermissions(req, "website")

	if permission == 0 {
		utility.CustomData{}.Unauthorized(w, req)
		return
	}

	// Check for id in params
	vars := mux.Vars(req)
	id, ok := vars["id"]
	if !ok {
		utility.SendError(errors.New("No ID"), w, req)
		return
	}

	realId, err := strconv.ParseInt(id, 10, 64)
	if err != nil {
		utility.SendError(err, w, req)
		return
	}

	var set imageSet

	set.get(realId)

	utility.CustomData{Data: map[string]interface{}{
		"result": set,
	}}.OK(w, req)
}

func SaveArticle(w http.ResponseWriter, req *http.Request) {
	// Get permission of requesting user
	permission := authentication.GetPermissions(req, "website")

	if permission < 2 {
		utility.CustomData{}.Unauthorized(w, req)
		return
	}

	obj := wrapArticle{
		article{},
	}

	err := tools.DecodeJson(&obj, req)
	if err != nil {
		utility.SendError(err, w, req)
		return
	}

	if obj.Data.Id == 0 {
		query, err := database.DBCon.Exec(
			`
			INSERT INTO
				articles
			(
				published,
				type,
				language,
				title,
				slug,
				description,
				blurb,
				text
			)
			VALUES(?, ?, ?, ?, ?, ?, ?, ?)
			`,
			obj.Data.Published,
			obj.Data.Type,
			obj.Data.Language,
			obj.Data.Title,
			obj.Data.Slug,
			obj.Data.Description,
			obj.Data.Blurb,
			obj.Data.Text,
		)
		if err != nil {
			utility.SendError(err, w, req)
			return
		}

		obj.Data.Id, _ = query.LastInsertId()
	} else {
		err = database.PerformQuery(
			`
			UPDATE
				articles
			SET
				published = ?,
				type = ?,
				language = ?,
				title = ?,
				slug = ?,
				description = ?,
				blurb = ?,
				text = ?
			WHERE
				id=?
			`,
			func(rows *sql.Rows) {},
			obj.Data.Published,
			obj.Data.Type,
			obj.Data.Language,
			obj.Data.Title,
			obj.Data.Slug,
			obj.Data.Description,
			obj.Data.Blurb,
			obj.Data.Text,
			obj.Data.Id,
		)
		if err != nil {
			utility.SendError(err, w, req)
			return
		}
	}

	utility.CustomData{Data: map[string]interface{}{
		"result": obj.Data,
	}}.OK(w, req)
}

func SaveImageSet(w http.ResponseWriter, req *http.Request) {
	// Get permission of requesting user
	permission := authentication.GetPermissions(req, "website")

	if permission == 0 {
		utility.CustomData{}.Unauthorized(w, req)
		return
	}

	// Check for id in params
	vars := mux.Vars(req)
	idStr, ok := vars["id"]
	if !ok {
		utility.SendError(errors.New("No ID"), w, req)
		return
	}

	id, err := strconv.ParseInt(idStr, 10, 64)
	if err != nil {
		utility.SendError(err, w, req)
		return
	}

	obj := wrapImageSet{
		imageSet{},
	}

	err = tools.DecodeJson(&obj, req)
	if err != nil {
		utility.SendError(err, w, req)
		return
	}

	var count int

	err = database.PerformQuery(
		`
		SELECT
			COUNT(*)
		FROM
			articles
		WHERE
			id = ?
		`,
		func(rows *sql.Rows) {
			for rows.Next() {
				rows.Scan(&count)
			}
		},
		id,
	)

	if err != nil || count == 0 {
		utility.SendError(errors.New("Article not found"), w, req)
		return
	}

	obj.Data.save(id)

	utility.CustomData{}.OK(w, req)
}
