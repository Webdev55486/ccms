package website

import (
	"net/http"
	"github.com/MedFilm/ccms/lib/authentication"
	"github.com/MedFilm/ccms/lib/api/utility"
	"database/sql"
	"github.com/MedFilm/ccms/lib/tools"
	"github.com/MedFilm/ccms/lib/database"
)

func GetLogotypes(w http.ResponseWriter, req *http.Request) {

	// Get permission of requesting user
	permission := authentication.GetPermissions(req, "website")

	if permission == 0 {
		utility.CustomData{}.Unauthorized(w, req)
		return
	}

	result := wrapLogotypes{}

	err := database.PerformQuery(
		`
		SELECT
			id,
			company_logo,
			logotype_order,
			logotype_active
		FROM
			users
		WHERE
			company_logo IS NOT NULL AND company_logo != ''
		`,
		func (rows *sql.Rows) {
			for rows.Next() {
				tmp := Logotypes{}
				rows.Scan(
					&tmp.Id,
					&tmp.Logo,
					&tmp.Order,
					&tmp.Active,
				)

				result.Data = append(result.Data, tmp)
			}
		},
	)

	if err != nil {
		utility.SendError(err, w, req)
		return
	}

	for k, v := range result.Data {
		result.Data[k].Logo, err = tools.ConvImageToBase64("./images/logos/" + v.Logo)
		if err != nil {
			utility.SendError(err, w, req)
			return
		}
	}

	utility.CustomData{
		Data: map[string]interface{}{
			"logotypes": result,
		},
	}.OK(w, req)
}

func SaveLogotypes(w http.ResponseWriter, req *http.Request) {
	// Get permission of requesting user
	permission := authentication.GetPermissions(req, "website")

	if permission < 2 {
		utility.CustomData{}.Unauthorized(w, req)
		return
	}

	obj := wrapLogotypes{}

	err := tools.DecodeJson(&obj, req)
	if err != nil {
		utility.SendError(err, w, req)
		return
	}

	for _, v := range obj.Data {
		err = database.PerformQuery(
			`
			UPDATE
				users
			SET
				logotype_order = ?,
				logotype_active = ?
			WHERE
				id = ?
			`,
			func (rows *sql.Rows) {},
			v.Order,
			v.Active,
			v.Id,
		)

		if err != nil {
			utility.SendError(err, w, req)
			return
		}
	}

	utility.CustomData{}.OK(w, req)
}