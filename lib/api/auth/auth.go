/**
 * medfilm-ccms
 * Copyright (C) Sunstate AB - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Simon Wikstrand <simon@sunstate.se>, 2016-12-15
 */

package auth

import (
	"database/sql"
	"encoding/json"
	"errors"
	"github.com/MedFilm/ccms/lib/api/utility"
	"github.com/MedFilm/ccms/lib/authentication"
	"github.com/MedFilm/ccms/lib/database"
	"github.com/MedFilm/ccms/lib/email"
	"github.com/MedFilm/ccms/lib/tools"
	"golang.org/x/crypto/bcrypt"
	"net/http"
)

func Login(w http.ResponseWriter, req *http.Request) {

	var userData authentication.UserCredentials

	err := json.NewDecoder(req.Body).Decode(&userData)

	if err != nil {
		utility.CustomData{
			Data: map[string]interface{}{
				"errorMessage": err.Error(),
			},
		}.PreconditionFailed(w, req)
		return
	}

	tokenString, err := authentication.SignToken(userData)

	if err != nil {
		utility.CustomData{
			Data: map[string]interface{}{
				"errorMessage": err.Error(),
			},
		}.PreconditionFailed(w, req)
		return
	}

	utility.CustomData{
		Data: map[string]interface{}{
			"Token": tokenString,
		},
	}.OK(w, req)
}

func ValidateToken(w http.ResponseWriter, req *http.Request) {
	utility.CustomData{}.OK(w, req)
}

func ValidatePassword(w http.ResponseWriter, req *http.Request) {
	var cred authentication.UserCredentials

	err := tools.DecodeJson(&cred, req)
	if err != nil {
		utility.SendError(err, w, req)
		return
	}

	err = authentication.ValidatePassword(cred)

	if err != nil {
		utility.SendError(errors.New("Incorrect password"), w, req)
		return
	}

	utility.CustomData{}.OK(w, req)
}

func ResetPassword(w http.ResponseWriter, req *http.Request) {
	var cred authentication.UserCredentials
	var err error

	tools.DecodeJson(&cred, req)
	if err != nil {
		utility.SendError(err, w, req)
		return
	}

	var id int64

	err = database.DBCon.QueryRow(
		`
		SELECT
			id
		FROM
			employees
		WHERE
			email=?
		`,
		cred.Email,
	).Scan(&id)

	hash := tools.RandStringRunes(15)

	err = database.PerformQuery(
		`
		INSERT INTO
			employee_reset
			(
				employee_id,
				hash
			)
			VALUES(?,?)
		`,
		func(rows *sql.Rows) {},
		id,
		hash,
	)
	if err != nil {
		utility.SendError(err, w, req)
		return
	}

	email.SendResetPassword(cred.Email, hash)

	utility.CustomData{}.OK(w, req)
}

func SetPassword(w http.ResponseWriter, req *http.Request) {
	var password struct {
		Hash     string
		Password string
	}
	var err error

	err = tools.DecodeJson(&password, req)
	if err != nil {
		utility.SendError(err, w, req)
		return
	}

	var id, employee int64

	err = database.DBCon.QueryRow(
		`
		SELECT
			id,
			employee_id
		FROM
			employee_reset
		WHERE
			hash=?
		`,
		password.Hash,
	).Scan(&id, &employee)

	if err != nil {
		utility.SendError(err, w, req)
		return
	}

	hashedPassword, err := bcrypt.GenerateFromPassword([]byte(password.Password), bcrypt.DefaultCost)

	err = database.PerformQuery(
		`
		UPDATE
			employees
		SET
			password=?
		WHERE
			id=?
		`,
		func(rows *sql.Rows) {},
		hashedPassword,
		employee,
	)
	if err != nil {
		utility.SendError(err, w, req)
		return
	}

	err = database.PerformQuery(
		`
		DELETE FROM
			employee_reset
		WHERE
			id=?
		`,
		func(rows *sql.Rows) {},
		id,
	)

	if err != nil {
		utility.SendError(err, w, req)
		return
	}

	utility.CustomData{}.OK(w, req)
}
