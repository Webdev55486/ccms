/**
 * medfilm-ccms
 * Copyright (C) Sunstate AB - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Simon Wikstrand <simon@sunstate.se>, 2016-12-23
 */

package admin

import (
	"database/sql"
	"errors"
	"github.com/MedFilm/ccms/lib/api/utility"
	"github.com/MedFilm/ccms/lib/authentication"
	"github.com/MedFilm/ccms/lib/database"
	"github.com/MedFilm/ccms/lib/tools"
	"github.com/gorilla/mux"
	"golang.org/x/crypto/bcrypt"
	"net/http"
	"strconv"
)

type User struct {
	ID         int64            `json:"id"`
	Email      string           `json:"email"`
	Name       string           `json:"name"`
	Password   string           `json:"password,omitempty"`
	Permission map[string]int64 `json:"permissions"`
}

func GetUser(w http.ResponseWriter, req *http.Request) {

	// Get permission of requesting user
	permission := authentication.GetPermissions(req, "admin")

	if permission == 0 {
		utility.CustomData{}.Unauthorized(w, req)
		return
	}

	// Fill user struct
	var users []User

	err := database.PerformQuery("SELECT id, given_name, email FROM employees", func(rows *sql.Rows) {
		for rows.Next() {
			var singleUser = User{
				Permission: make(map[string]int64),
			}

			rows.Scan(&singleUser.ID, &singleUser.Name, &singleUser.Email)

			database.PerformQuery("SELECT permission, level FROM employee_permission WHERE emp_id=?",
				func(rows *sql.Rows) {
					for rows.Next() {
						var (
							permission string
							level      int64
						)

						rows.Scan(&permission, &level)

						singleUser.Permission[permission] = level
					}
					users = append(users, singleUser)
				},
				singleUser.ID,
			)
		}
	})

	if err != nil {
		utility.CustomData{
			Data: map[string]interface{}{
				"errorMessage": err.Error(),
			},
		}.NotFound(w, req)
		return
	}

	utility.CustomData{Data: map[string]interface{}{
		"result": users,
	}}.OK(w, req)
}

func GetSingleUser(w http.ResponseWriter, req *http.Request) {

	// Get permission of requesting user
	permission := authentication.GetPermissions(req, "admin")

	if permission == 0 {
		utility.CustomData{}.Unauthorized(w, req)
		return
	}

	// Get variables from URL
	vars := mux.Vars(req)

	// Convert ID to int64
	id, err := strconv.ParseInt(vars["id"], 10, 64)
	if err != nil {
		utility.CustomData{
			Data: map[string]interface{}{
				"errorMessage": err.Error(),
			},
		}.NotFound(w, req)
		return
	}

	// If it's zero, error
	if id == 0 {
		utility.CustomData{
			Data: map[string]interface{}{
				"errorMessage": "Invalid ID",
			},
		}.PreconditionFailed(w, req)
		return
	}

	var user User

	user.ID = id

	err = database.DBCon.QueryRow(
		"SELECT given_name, email FROM employees WHERE id=?",
		id,
	).Scan(&user.Name, &user.Email)

	if err != nil {
		utility.CustomData{
			Data: map[string]interface{}{
				"errorMessage": err.Error(),
			},
		}.NotFound(w, req)
		return
	}

	user.Permission = make(map[string]int64)
	err = database.PerformQuery(
		"SELECT permission, level FROM employee_permission WHERE emp_id=?",
		func(rows *sql.Rows) {
			for rows.Next() {
				var (
					permission string
					level      int64
				)

				rows.Scan(&permission, &level)

				user.Permission[permission] = level
			}
		},
		id,
	)

	if err != nil {
		utility.CustomData{
			Data: map[string]interface{}{
				"errorMessage": err.Error(),
			},
		}.InternalServerError(w, req)
		return
	}

	// Return result
	utility.CustomData{Data: map[string]interface{}{
		"result": user,
	}}.OK(w, req)
}

func SaveUser(w http.ResponseWriter, req *http.Request) {

	// Get permission of requesting user
	permission := authentication.GetPermissions(req, "admin")

	// Require edit level
	if permission != 2 {
		utility.CustomData{}.Unauthorized(w, req)
		return
	}

	var user User
	getUserId(&user, req)

	// Get user from request
	err := tools.DecodeJson(&user, req)
	if user.ID == 0 {
		err = insertUser(user)
		if err != nil {
			utility.CustomData{
				Data: map[string]interface{}{
					"errorMessage": err.Error(),
				},
			}.InternalServerError(w, req)
			return
		}
	} else {
		err = updateUser(user)
		if err != nil {
			utility.CustomData{
				Data: map[string]interface{}{
					"errorMessage": err.Error(),
				},
			}.InternalServerError(w, req)
			return
		}
	}

	// Return result
	utility.CustomData{}.OK(w, req)
}

func DeleteUser(w http.ResponseWriter, req *http.Request) {

	// Get permission of requesting user
	permission := authentication.GetPermissions(req, "admin")

	// Require edit level
	if permission != 2 {
		utility.CustomData{}.Unauthorized(w, req)
		return
	}

	var user User

	getUserId(&user, req)

	// Get user from request
	err := tools.DecodeJson(&user, req)
	if err != nil {
		utility.CustomData{
			Data: map[string]interface{}{
				"errorMessage": err.Error(),
			},
		}.InternalServerError(w, req)
		return
	}

	err = deleteUser(user)
	if err != nil {
		utility.CustomData{
			Data: map[string]interface{}{
				"errorMessage": err.Error(),
			},
		}.InternalServerError(w, req)
		return
	}

	// Return result
	utility.CustomData{}.OK(w, req)
}

func insertUser(user User) error {
	var hashedPassword []byte
	var err error

	if len(user.Email) < 4 || len(user.Name) < 4 {
		return errors.New("Name or email is too short")
	}

	if user.Password != "" {
		hashedPassword, err = bcrypt.GenerateFromPassword([]byte(user.Password), bcrypt.DefaultCost)
		if err != nil {
			return err
		}
	}

	stmt, err := database.DBCon.Prepare("INSERT INTO employees (email, given_name, password) VALUES(?, ?, ?)")
	if err != nil {
		return err
	}

	res, err := stmt.Exec(user.Email, user.Name, string(hashedPassword))
	if err != nil {
		return err
	}

	user.ID, err = res.LastInsertId()
	if err != nil {
		return err
	}

	insertPermissions(user)

	return nil
}

func updateUser(user User) error {

	var hashedPassword []byte
	var err error

	if len(user.Email) < 4 || len(user.Name) < 4 {
		return errors.New("Name or email is too short")
	}

	if user.Password != "" {
		hashedPassword, err = bcrypt.GenerateFromPassword([]byte(user.Password), bcrypt.DefaultCost)
		if err != nil {
			return err
		}

		err = database.PerformQuery(
			"UPDATE employees SET email=?,given_name=?,password=? WHERE id=?",
			func(rows *sql.Rows) {},
			user.Email,
			user.Name,
			string(hashedPassword),
			user.ID,
		)
		if err != nil {
			return err
		}
	} else {
		err = database.PerformQuery(
			"UPDATE employees SET email=?,given_name=? WHERE id=?",
			func(rows *sql.Rows) {},
			user.Email,
			user.Name,
			user.ID,
		)
		if err != nil {
			return err
		}
	}

	insertPermissions(user)

	return nil
}

func deleteUser(user User) error {
	err := database.PerformQuery(
		"DELETE FROM employees WHERE id=?",
		func(rows *sql.Rows) {},
		user.ID,
	)
	if err != nil {
		return err
	}
	return nil
}

func insertPermissions(user User) error {

	for name, value := range user.Permission {
		stmt, err := database.DBCon.Prepare("REPLACE INTO employee_permission (emp_id, permission, level) VALUES(?, ?, ?)")
		if err == sql.ErrNoRows {
			stmt.Close()
		}

		if err != nil {
			return err
		}

		_, err = stmt.Exec(user.ID, name, value)
		if err != nil {
			return err
		}
		stmt.Close()
	}

	return nil
}

func getUserId(user *User, req *http.Request) {

	// Check for id in params
	vars := mux.Vars(req)
	if val, ok := vars["id"]; ok {
		user.ID, _ = strconv.ParseInt(val, 10, 64)
	}
}
