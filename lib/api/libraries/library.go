package libraries

import (
	"database/sql"
	"github.com/MedFilm/ccms/lib/api/utility"
	"github.com/MedFilm/ccms/lib/authentication"
	"github.com/MedFilm/ccms/lib/database"
	"github.com/MedFilm/ccms/lib/tools"
	"github.com/nu7hatch/gouuid"
	"net/http"
	"os"
)

func GetLibraries(w http.ResponseWriter, req *http.Request) {
	// Get permission of requesting user
	permission := authentication.GetPermissions(req, "libraries")
	if permission == 0 {
		utility.CustomData{}.Unauthorized(w, req)
		return
	}

	var result []Library

	err := database.PerformQuery(
		`
		SELECT
			fs.id,
			fsl.name,
			fs.type,
			fs.parent_id,
			fs.created,
			fs.updated
		FROM
			film_subject fs
		INNER JOIN
			film_subject_languages fsl ON fs.id = fsl.film_subject_id AND fsl.language = 'en'
		`,
		func(rows *sql.Rows) {
			for rows.Next() {
				tmpLib := Library{}
				rows.Scan(
					&tmpLib.Id,
					&tmpLib.Name,
					&tmpLib.Type,
					&tmpLib.ParentId,
					&tmpLib.Created,
					&tmpLib.Updated,
				)

				result = append(result, tmpLib)
			}
		},
	)

	if err != nil {
		utility.SendError(err, w, req)
		return
	}

	utility.CustomData{
		Data: map[string]interface{}{
			"libraries": result,
		},
	}.OK(w, req)
}

func GetLibrary(w http.ResponseWriter, req *http.Request) {
	// Get permission of requesting user
	permission := authentication.GetPermissions(req, "libraries")
	if permission == 0 {
		utility.CustomData{}.Unauthorized(w, req)
		return
	}

	id, err := tools.GetIdFromUrl(req)
	if err != nil {
		utility.SendError(err, w, req)
		return
	}

	var result Library

	err = database.DBCon.QueryRow(
		`
		SELECT
			id,
			name,
			hash,
			type,
			parent_id,
			created,
			updated
		FROM
			film_subject
		WHERE
			id = ?
		`,
		id,
	).Scan(
		&result.Id,
		&result.Name,
		&result.Hash,
		&result.Type,
		&result.ParentId,
		&result.Created,
		&result.Updated,
	)

	if err != nil {
		utility.SendError(err, w, req)
		return
	}

	err = database.PerformQuery(
		`
		SELECT
			id,
			film_subject_id,
			language,
			name,
			created,
			updated
		FROM
			film_subject_languages
		WHERE
			film_subject_id = ?
		`,
		func(rows *sql.Rows) {
			for rows.Next() {
				var language Language
				rows.Scan(
					&language.Id,
					&language.ParentId,
					&language.Language,
					&language.Name,
					&language.Created,
					&language.Updated,
				)

				result.Languages = append(result.Languages, language)
			}
		},
		result.Id,
	)

	if err != nil {
		utility.SendError(err, w, req)
		return
	}

	result.Image, err = tools.ConvImageToBase64("./images/library/" + result.Hash)

	if err != nil && !os.IsNotExist(err) {
		utility.SendError(err, w, req)
		return
	}

	utility.CustomData{
		Data: map[string]interface{}{
			"library": result,
		},
	}.OK(w, req)
}

func SaveLibrary(w http.ResponseWriter, req *http.Request) {
	// Get permission of requesting user
	permission := authentication.GetPermissions(req, "libraries")
	if permission < 2 {
		utility.CustomData{}.Unauthorized(w, req)
		return
	}

	var lib libraryWrap

	id, err := tools.GetIdFromUrl(req)
	if err != nil {
		utility.SendError(err, w, req)
		return
	}

	// Get user from request
	err = tools.DecodeJson(&lib, req)
	if err != nil {
		utility.SendError(err, w, req)
		return
	}

	err = database.PerformQuery(
		`
		UPDATE
			film_subject
		SET
			name = ?,
			type = ?,
			parent_id = ?
		WHERE
			id = ?
		`,
		func(rows *sql.Rows) {},
		lib.Library.Name,
		lib.Library.Type,
		lib.Library.ParentId,
		id,
	)

	if err != nil {
		utility.SendError(err, w, req)
		return
	}

	for _, v := range lib.Library.Languages {
		if v.Id == 0 {
			err = v.insertFilmSubjectLanguage(id)
			if err != nil {
				utility.SendError(err, w, req)
				return
			}
		} else {
			err = v.updateFilmSubjectLanguage()
			if err != nil {
				utility.SendError(err, w, req)
				return
			}
		}
	}

	if lib.Library.Image != "" {
		err = tools.ConvImageToBinary("./images/library/"+lib.Library.Hash, lib.Library.Image)
		if err != nil {
			utility.SendError(err, w, req)
			return
		}
	}

	utility.CustomData{}.OK(w, req)
}

func RemoveLibrary(w http.ResponseWriter, req *http.Request) {
	// Get permission of requesting user
	permission := authentication.GetPermissions(req, "libraries")
	if permission < 2 {
		utility.CustomData{}.Unauthorized(w, req)
		return
	}

	id, err := tools.GetIdFromUrl(req)
	if err != nil {
		utility.SendError(err, w, req)
		return
	}

	err = database.PerformQuery(
		`
		DELETE FROM
			film_subject
		WHERE
			id = ?
		`,
		func(rows *sql.Rows) {},
		id,
	)

	if err != nil {
		utility.SendError(err, w, req)
		return
	}

	utility.CustomData{}.OK(w, req)
}

func NewLibrary(w http.ResponseWriter, req *http.Request) {
	// Get permission of requesting user
	permission := authentication.GetPermissions(req, "libraries")
	if permission < 2 {
		utility.CustomData{}.Unauthorized(w, req)
		return
	}

	var lib libraryWrap

	// Get user from request
	err := tools.DecodeJson(&lib, req)
	if err != nil {
		utility.SendError(err, w, req)
		return
	}

	if *lib.Library.ParentId == 0 {
		lib.Library.ParentId = nil
		lib.Library.Type = "library"
	} else {
		lib.Library.Type = "category"
	}

	str, err := uuid.NewV4()
	if err != nil {
		utility.SendError(err, w, req)
		return
	}
	lib.Library.Hash = str.String()

	result, err := database.DBCon.Exec(
		`
		INSERT INTO
			film_subject
			(name, hash, type, parent_id)
		VALUES(?, ?, ?, ?)
		`,
		lib.Library.Name,
		lib.Library.Hash,
		lib.Library.Type,
		lib.Library.ParentId,
	)

	if err != nil {
		utility.SendError(err, w, req)
		return
	}

	id, err := result.LastInsertId()

	lang := Language{
		Language: "en",
		Name:     lib.Library.Name,
	}

	err = lang.insertFilmSubjectLanguage(id)

	if err != nil {
		utility.SendError(err, w, req)
		return
	}

	utility.CustomData{}.OK(w, req)
}

func NewLibraryLanguage(w http.ResponseWriter, req *http.Request) {
	// Get permission of requesting user
	permission := authentication.GetPermissions(req, "libraries")
	if permission < 2 {
		utility.CustomData{}.Unauthorized(w, req)
		return
	}

	id, err := tools.GetIdFromUrl(req)
	if err != nil {
		utility.SendError(err, w, req)
		return
	}

	var lang Language

	lang.ParentId = id

	// Get user from request
	err = tools.DecodeJson(&lang, req)
	if err != nil {
		utility.SendError(err, w, req)
		return
	}

	result, err := database.DBCon.Exec(
		`
		INSERT INTO
			film_subject_languages (film_subject_id, language, name)
			VALUES(?, ?, ?)
		`,
		lang.ParentId,
		lang.Language,
		lang.Name,
	)
	if err != nil {
		utility.SendError(err, w, req)
		return
	}

	lang.Id, err = result.LastInsertId()
	if err != nil {
		utility.SendError(err, w, req)
		return
	}

	utility.CustomData{
		Data: map[string]interface{}{
			"language": lang,
		},
	}.OK(w, req)
}
