package libraries

import (
	"database/sql"
	"fmt"
	"github.com/MedFilm/ccms/lib/database"
)

func NewAvailability() Availability {
	a := Availability{}
	a["highResolution"] = 0
	a["midResolution"] = 0
	a["lowResolution"] = 0
	a["preview"] = 0
	a["previewSubtitle"] = 0
	a["smallThumbnail"] = 0
	a["normalThumbnail"] = 0
	a["chapter"] = 0
	a["subtitle"] = 0
	a["showCase1"] = 0
	a["showCase2"] = 0
	a["showCase3"] = 0
	return a
}

func (a *Availability) CheckFilmStatus(id int64, language string) Availability {
	err := database.PerformQuery(
		`
		SELECT
			name
		FROM
			film_files
		WHERE
			language = ? AND film_id = ? AND subscription_id IS NULL
		`,
		func(rows *sql.Rows) {
			var name string
			for rows.Next() {
				rows.Scan(&name)
				(*a)[name] = 1
			}
		},
		language,
		id,
	)

	if err != nil {
		fmt.Println(err)
	}
	return (*a)
}

func (a *Availability) CheckSubscriptionStatus(filmId int64, subscriptionId int64, language string) Availability {
	err := database.PerformQuery(
		`
		SELECT
			name,
			subscription_id
		FROM
			film_files
		WHERE
			language = ? AND film_id = ? AND (subscription_id = ? OR subscription_id IS NULL)
		`,
		func(rows *sql.Rows) {
			var name string
			var subId *int64
			for rows.Next() {
				rows.Scan(
					&name,
					&subId,
				)
				if subId == nil && (*a)[name] == 0 {
					(*a)[name] = 2
				} else {
					(*a)[name] = 1
				}
			}
		},
		language,
		filmId,
		subscriptionId,
	)

	if err != nil {
		fmt.Println(err)
	}

	return (*a)
}
