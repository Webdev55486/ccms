package libraries

import (
	"database/sql"
	"errors"
	"github.com/MedFilm/ccms/lib/api/utility"
	"github.com/MedFilm/ccms/lib/authentication"
	"github.com/MedFilm/ccms/lib/database"
	"github.com/MedFilm/ccms/lib/tools"
	"net/http"
	"net/url"
)

func GetFilms(w http.ResponseWriter, req *http.Request) {
	// Get permission of requesting user
	permission := authentication.GetPermissions(req, "prospects")
	if permission == 0 {
		utility.CustomData{}.Unauthorized(w, req)
		return
	}

	var result []Film

	err := database.PerformQuery(
		`
		SELECT
		  f.id,
		  fl.name,
		  f.file_hash,
		  f.subject_id,
		  f.is_public,
		  f.full_public,
		  f.views + (
			SELECT
			  COUNT(*)
			FROM
			  film_views
			WHERE
			  film_id = f.id
		  ) as views,
		  f.created,
		  f.updated
		FROM
		  film f
		INNER JOIN
		  film_languages fl ON f.id = fl.film_id AND fl.language = 'en' AND original = 1
		`,
		func(rows *sql.Rows) {
			for rows.Next() {
				tmpFilm := Film{}
				var err error
				rows.Scan(
					&tmpFilm.Id,
					&tmpFilm.Name,
					&tmpFilm.FileHash,
					&tmpFilm.SubjectId,
					&tmpFilm.IsPublic,
					&tmpFilm.FullPublic,
					&tmpFilm.Views,
					&tmpFilm.Created,
					&tmpFilm.Updated,
				)

				tmpFilm.Name, err = url.QueryUnescape(tmpFilm.Name)
				if err != nil {
					continue
				}

				result = append(result, tmpFilm)
			}
		},
	)

	if err != nil {
		utility.SendError(err, w, req)
		return
	}

	utility.CustomData{
		Data: map[string]interface{}{
			"films": result,
		},
	}.OK(w, req)
}

func GetFilm(w http.ResponseWriter, req *http.Request) {
	// Get permission of requesting user
	permission := authentication.GetPermissions(req, "prospects")
	if permission == 0 {
		utility.CustomData{}.Unauthorized(w, req)
		return
	}

	id, err := tools.GetIdFromUrl(req)
	if err != nil {
		utility.SendError(err, w, req)
		return
	}

	var result Film

	err = database.DBCon.QueryRow(
		`
		SELECT
		  fs.id,
		  fs.name,
		  fs.file_hash,
		  fs.subject_id,
		  fs.is_public,
		  fs.full_public,
		  fs.views + (
			SELECT
			  COUNT(*)
			FROM
			  film_views
			WHERE
			  film_id = fs.id
		  ) as views,
		  fs.created,
		  fs.updated
		FROM
		  film fs
		WHERE
		  fs.id = ?
		`,
		id,
	).Scan(
		&result.Id,
		&result.Name,
		&result.FileHash,
		&result.SubjectId,
		&result.IsPublic,
		&result.FullPublic,
		&result.Views,
		&result.Created,
		&result.Updated,
	)

	if err != nil {
		utility.SendError(err, w, req)
		return
	}

	err = database.PerformQuery(
		`
		SELECT
			id,
			film_id,
			language,
			name,
			description,
			created,
			updated
		FROM
			film_languages
		WHERE
			film_id = ? AND original = 1
		`,
		func(rows *sql.Rows) {
			for rows.Next() {
				var language Language
				rows.Scan(
					&language.Id,
					&language.ParentId,
					&language.Language,
					&language.Name,
					&language.Description,
					&language.Created,
					&language.Updated,
				)

				language.Name, err = url.QueryUnescape(language.Name)
				if err != nil {
					continue
				}

				language.Availability = NewAvailability()
				language.Availability.CheckFilmStatus(result.Id, language.Language)
				result.Languages = append(result.Languages, language)
			}
		},
		result.Id,
	)

	if err != nil {
		utility.SendError(err, w, req)
		return
	}

	utility.CustomData{
		Data: map[string]interface{}{
			"film": result,
		},
	}.OK(w, req)
}

func SaveFilm(w http.ResponseWriter, req *http.Request) {
	// Get permission of requesting user
	permission := authentication.GetPermissions(req, "libraries")
	if permission < 2 {
		utility.CustomData{}.Unauthorized(w, req)
		return
	}

	var film filmWrap

	id, err := tools.GetIdFromUrl(req)
	if err != nil {
		utility.SendError(err, w, req)
		return
	}

	// Get user from request
	err = tools.DecodeJson(&film, req)
	if err != nil {
		utility.SendError(err, w, req)
		return
	}

	err = database.PerformQuery(
		`
		UPDATE
			film
		SET
			name = ?,
			file_hash = ?,
			subject_id = ?,
			is_public = ?,
			full_public = ?
		WHERE
			id = ?
		`,
		func(rows *sql.Rows) {},
		film.Film.Name,
		film.Film.FileHash,
		film.Film.SubjectId,
		film.Film.IsPublic,
		film.Film.FullPublic,
		id,
	)

	if err != nil {
		utility.SendError(err, w, req)
		return
	}

	for _, v := range film.Film.Languages {
		if v.Id == 0 {
			err = v.insertFilmLanguage(id)
			if err != nil {
				utility.SendError(err, w, req)
				return
			}
		} else {
			err = v.updateFilmLanguage()
			if err != nil {
				utility.SendError(err, w, req)
				return
			}
		}
	}

	utility.CustomData{}.OK(w, req)
}

func RemoveFilm(w http.ResponseWriter, req *http.Request) {
	// Get permission of requesting user
	permission := authentication.GetPermissions(req, "libraries")
	if permission < 2 {
		utility.CustomData{}.Unauthorized(w, req)
		return
	}

	id, err := tools.GetIdFromUrl(req)
	if err != nil {
		utility.SendError(err, w, req)
		return
	}

	err = database.PerformQuery(
		`
		DELETE FROM
			film
		WHERE
			id = ?
		`,
		func(rows *sql.Rows) {},
		id,
	)

	if err != nil {
		utility.SendError(err, w, req)
		return
	}

	utility.CustomData{}.OK(w, req)
}

func NewFilm(w http.ResponseWriter, req *http.Request) {
	// Get permission of requesting user
	permission := authentication.GetPermissions(req, "libraries")
	if permission < 2 {
		utility.CustomData{}.Unauthorized(w, req)
		return
	}

	var film filmWrap

	// Get user from request
	err := tools.DecodeJson(&film, req)
	if err != nil {
		utility.SendError(err, w, req)
		return
	}

	if film.Film.SubjectId == 0 {
		utility.SendError(errors.New("Subject Id not given"), w, req)
		return
	}

	film.Film.FileHash = tools.RandStringRunes(15)

	result, err := database.DBCon.Exec(
		`
		INSERT INTO
			film
			(name, file_hash, subject_id)
		VALUES(?, ?, ?)
		`,
		film.Film.Name,
		film.Film.FileHash,
		film.Film.SubjectId,
	)

	if err != nil {
		utility.SendError(err, w, req)
		return
	}

	id, err := result.LastInsertId()
	if err != nil {
		utility.SendError(err, w, req)
		return
	}

	_, err = database.DBCon.Exec(
		`
		INSERT INTO
			film_languages (film_id, language, name)
		VALUES (?, ?, ?)
		`,
		id,
		"en",
		film.Film.Name,
	)

	if err != nil {
		utility.SendError(err, w, req)
		return
	}

	utility.CustomData{}.OK(w, req)
}

func NewFilmLanguage(w http.ResponseWriter, req *http.Request) {
	// Get permission of requesting user
	permission := authentication.GetPermissions(req, "libraries")
	if permission < 2 {
		utility.CustomData{}.Unauthorized(w, req)
		return
	}

	id, err := tools.GetIdFromUrl(req)
	if err != nil {
		utility.SendError(err, w, req)
		return
	}

	var lang Language

	lang.ParentId = id

	// Get user from request
	err = tools.DecodeJson(&lang, req)
	if err != nil {
		utility.SendError(err, w, req)
		return
	}

	u, err := url.Parse(lang.Name)
	if err != nil {
		utility.SendError(err, w, req)
		return
	}

	lang.Name = u.String()

	result, err := database.DBCon.Exec(
		`
		INSERT INTO
			film_languages (film_id, language, name)
			VALUES(?, ?, ?)
		`,
		lang.ParentId,
		lang.Language,
		lang.Name,
	)
	if err != nil {
		utility.SendError(err, w, req)
		return
	}

	lang.Id, err = result.LastInsertId()
	if err != nil {
		utility.SendError(err, w, req)
		return
	}

	lang.Availability = NewAvailability()
	lang.Availability.CheckFilmStatus(lang.ParentId, lang.Language)

	utility.CustomData{
		Data: map[string]interface{}{
			"language": lang,
		},
	}.OK(w, req)
}

func RemoveFilmLanguage (w http.ResponseWriter, req *http.Request) {
	// Get permission of requesting user
	permission := authentication.GetPermissions(req, "libraries")
	if permission < 2 {
		utility.CustomData{}.Unauthorized(w, req)
		return
	}

	id, err := tools.GetIdFromUrl(req)
	if err != nil {
		utility.SendError(err, w, req)
		return
	}


	_, err = database.DBCon.Exec(
		`
		DELETE FROM
			film_languages
		WHERE
			id = ?
		`,
		id,
	)

	if err != nil {
		utility.SendError(err, w, req)
		return
	}

	utility.CustomData{}.OK(w, req)
}
