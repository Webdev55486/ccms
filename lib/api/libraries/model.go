package libraries

import (
	"database/sql"
	"github.com/MedFilm/ccms/lib/database"
	"net/url"
)

type filmWrap struct {
	Film Film `json:"film"`
}

type libraryWrap struct {
	Library Library `json:"library"`
}

type Film struct {
	Id         int64      `json:"id"`
	Name       string     `json:"name"`
	Hash       string     `json:"hash"`
	FileHash   string     `json:"fileHash"`
	SubjectId  int64      `json:"subjectId"`
	IsPublic   bool       `json:"isPublic"`
	FullPublic bool       `json:"fullPublic"`
	Views      int64      `json:"views"`
	Languages  []Language `json:"languages"`
	Created    string     `json:"created"`
	Updated    string     `json:"updated"`
}

type Library struct {
	Id        int64      `json:"id"`
	Name      string     `json:"name"`
	Hash      string     `json:"hash"`
	Type      string     `json:"type"`
	ParentId  *int64     `json:"parentId"`
	Languages []Language `json:"languages"`
	Image     string     `json:"image"`
	Created   string     `json:"created"`
	Updated   string     `json:"updated"`
}

type Language struct {
	Id           int64        `json:"id"`
	ParentId     int64        `json:"parentId"`
	Language     string       `json:"language"`
	Name         string       `json:"name"`
	Description  string       `json:"description"`
	Availability Availability `json:"availability"`
	Created      string       `json:"created"`
	Updated      string       `json:"updated"`
}

type Availability map[string]int64

func (l *Language) insertFilmLanguage(filmSubjectId int64) error {
	u, err := url.Parse(l.Name)
	if err != nil {
		return err
	}
	l.Name = u.String()
	_, err = database.DBCon.Exec(
		`
		INSERT INTO
			film_subject_languages
			(film_subject_id, language, name, description)
		VALUES(?, ?, ?)
		`,
		filmSubjectId,
		l.Language,
		l.Name,
		l.Description,
	)

	return err
}

func (l *Language) insertFilmSubjectLanguage(filmSubjectId int64) error {
	_, err := database.DBCon.Exec(
		`
		INSERT INTO
			film_subject_languages
			(film_subject_id, language, name)
		VALUES(?, ?, ?)
		`,
		filmSubjectId,
		l.Language,
		l.Name,
	)

	return err
}

func (l *Language) updateFilmSubjectLanguage() error {
	err := database.PerformQuery(
		`
		UPDATE film_subject_languages
		SET
			name = ?
		WHERE
			id = ?
		`,
		func(rows *sql.Rows) {},
		l.Name,
		l.Id,
	)

	return err
}

func (l *Language) updateFilmLanguage() error {
	u, err := url.Parse(l.Name)
	if err != nil {
		return err
	}
	l.Name = u.String()
	err = database.PerformQuery(
		`
		UPDATE film_languages
		SET
			name = ?,
			description = ?
		WHERE
			id = ?
		`,
		func(rows *sql.Rows) {},
		l.Name,
		l.Description,
		l.Id,
	)

	return err
}

func (f *Film) GetFilmById(id int64) error {
	var err error
	err = database.DBCon.QueryRow(
		`
		SELECT
		  f.id,
		  fl.name,
		  f.file_hash,
		  f.subject_id,
		  f.is_public,
		  f.views + (
			SELECT
			  COUNT(*)
			FROM
			  film_views
			WHERE
			  film_id = f.id
		  ),
		  f.created,
		  f.updated
		FROM
		  film f
		INNER JOIN
		  film_languages fl on f.id = fl.film_id AND fl.language = 'en'
		WHERE
		  f.id = ?
		`,
		id,
	).Scan(
		&f.Id,
		&f.Name,
		&f.FileHash,
		&f.SubjectId,
		&f.IsPublic,
		&f.Views,
		&f.Created,
		&f.Updated,
	)

	if err != nil {
		return err
	}

	f.Name, err = url.QueryUnescape(f.Name)

	return err
}

func GetFilmLanguagesForSub(id int64, subscriptionId int64) ([]Language, error) {
	var result []Language
	err := database.PerformQuery(
		`
		SELECT
		  fl.id,
		  fl.film_id,
		  fl.language,
		  fl.name,
		  fl.description,
		  fl.created,
		  fl.updated
		FROM
		  film_languages fl
		INNER JOIN
		  subscription_film_language_binding sflb ON fl.id = sflb.film_language_id
		INNER JOIN
		  subscriptions sub ON sflb.subscription_id = sub.id AND sub.id = ?
		WHERE
		  fl.film_id = ?
		`,
		func(rows *sql.Rows) {
			for rows.Next() {
				tmp := Language{}
				var err error
				rows.Scan(
					&tmp.Id,
					&tmp.ParentId,
					&tmp.Language,
					&tmp.Name,
					&tmp.Description,
					&tmp.Created,
					&tmp.Updated,
				)

				tmp.Name, err = url.QueryUnescape(tmp.Name)
				if err != nil {
					continue
				}

				tmp.Availability = NewAvailability()
				tmp.Availability.CheckSubscriptionStatus(id, subscriptionId, tmp.Language)
				result = append(result, tmp)
			}
		},
		subscriptionId,
		id,
	)

	return result, err
}
