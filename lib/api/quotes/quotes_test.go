package quotes

import "testing"

func TestCurrencyAbbreviation(t *testing.T) {
	type testcase struct {
		input  string
		output string
	}

	tt := []testcase{
		{
			input:  "1,55",
			output: "tkr",
		},
		{
			input:  "0,00",
			output: "kr",
		},
	}

	for _, tc := range tt {
		result := CurrencyAbbreviation(tc.input)
		if result != tc.output {
			t.Fatalf("Expected output '%s', got '%s'", tc.output, result)
		}
	}
}
