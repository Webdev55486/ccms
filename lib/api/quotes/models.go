package quotes

import (
	"fmt"
	"strings"
)

// A Request contains html
// data that is to be parsed
// into pdf
type Request struct {
	HTML string `json:"html"`
}

// Currency is just a float32 that can
// be formated from 1.50 to 1,50
type Currency float32

// Format to currency format
func (c Currency) Format() string {
	value := fmt.Sprintf("%.2f", float32(c))
	return strings.Replace(value, ".", ",", -1)
}

// CurrencyFormat string to currency string
func CurrencyFormat(str *string) {
	*str = strings.Replace(*str, ".", ",", -1)
}

// CurrencyFormatter is probably not needed as an interface
type CurrencyFormatter interface {
	FormatCurrencies()
}

// Quote data representation
type Quote struct {
	ContactPerson                  string  `json:"contactPerson"`
	Organisation                   string  `json:"organisation"`
	TotalOptionals                 float32 `json:"totalOptionals"`
	TotalSubscriptions             float32 `json:"totalSubscriptions"`
	TotalOptionalsPreDiscounts     float32 `json:"totalOptionalsPreDiscounts"`
	TotalSubscriptionsPreDiscounts float32 `json:"totalSubscriptionsPreDiscounts"`

	StrTotalOptionals                 string
	StrTotalSubscriptions             string
	StrTotalOptionalsPreDiscounts     string
	StrTotalSubscriptionsPreDiscounts string

	KAM               KAM               `json:"kam"`
	Totals            totals            `json:"totals"`
	Subscriptions     []subscription    `json:"subscriptions"`
	OptionalProducts  []optionalProduct `json:"optionalProducts"`
	Discounts         []discount        `json:"discounts"`
	DiscountsOptional []discount        `json:"discountsOptional"`
}

// FormatCurrencies deep formats all currency values
func (q *Quote) FormatCurrencies() {
	q.StrTotalOptionals = Currency(q.TotalOptionals).Format()
	q.StrTotalSubscriptions = Currency(q.TotalSubscriptions).Format()
	q.StrTotalOptionalsPreDiscounts = Currency(q.TotalOptionalsPreDiscounts).Format()
	q.StrTotalSubscriptionsPreDiscounts = Currency(q.TotalSubscriptionsPreDiscounts).Format()

	q.Totals.FormatCurrencies()
	for i := 0; i < len(q.Subscriptions); i++ {
		q.Subscriptions[i].FormatCurrencies()
	}
	for i := 0; i < len(q.OptionalProducts); i++ {
		q.OptionalProducts[i].FormatCurrencies()
	}

	for i := 0; i < len(q.Discounts); i++ {
		CurrencyFormat(&q.Discounts[i].Value)
	}

	for i := 0; i < len(q.DiscountsOptional); i++ {
		CurrencyFormat(&q.DiscountsOptional[i].Value)
	}
}

type totals struct {
	PricePerFilm     float32 `json:"pricePerFilm"`
	PricePerLanguage float32 `json:"pricePerLanguage"`
	Licenses         int32   `json:"licenses"`
	Languages        int32   `json:"languages"`

	StrPricePerFilm     string
	StrPricePerLanguage string
}

func (t *totals) FormatCurrencies() {
	t.StrPricePerFilm = Currency(t.PricePerFilm).Format()
	t.StrPricePerLanguage = Currency(t.PricePerLanguage).Format()
}

// KAM data
type KAM struct {
	Name        string `json:"name"`
	Role        string `json:"role"`
	Phonenumber string `json:"phonenumber"`
	Email       string `json:"email"`
	PhotoURL    string `json:"photoUrl"`
}

// optionalProduct data
type optionalProduct struct {
	Name       string  `json:"name"`
	Amount     int32   `json:"amount"`
	Price      float32 `json:"price"`
	PriceTotal float32 `json:"priceTotal"`

	StrPrice      string
	StrPriceTotal string
}

func (o *optionalProduct) FormatCurrencies() {
	o.StrPrice = Currency(o.Price).Format()
	o.StrPriceTotal = Currency(o.PriceTotal).Format()
}

// subscription data
type subscription struct {
	Name           string  `json:"name"`
	PriceFilms     float32 `json:"priceFilms"`
	Licenses       int8    `json:"licenses"`
	Languages      string  `json:"languages"`
	PriceLanguages int32   `json:"priceLanguages"`

	StrPriceFilms string
}

func (s *subscription) FormatCurrencies() {
	s.StrPriceFilms = Currency(s.PriceFilms).Format()
}

// discount data
type discount struct {
	Reason string `json:"key"`
	Value  string `json:"value"`
}
