package quotes

import (
	"bytes"
	"html/template"
	"net/http"
	"strconv"
	"strings"

	"github.com/MedFilm/ccms/lib/api/utility"
	"github.com/MedFilm/ccms/lib/tools"
	wkhtmltopdf "github.com/SebastiaanKlippert/go-wkhtmltopdf"
)

// FormatCurrency if zero and with correct
// decimal places if contains zero
// TemplateFunction
func FormatCurrency(str string) string {
	if str[len(str)-1] == '0' {
		str = str[:len(str)-1]
		return FormatCurrency(str)
	}
	if str[len(str)-1] == ',' {
		return str[:len(str)-1]
	}
	val, err := strconv.ParseFloat(strings.Replace(str, ",", ".", -1), 32)
	if err != nil {
		return str
	}
	if val == 0 {
		return "0"
	}
	return str
}

// CurrencyAbbreviation returns either tkr or
// kr depending if passed currency string has a zero value
func CurrencyAbbreviation(str string) string {
	value, err := strconv.ParseFloat(strings.Replace(str, ",", ".", -1), 32)
	if err != nil || value != 0 {
		return "tkr"
	}
	return "kr"
}

// MakePDF ... Hey, what did you expect
func MakePDF(w http.ResponseWriter, r *http.Request) {

	quote := Quote{}
	if err := tools.DecodeJson(&quote, r); err != nil {
		utility.SendError(err, w, r)
		return
	}

	quote.FormatCurrencies()

	buffer := bytes.NewBufferString("")
	// tmpl, err := template.ParseFiles("./templates/quote.html")
	tmpl, err := template.New("quote.gohtml").Funcs(template.FuncMap{
		"format": FormatCurrency,
		"abbrev": CurrencyAbbreviation,
	}).ParseFiles("./templates/quote.gohtml")
	if err != nil {
		utility.SendError(err, w, r)
		return
	}

	if err := tmpl.Execute(buffer, quote); err != nil {
		utility.SendError(err, w, r)
		return
	}

	pdfg, err := wkhtmltopdf.NewPDFGenerator()
	if err != nil {
		utility.SendError(err, w, r)
		return
	}

	pdfg.AddPage(wkhtmltopdf.NewPageReader(buffer))

	if err := pdfg.Create(); err != nil {
		utility.SendError(err, w, r)
		return
	}

	// w.Header().Set("Content-Type", "application/pdf")

	// if _, err := io.Copy(w, pdfg.Buffer()); err != nil {
	// 	utility.SendError(err, w, r)
	// 	return
	// }

	pdfbuffer := pdfg.Buffer()
	utility.CustomData{Data: map[string]interface{}{
		"buffer": pdfbuffer.Bytes(),
	}}.OK(w, r)
}
