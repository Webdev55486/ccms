package quotes

import (
	"bytes"
	"net/http"
	"strings"

	"github.com/MedFilm/ccms/lib/api/utility"
	"github.com/MedFilm/ccms/lib/tools"
	topdf "github.com/SebastiaanKlippert/go-wkhtmltopdf"
)

// GeneratePDF takes html data as a string
// and generates a pdf using wkhtmltopdf
func GeneratePDF(data string) (*bytes.Buffer, error) {
	// Create a generator
	pdfg, err := topdf.NewPDFGenerator()
	if err != nil {
		return nil, err
	}

	// Remove all margin
	pdfg.MarginBottom.Set(0)
	pdfg.MarginTop.Set(0)
	pdfg.MarginRight.Set(0)
	pdfg.MarginLeft.Set(0)
	pdfg.PageSize.Set(topdf.PageSizeA4)
	pdfg.Dpi.Set(600)

	// Add a html content to pdf
	html := strings.NewReader(data)
	pdfg.AddPage(topdf.NewPageReader(html))

	// Create the file
	err = pdfg.Create()
	if err != nil {
		return nil, err
	}

	// Return the bytes of the pdf
	bytes := pdfg.Buffer()
	return bytes, nil
}

// PDFHandler takes a POST request with html data
// and returns a pdf file
func PDFHandler(w http.ResponseWriter, r *http.Request) {
	// Decode request data into request struct
	request := Request{}
	if err := tools.DecodeJson(&request, r); err != nil {
		utility.SendError(err, w, r)
		return
	}
	// Generate PDF
	buffer, err := GeneratePDF(request.HTML)
	if err != nil {
		utility.SendError(err, w, r)
		return
	}
	// Send pdf data
	utility.CustomData{Data: map[string]interface{}{
		"buffer": buffer.Bytes(),
	}}.OK(w, r)
}
