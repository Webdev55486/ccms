package devices

import (
	"database/sql"
	"io/ioutil"
	"net/http"

	"fmt"

	"github.com/MedFilm/ccms/lib/api/utility"
	"github.com/MedFilm/ccms/lib/authentication"
	"github.com/MedFilm/ccms/lib/database"
	"github.com/MedFilm/ccms/lib/tools"
)

type TabletPacket struct {
	Tablet        Tablet                 `json:"tablet"`
	Manifest      Manifest               `json:"manifest"`
	Subscriptions []ManifestSubscription `json:"subscriptions"`
}

type Tablet struct {
	Id         int64  `json:"id"`
	UserId     int64  `json:"userId"`
	TabletName string `json:"tabletName"`
	Token      string `json:"token"`
}

type Manifest struct {
	Id       int64 `json:"id"`
	TabletId int64 `json:"tabletId"`
	Version  int64 `json:"version"`
}

type ManifestSubscription struct {
	Id             int64 `json:"id"`
	ManifestId     int64 `json:"manifestId"`
	SubscriptionId int64 `json:"subscriptionId"`
}

func GetTablets(w http.ResponseWriter, req *http.Request) {
	// Get permission of requesting user
	permission := authentication.GetPermissions(req, "accounts")

	if permission == 0 {
		utility.CustomData{}.Unauthorized(w, req)
		return
	}

	// Get id from request
	id, err := tools.GetIdFromUrl(req)
	if err != nil {
		utility.SendError(err, w, req)
		return
	}

	var result []TabletPacket
	var tablets []Tablet

	err = database.PerformQuery(
		`
		SELECT
			t.id,
			t.user_id,
			t.tablet_name,
			t.token
		FROM
			tablet as t
		WHERE
			t.user_id=?
		ORDER BY
			createdAt
		`,
		func(rows *sql.Rows) {
			for rows.Next() {
				tablet := Tablet{}

				rows.Scan(
					&tablet.Id,
					&tablet.UserId,
					&tablet.TabletName,
					&tablet.Token,
				)

				tablets = append(tablets, tablet)
			}
		}, id,
	)
	if err != nil {
		utility.SendError(err, w, req)
		return
	}

	for _, tablet := range tablets {

		subs := []ManifestSubscription{}
		packet := TabletPacket{Tablet: tablet, Subscriptions: subs}
		manifest := Manifest{}

		err := database.PerformQuery(`
			SELECT
				m.id,
				m.tablet_id,
				m.version
			FROM
				manifest as m
			WHERE
				m.tablet_id=?
		`,
			func(rows *sql.Rows) {
				for rows.Next() {
					rows.Scan(
						&manifest.Id,
						&manifest.TabletId,
						&manifest.Version,
					)
				}
			}, tablet.Id,
		)

		if err != nil {
			utility.SendError(err, w, req)
			return
		}

		err = database.PerformQuery(
			`
				SELECT
					ms.id,
					ms.manifest_id,
					ms.subscription_id
				FROM
					manifest_subscription as ms
				WHERE
					ms.manifest_id=?
			`,
			func(rows *sql.Rows) {

				for rows.Next() {
					manifestSub := ManifestSubscription{}

					rows.Scan(
						&manifestSub.Id,
						&manifestSub.ManifestId,
						&manifestSub.SubscriptionId,
					)

					packet.Subscriptions = append(packet.Subscriptions, manifestSub)
				}

			}, manifest.Id,
		)

		if err != nil {
			utility.SendError(err, w, req)
			return
		}
		packet.Manifest = manifest
		result = append(result, packet)
	}

	utility.CustomData{Data: map[string]interface{}{
		"result": result,
	}}.OK(w, req)
}
func GetManifest(w http.ResponseWriter, req *http.Request) {
	permission := authentication.GetPermissions(req, "accounts")
	if permission == 0 {
		utility.CustomData{}.Unauthorized(w, req)
		return
	}

	id, err := tools.GetIdFromUrl(req)
	if err != nil {
		utility.SendError(err, w, req)
		return
	}

	manifest := Manifest{}

	err = database.PerformQuery(`
		SELECT
			id,
			tablet_id,
			version
		FROM
			manifest
		WHERE
			tablet_id=?
	`, func(rows *sql.Rows) {
		for rows.Next() {
			rows.Scan(
				&manifest.Id,
				&manifest.TabletId,
				&manifest.Version,
			)
		}
	}, id)
	if err != nil {
		utility.SendError(err, w, req)
		return
	}

	utility.CustomData{Data: map[string]interface{}{
		"result": manifest,
	}}.OK(w, req)
}
func GetManifestSubscriptions(w http.ResponseWriter, req *http.Request) {
	permission := authentication.GetPermissions(req, "accounts")
	if permission == 0 {
		utility.CustomData{}.Unauthorized(w, req)
		return
	}

	id, err := tools.GetIdFromUrl(req)
	if err != nil {
		utility.SendError(err, w, req)
		return
	}

	var resultSubscriptions []ManifestSubscription

	err = database.PerformQuery(`
		SELECT
			id,
			manifest_id,
			subscription_id
		FROM
			manifest_subscription
		WHERE
			manifest_id=?
	`, func(rows *sql.Rows) {
		for rows.Next() {
			sub := ManifestSubscription{}

			rows.Scan(
				&sub.ManifestId,
				&sub.SubscriptionId,
			)

			resultSubscriptions = append(resultSubscriptions, sub)
		}
	}, id)

	if err != nil {
		utility.SendError(err, w, req)
		return
	}

	utility.CustomData{Data: map[string]interface{}{
		"result": resultSubscriptions,
	}}.OK(w, req)
}

func AddTablet(w http.ResponseWriter, req *http.Request) {
	permission := authentication.GetPermissions(req, "accounts")
	if permission < 2 {
		utility.CustomData{}.Unauthorized(w, req)
		return
	}

	tablet := Tablet{}

	err := tools.DecodeJson(&tablet, req)
	if err != nil {
		utility.SendError(err, w, req)
		return
	}

	// CREATE TABLET
	err = database.PerformQuery(`
		INSERT INTO tablet
		(
			tablet_name,
			user_id,
			token
		) VALUES (?, ?, ?)
	`, func(rows *sql.Rows) {}, tablet.TabletName, tablet.UserId, tablet.Token)

	if err != nil {
		utility.SendError(err, w, req)
		return
	}

	// CREATE MANIFEST
	err = database.PerformQuery(`
		SELECT
			id
		FROM
			tablet
		WHERE
			tablet_name=?
			AND
			user_id=?
	`, func(rows *sql.Rows) {
		for rows.Next() {
			rows.Scan(
				&tablet.Id,
			)
		}
	}, tablet.TabletName, tablet.UserId)

	if err != nil {
		utility.SendError(err, w, req)
		return
	}

	err = database.PerformQuery(`
		INSERT INTO manifest(tablet_id)VALUES(?)
	`, func(rows *sql.Rows) {}, tablet.Id)

	if err != nil {
		utility.SendError(err, w, req)
		return
	}
	utility.CustomData{}.OK(w, req)

}
func AddManifest(w http.ResponseWriter, req *http.Request) {}
func AddManifestSubscription(w http.ResponseWriter, req *http.Request) {
	permission := authentication.GetPermissions(req, "accounts")

	if permission < 2 {
		utility.CustomData{}.Unauthorized(w, req)
		return
	}

	manifestSub := ManifestSubscription{}

	err := tools.DecodeJson(&manifestSub, req)

	if err != nil {
		utility.SendError(err, w, req)
		return
	}
	fmt.Println(manifestSub)
	err = database.PerformQuery(`
		INSERT INTO manifest_subscription (manifest_id, subscription_id) VALUES (?, ?)
	`, func(rows *sql.Rows) {}, manifestSub.ManifestId, manifestSub.SubscriptionId)

	if err != nil {
		utility.SendError(err, w, req)
		return
	}

	utility.CustomData{}.OK(w, req)
}

func DeleteTablet(w http.ResponseWriter, req *http.Request)   {}
func DeleteManifest(w http.ResponseWriter, req *http.Request) {}
func DeleteManifestSubscription(w http.ResponseWriter, req *http.Request) {
	permission := authentication.GetPermissions(req, "accounts")
	if permission < 2 {
		utility.CustomData{}.Unauthorized(w, req)
		return
	}

	id, err := tools.GetIdFromUrl(req)
	if err != nil {
		utility.SendError(err, w, req)
		return
	}

	database.PerformQuery(`
		DELETE FROM
			manifest_subscription
		WHERE
			id=?
	`, func(rows *sql.Rows) {}, id)

	if err != nil {
		utility.SendError(err, w, req)
		return
	}

	utility.CustomData{}.OK(w, req)
}

func UpdateTablet(w http.ResponseWriter, req *http.Request) {}
func UpdateManifest(w http.ResponseWriter, req *http.Request) {
	permission := authentication.GetPermissions(req, "accounts")
	if permission < 2 {
		utility.CustomData{}.Unauthorized(w, req)
		return
	}

	id, err := tools.GetIdFromUrl(req)
	if err != nil {
		utility.SendError(err, w, req)
		return
	}

	err = database.PerformQuery(`
		UPDATE manifest
			SET version=version+1
		WHERE
			id=?
	`, func(rows *sql.Rows) {}, id)

	if err != nil {
		utility.SendError(err, w, req)
		return
	}

	utility.CustomData{}.OK(w, req)
}
func UpdateManifestSubscription(w http.ResponseWriter, req *http.Request) {}
func DownloadAPK(w http.ResponseWriter, r *http.Request) {
	data, err := ioutil.ReadFile("/files/app-release.apk")

	if err != nil {
		utility.SendError(err, w, r)
		return
	}

	w.Header().Set("Content-Disposition", "attachment; filename=app-release.apk")
	w.Header().Set("Content-Type", "application/vnd.android.package-archive")
	w.Write(data)
}
