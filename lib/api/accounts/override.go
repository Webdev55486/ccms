/**
 * ccms
 * Copyright (C) Sunstate AB - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Simon Wikstrand <simon@sunstate.se>, 2017-01-13
 */

package accounts

import (
	"errors"
	"github.com/MedFilm/ccms/lib/api/utility"
	"github.com/MedFilm/ccms/lib/authentication"
	"github.com/gorilla/mux"
	"net/http"
	"os"
	"reflect"
	"strings"
)

type permit struct {
	Film      subCategory `json:"film"`
	Thumbnail subCategory `json:"thumbnail"`
	Chapter   languages   `json:"chapter"`
	Subtitle  languages   `json:"subtitle"`
}

type subCategory map[string]*languages

type languages struct {
	En bool `json:"en"`
	Se bool `json:"se"`
	No bool `json:"no"`
	Fi bool `json:"fi"`
	Fa bool `json:"fa"`
	Ar bool `json:"ar"`
	Hr bool `json:"hr"`
}

func (p *permit) initPermit(name string) error {

	// Initiate film
	p.Film = subCategory{
		"720p": &languages{},
		"480p": &languages{},
		"360p": &languages{},
	}

	// Initiate thumbnail
	p.Thumbnail = subCategory{
		"small":  &languages{},
		"normal": &languages{},
	}

	p.Film["720p"].senseFilm(name, "720p")
	p.Film["480p"].senseFilm(name, "480p")
	p.Film["360p"].senseFilm(name, "360p")

	p.Thumbnail["small"].senseThumbnail(name, "small")
	p.Thumbnail["normal"].senseThumbnail(name, "normal")

	p.Chapter.senseChapter(name)
	p.Subtitle.senseSubtitle(name)

	return nil
}

func (l *languages) senseFilm(name string, resolution string) {

	// Get reflect value of struct
	v := reflect.Indirect(reflect.ValueOf(l))

	// Loop through all structs
	for i := 0; i < v.NumField(); i++ {

		// Get name of struct property
		fn := strings.ToLower(v.Type().Field(i).Name)
		if _, err := os.Stat("./uploads/film/" + name + "_" + resolution + "_" + fn + ".mp4"); err == nil {
			v.Field(i).SetBool(true)
		}
	}
}

func (l *languages) senseThumbnail(name string, size string) {

	// Get reflect value of struct
	v := reflect.Indirect(reflect.ValueOf(l))

	// Loop through all structs
	for i := 0; i < v.NumField(); i++ {

		// Get name of struct property
		fn := strings.ToLower(v.Type().Field(i).Name)
		extension := ".png"

		// If size is small, add additional extension
		if size == "small" {
			extension = extension + ".small.png"
		}

		if _, err := os.Stat("./uploads/thumbnails/" + name + "_thumb_" + fn + extension); err == nil {
			v.Field(i).SetBool(true)
		}
	}
}

func (l *languages) senseChapter(name string) {
	// Get reflect value of struct
	v := reflect.Indirect(reflect.ValueOf(l))

	// Loop through all structs
	for i := 0; i < v.NumField(); i++ {

		// Get name of struct property
		fn := strings.ToLower(v.Type().Field(i).Name)
		if _, err := os.Stat("./uploads/vtt/" + name + "_chapters_" + fn + ".vtt"); err == nil {
			v.Field(i).SetBool(true)
		}
	}
}

func (l *languages) senseSubtitle(name string) {
	// Get reflect value of struct
	v := reflect.Indirect(reflect.ValueOf(l))

	// Loop through all structs
	for i := 0; i < v.NumField(); i++ {

		// Get name of struct property
		fn := strings.ToLower(v.Type().Field(i).Name)
		if _, err := os.Stat("./uploads/vtt/" + name + "_captions_" + fn + ".vtt"); err == nil {
			v.Field(i).SetBool(true)
		}
	}
}

func GetPermits(w http.ResponseWriter, req *http.Request) {
	var permit permit
	// Get permission of requesting user
	permission := authentication.GetPermissions(req, "accounts")
	if permission == 0 {
		utility.CustomData{}.Unauthorized(w, req)
		return
	}

	// Check for id in params
	val, ok := mux.Vars(req)["override"]
	if !ok {
		utility.SendError(errors.New("Could not get override string"), w, req)
		return
	}

	permit.initPermit(val)

	utility.CustomData{Data: map[string]interface{}{
		"result": permit,
	}}.OK(w, req)
}
