/**
 * ccms
 * Copyright (C) Sunstate AB - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Simon Wikstrand <simon@sunstate.se>, 2017-01-13
 */

package subscriptions

import (
	"database/sql"
	"errors"
	"fmt"
	"github.com/MedFilm/ccms/lib/api/libraries"
	"github.com/MedFilm/ccms/lib/api/utility"
	"github.com/MedFilm/ccms/lib/authentication"
	"github.com/MedFilm/ccms/lib/database"
	"github.com/MedFilm/ccms/lib/tools"
	"github.com/gorilla/mux"
	"net/http"
	"os"
	"net/url"
)

type Subscriptions struct {
	Id               int64               `json:"id"`
	UserId           int64               `json:"userId"`
	FilmId           int64               `json:"filmId"`
	Months           database.NullInt    `json:"months"`
	StartDate        database.NullInt    `json:"startDate"`
	EndDate          database.NullInt    `json:"endDate"`
	AccessCode       database.NullString `json:"accessCode"`
	FilmFileOverride database.NullString `json:"filmFileOverride"`
	Views            int64               `json:"views"`
	CategoryName	 string `json:"categoryName"`
	Type             string              `json:"type"`
	Prices           Prices              `json:"prices"`
	Film             libraries.Film      `json:"film"`
}

type Prices struct {
	Cur  database.NullString `json:"currency"`
	Base int64               `json:"base"`
	En   int64               `json:"en"`
	Se   int64               `json:"se"`
	No   int64               `json:"no"`
	Fi   int64               `json:"fi"`
	Fa   int64               `json:"fa"`
	Ar   int64               `json:"ar"`
	Hr   int64               `json:"hr"`
}

func GetSubscriptions(w http.ResponseWriter, req *http.Request) {
	// Get permission of requesting user
	permission := authentication.GetPermissions(req, "libraries")
	if permission == 0 {
		utility.CustomData{}.Unauthorized(w, req)
		return
	}

	id, err := tools.GetIdFromUrl(req)
	if err != nil {
		utility.SendError(err, w, req)
		return
	}

	var result []Subscriptions

	err = database.PerformQuery(
		`
		SELECT
			sub.id,
		 	sub.user_id,
		  	sub.film_id,
		  	sub.months,
		  	sub.end_date,
		  	sub.start_date,
		  	sub.accesscode,
		  	sub.film_file_override,
		  	sub.type,
		  	fs.name,
		  	COALESCE(subo.price, 0),
		  	COALESCE(subo.price_en, 0),
		  	COALESCE(subo.price_se, 0),
		  	COALESCE(subo.price_no, 0),
		  	COALESCE(subo.price_fi, 0),
		  	COALESCE(subo.price_fa, 0),
		  	COALESCE(subo.price_ar, 0),
		  	COALESCE(subo.price_hr, 0),
		  	subo.currency,
		  	(
				SELECT
			  		COUNT(*)
				FROM
			  		subscription_views
				WHERE
			  		subscription_id = sub.id
		  	) + sub.views AS view_count
		FROM
		  	subscriptions sub
		LEFT OUTER JOIN
		  	subscriptions_options subo ON sub.id = subo.subscription_id
		LEFT OUTER JOIN
			film f ON sub.film_id = f.id
		LEFT OUTER JOIN
			film_subject fs ON f.subject_id = fs.id
		WHERE
			sub.user_id = ?
		`,
		func(rows *sql.Rows) {
			for rows.Next() {
				sub := Subscriptions{
					Prices: Prices{},
				}

				rows.Scan(
					&sub.Id,
					&sub.UserId,
					&sub.FilmId,
					&sub.Months,
					&sub.EndDate,
					&sub.StartDate,
					&sub.AccessCode,
					&sub.FilmFileOverride,
					&sub.Type,
					&sub.CategoryName,
					&sub.Prices.Base,
					&sub.Prices.En,
					&sub.Prices.Se,
					&sub.Prices.No,
					&sub.Prices.Fi,
					&sub.Prices.Fa,
					&sub.Prices.Ar,
					&sub.Prices.Hr,
					&sub.Prices.Cur,
					&sub.Views,
				)

				sub.Film.GetFilmById(sub.FilmId)

				result = append(result, sub)
			}
		},
		id,
	)

	if err != nil {
		utility.SendError(err, w, req)
		return
	}

	utility.CustomData{
		Data: map[string]interface{}{
			"subscriptions": result,
		},
	}.OK(w, req)
}

func GetSubscriptionsModel(id int64) ([]Subscriptions, error) {
	var (
		result []Subscriptions
		err    error
	)

	err = database.PerformQuery(
		`
		SELECT
			sub.id,
		 	sub.user_id,
		  	sub.film_id,
		  	sub.months,
		  	sub.end_date,
		  	sub.start_date,
		  	sub.accesscode,
		  	sub.film_file_override,
		  	fs.name,
		  	sub.type,
		  	COALESCE(subo.price, 0),
		  	COALESCE(subo.price_en, 0),
		  	COALESCE(subo.price_se, 0),
		  	COALESCE(subo.price_no, 0),
		  	COALESCE(subo.price_fi, 0),
		  	COALESCE(subo.price_fa, 0),
		  	COALESCE(subo.price_ar, 0),
		  	COALESCE(subo.price_hr, 0),
		  	subo.currency,
		  	(
				SELECT
			  		COUNT(*)
				FROM
			  		subscription_views
				WHERE
			  		subscription_id = sub.id
		  	) + sub.views AS view_count
		FROM
		  	subscriptions sub
		LEFT OUTER JOIN
		  	subscriptions_options subo ON sub.id = subo.subscription_id
		LEFT OUTER JOIN
			film f ON sub.film_id = f.id
		LEFT OUTER JOIN
			film_subject fs ON f.subject_id = fs.id
		WHERE
			sub.user_id = ?
		`,
		func(rows *sql.Rows) {
			for rows.Next() {
				sub := Subscriptions{
					Prices: Prices{},
				}

				rows.Scan(
					&sub.Id,
					&sub.UserId,
					&sub.FilmId,
					&sub.Months,
					&sub.EndDate,
					&sub.StartDate,
					&sub.AccessCode,
					&sub.FilmFileOverride,
					&sub.CategoryName,
					&sub.Type,
					&sub.Prices.Base,
					&sub.Prices.En,
					&sub.Prices.Se,
					&sub.Prices.No,
					&sub.Prices.Fi,
					&sub.Prices.Fa,
					&sub.Prices.Ar,
					&sub.Prices.Hr,
					&sub.Prices.Cur,
					&sub.Views,
				)

				sub.Film.GetFilmById(sub.FilmId)
				sub.Film.Languages, err = libraries.GetFilmLanguagesForSub(sub.FilmId, sub.Id)
				if err != nil {
					fmt.Println(err.Error())
					continue
				}
				result = append(result, sub)
			}
		},
		id,
	)

	return result, err
}

func CreateNewLanguageBinding(w http.ResponseWriter, req *http.Request) {
	// Get permission of requesting user
	permission := authentication.GetPermissions(req, "accounts")

	if permission < 2 {
		utility.CustomData{}.Unauthorized(w, req)
	}

	id, err := tools.GetIdFromUrl(req)
	if err != nil {
		utility.SendError(err, w, req)
		return
	}

	payload := struct {
		FilmId       int64  `json:"filmId"`
		FilmLanguage string `json:"filmLanguage"`
	}{}

	err = tools.DecodeJson(&payload, req)
	if err != nil {
		utility.SendError(err, w, req)
		return
	}

	var filmLanguageId int64

	err = database.DBCon.QueryRow(
		`
		SELECT
			id
		FROM
			film_languages
		WHERE
			film_id = ? AND language = ? AND original = 1
		`,
		payload.FilmId,
		payload.FilmLanguage,
	).Scan(
		&filmLanguageId,
	)

	if err != nil && err != sql.ErrNoRows {
		utility.SendError(err, w, req)
		return
	}

	if filmLanguageId == 0 {
		query, err := database.DBCon.Exec(
			`
			INSERT INTO
				film_languages (film_id, original, language)
			VALUES
				(?, 0, ?)
			`,
			payload.FilmId,
			payload.FilmLanguage,
		)

		if err != nil {
			utility.SendError(err, w, req)
			return
		}

		filmLanguageId, err = query.LastInsertId()

		if err != nil {
			utility.SendError(err, w, req)
			return
		}
	}

	var count int64
	err = database.DBCon.QueryRow(
		`
		SELECT
			COUNT(*)
		FROM
			subscription_film_language_binding
		WHERE
			subscription_id = ? AND film_language_id = ?
		`,
		id,
		filmLanguageId,
	).Scan(
		&count,
	)

	if err != nil {
		utility.SendError(err, w, req)
		return
	}

	if count > 0 {
		utility.SendError(errors.New("Binding already exists"), w, req)
		return
	}

	_, err = database.DBCon.Exec(
		`
		INSERT INTO
			subscription_film_language_binding (subscription_id, film_language_id)
		VALUES
			(?, ?)
		`,
		id,
		filmLanguageId,
	)

	if err != nil {
		utility.SendError(err, w, req)
		return
	}

	utility.CustomData{}.OK(w, req)
}

func DeleteLanguageBinding(w http.ResponseWriter, req *http.Request) {
	// Get permission of requesting user
	permission := authentication.GetPermissions(req, "accounts")

	if permission < 2 {
		utility.CustomData{}.Unauthorized(w, req)
	}

	vars := mux.Vars(req)
	subId, ok := vars["subscriptionId"]
	if !ok {
		utility.SendError(errors.New("Could not find subscriptionId"), w, req)
		return
	}

	LanguageId, ok := vars["languageId"]
	if !ok {
		utility.SendError(errors.New("Could not find LanguageId"), w, req)
		return
	}

	var language string
	err := database.DBCon.QueryRow(
		`
		SELECT
			language
		FROM
			film_languages
		WHERE
			id = ?
		`,
		LanguageId,
	).Scan(
		&language,
	)

	if err != nil {
		utility.SendError(err, w, req)
		return
	}

	_, err = database.DBCon.Exec(
		`
		DELETE FROM
			subscription_film_language_binding
		WHERE
			subscription_id = ? AND film_language_id = ?
		`,
		subId,
		LanguageId,
	)

	if err != nil {
		utility.SendError(err, w, req)
		return
	}

	err = database.PerformQuery(
		`
		SELECT
		  ff.id,
		  ff.filename
		FROM
		  film_files ff
		INNER JOIN
		  film f ON ff.film_id = f.id
		INNER JOIN
		  subscriptions sub ON f.id = sub.film_id AND sub.id = ?
		WHERE
		  ff.subscription_id = ? AND language = ?
		`,
		func(rows *sql.Rows) {
			for rows.Next() {
				var (
					id       int64
					filename string
				)
				rows.Scan(
					&id,
					&filename,
				)

				if _, err = os.Stat("./files/" + filename); !os.IsNotExist(err) {
					err = os.Remove("./files/" + filename)
					if err != nil {
						continue
					}

					_, err = database.DBCon.Exec(
						`
						DELETE FROM
							film_files
						WHERE
							id = ?
						`,
						id,
					)
				}
			}
		},
		subId,
		subId,
		language,
	)

	_, err = database.DBCon.Exec(
		`
		DELETE FROM
			film_languages
		WHERE
			id = ? AND original = 0
		`,
		LanguageId,
	)

	if err != nil {
		utility.SendError(err, w, req)
		return
	}

	utility.CustomData{}.OK(w, req)
}

func UpdateSubscription(w http.ResponseWriter, req *http.Request) {
	// Get permission of requesting user
	permission := authentication.GetPermissions(req, "accounts")

	if permission < 2 {
		utility.CustomData{}.Unauthorized(w, req)
	}

	var err error
	var id int64
	sub := Subscriptions{
		Prices: Prices{},
		Film: libraries.Film{
			Languages: []libraries.Language{},
		},
	}

	id, err = tools.GetIdFromUrl(req)
	if err != nil {
		utility.SendError(err, w, req)
		return
	}

	err = tools.DecodeJson(&sub, req)
	if err != nil {
		utility.SendError(err, w, req)
		return
	}

	err = database.PerformQuery(
		`
		UPDATE
			subscriptions
		SET
			months=?,
			start_date=?,
			end_date=?,
			film_file_override=?,
			type=?
		WHERE
			id=?
		`,
		func(rows *sql.Rows) {},
		sub.Months,
		sub.StartDate,
		sub.EndDate,
		sub.FilmFileOverride,
		sub.Type,
		id,
	)
	if err != nil {
		utility.SendError(err, w, req)
		return
	}

	err = database.PerformQuery(
		`
		REPLACE INTO
			subscriptions_options
			(
				subscription_id,
				price,
				price_en,
				price_se,
				price_no,
				price_fi,
				price_fa,
				price_ar,
				price_hr,
				currency
			)
			VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?, ?)
		`,
		func(rows *sql.Rows) {},
		sub.Id,
		sub.Prices.Base,
		sub.Prices.En,
		sub.Prices.Se,
		sub.Prices.No,
		sub.Prices.Fi,
		sub.Prices.Fa,
		sub.Prices.Ar,
		sub.Prices.Hr,
		sub.Prices.Cur,
	)
	if err != nil {
		utility.SendError(err, w, req)
		return
	}

	err = database.PerformQuery(
		`
		SELECT
		  fl.id,
		  fl.original,
		  fl.name
		FROM
		  film_languages fl
		INNER JOIN
		  subscription_film_language_binding sflb ON fl.id = sflb.film_language_id
		INNER JOIN
		  subscriptions sub ON sflb.subscription_id = sub.id AND sub.id = ?
		WHERE
		  fl.film_id = ?
		`,
		func(rows *sql.Rows) {
			for rows.Next() {
				var (
					lang     libraries.Language
					original int64
				)

				rows.Scan(
					&lang.Id,
					&original,
					&lang.Name,
				)

				for _, v := range sub.Film.Languages {
					if v.Id == lang.Id && (v.Name != lang.Name || v.Description != lang.Description) {
						u, err := url.Parse(v.Name)
						if err != nil {
							continue
						}
						v.Name = u.String()

						if original == 0 {
							_, err = database.DBCon.Exec(
								`
								UPDATE
									film_languages
								SET
									name = ?,
									description = ?
								WHERE
									id = ?
								`,
								v.Name,
								v.Description,
								v.Id,
							)
						} else {
							query, err := database.DBCon.Exec(
								`
								INSERT INTO
									film_languages (film_id, original, language, name, description)
								VALUES
									(?, 0, ?, ?, ?)
								`,
								sub.Film.Id,
								v.Language,
								v.Name,
								v.Description,
							)

							if err != nil {
								continue
							}

							id, err := query.LastInsertId()
							if err != nil {
								continue
							}

							_, err = database.DBCon.Exec(
								`
								UPDATE
									subscription_film_language_binding
								SET
									film_language_id = ?
								WHERE
									subscription_id = ? AND film_language_id = ?
								`,
								id,
								sub.Id,
								lang.Id,
							)
						}
					}
				}
			}
		},
		sub.Id,
		sub.Film.Id,
	)

	utility.CustomData{}.OK(w, req)
}

func DeleteSubscription(w http.ResponseWriter, req *http.Request) {
	// Get permission of requesting user
	permission := authentication.GetPermissions(req, "accounts")

	if permission < 2 {
		utility.CustomData{}.Unauthorized(w, req)
	}

	id, err := tools.GetIdFromUrl(req)
	if err != nil {
		utility.SendError(err, w, req)
		return
	}

	err = database.PerformQuery(
		`
		DELETE
		  sub, sflb, fl
		FROM
		  subscriptions sub
		LEFT OUTER JOIN
		  subscription_film_language_binding sflb ON sub.id = sflb.subscription_id
		LEFT OUTER JOIN
		  film_languages fl ON sflb.film_language_id = fl.id AND fl.original = 0
		WHERE
		  sub.id = ?
		`,
		func(rows *sql.Rows) {},
		id,
	)

	if err != nil {
		utility.SendError(err, w, req)
		return
	}

	err = database.PerformQuery(
		`
		SELECT
			id,
			filename
		FROM
			film_files
		WHERE
			subscription_id = ?
		`,
		func(rows *sql.Rows) {
			for rows.Next() {
				var (
					fileId   int64
					filename string
				)
				rows.Scan(
					&fileId,
					&filename,
				)

				if _, err = os.Stat("./files/" + filename); !os.IsNotExist(err) {
					err = os.Remove("./files/" + filename)
					if err != nil {
						continue
					}

					_, err = database.DBCon.Exec(
						`
						DELETE FROM
							film_files
						WHERE
							id = ?
						`,
						fileId,
					)
				}
			}
		},
		id,
	)

	utility.CustomData{}.OK(w, req)
}

func InsertSubscription(w http.ResponseWriter, req *http.Request) {
	// Get permission of requesting user
	permission := authentication.GetPermissions(req, "accounts")

	if permission < 2 {
		utility.CustomData{}.Unauthorized(w, req)
	}

	var err error

	sub := Subscriptions{
		Prices: Prices{},
	}

	err = tools.DecodeJson(&sub, req)
	if err != nil {
		utility.SendError(err, w, req)
		return
	}

	res, err := database.DBCon.Exec(
		`
		INSERT INTO
			subscriptions
		(
			user_id,
			film_id,
			start_date,
			end_date,
			type
		)
		VALUES (?,?,?,?,?)
		`,
		sub.UserId,
		sub.FilmId,
		sub.StartDate,
		sub.EndDate,
		sub.Type,
	)

	if err != nil {
		utility.SendError(err, w, req)
		return
	}

	sub.Id, err = res.LastInsertId()
	if err != nil {
		utility.SendError(err, w, req)
		return
	}

	database.PerformQuery(
		`
		REPLACE INTO
			subscriptions_options
			(
				subscription_id,
				price,
				price_en,
				price_se,
				price_no,
				price_fi,
				price_fa,
				price_ar,
				price_hr,
				currency
			)
			VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?, ?)
		`,
		func(rows *sql.Rows) {},
		sub.Id,
		sub.Prices.Base,
		sub.Prices.En,
		sub.Prices.Se,
		sub.Prices.No,
		sub.Prices.Fi,
		sub.Prices.Fa,
		sub.Prices.Ar,
		sub.Prices.Hr,
		sub.Prices.Cur,
	)
	if err != nil {
		utility.SendError(err, w, req)
		return
	}

	utility.CustomData{
		Data: map[string]interface{}{
			"id": sub.Id,
		},
	}.OK(w, req)
}
