/**
 * medfilm-ccms
 * Copyright (C) Sunstate AB - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Simon Wikstrand <simon@sunstate.se>, 2016-12-22
 */

package accounts

import (
	"database/sql"
	"fmt"
	"net/http"
	"strings"
	"time"

	"github.com/MedFilm/ccms/lib/api/accounts/subscriptions"
	"github.com/MedFilm/ccms/lib/api/prospects"
	"github.com/MedFilm/ccms/lib/api/utility"
	"github.com/MedFilm/ccms/lib/authentication"
	"github.com/MedFilm/ccms/lib/database"
	"github.com/MedFilm/ccms/lib/tools"
	"golang.org/x/crypto/bcrypt"
)

type accountsData struct {
	Id             int64                         `json:"id"`
	Username       database.NullString           `json:"username"`
	CompanyName    string                        `json:"companyName"`
	Password       string                        `json:"password,omitempty"`
	AccountType    string                        `json:"type"`
	Slug           database.NullString           `json:"slug"`
	Sla            bool                          `json:"sla"`
	Language       accountsLanguage              `json:"languages"`
	Contact        contact                       `json:"contact"`
	LogoType       logos                         `json:"logos"`
	Messages       messages                      `json:"messages"`
	Numbers        numbers                       `json:"numbers"`
	Prospects      []prospects.Country           `json:"prospects"`
	Subscriptions  []subscriptions.Subscriptions `json:"subscriptions"`
	BrowseDatabase bool                          `json:"browseDatabase"`
}

type accountsLanguage struct {
	Website database.NullString `json:"website"`
	Film    database.NullString `json:"film"`
	Email   database.NullString `json:"email"`
}

type contact struct {
	Name  database.NullString `json:"name"`
	Email database.NullString `json:"email"`
	Phone database.NullString `json:"phone"`
}

type logos struct {
	Film      database.NullString `json:"film"`
	FilmBase  database.NullString `json:"filmBase"`
	Index     database.NullString `json:"index"`
	IndexBase database.NullString `json:"indexBase"`
}

type messages struct {
	Film  filmDescription  `json:"filmDescription"`
	Index indexDescription `json:"indexDescription"`
}

type filmDescription struct {
	Se database.NullString `json:"se"`
	En database.NullString `json:"en"`
	Ar database.NullString `json:"ar"`
	Hr database.NullString `json:"hr"`
	No database.NullString `json:"no"`
	Fi database.NullString `json:"fi"`
	Fa database.NullString `json:"Fa"`
}

type indexDescription struct {
	Se database.NullString `json:"se"`
	En database.NullString `json:"en"`
	Ar database.NullString `json:"ar"`
	Hr database.NullString `json:"hr"`
	No database.NullString `json:"no"`
	Fi database.NullString `json:"fi"`
	Fa database.NullString `json:"Fa"`
}

type numbers struct {
	Films      int64  `json:"films"`
	Tablets    int64  `json:"tablets"`
	Views      int64  `json:"views"`
	ExpDate    int64  `json:"expDate"`
	Sla        bool   `json:"sla"`
	TotalPrice int64  `json:"totalPrice"`
	Currency   string `json:"currency"`
}

func GetAccounts(w http.ResponseWriter, req *http.Request) {
	// Get permission of requesting user
	permission := authentication.GetPermissions(req, "accounts")

	if permission == 0 {
		utility.CustomData{}.Unauthorized(w, req)
		return
	}

	// Initiate result slice
	var result []accountsData

	err := database.PerformQuery(
		`
		SELECT
			u.id,
			u.username,
			pro.hospital,
			u.company_logo,
			u.company_header,
			u.type,
			u.message_en,
			u.message_se,
			u.message_no,
			u.message_fi,
			u.message_fa,
			u.message_ar,
			u.message_hr,
			u.description_en,
			u.description_se,
			u.description_no,
			u.description_fi,
			u.description_fa,
			u.description_ar,
			u.description_hr,
			u.website_language,
			u.film_language,
			u.email_language,
			u.can_patients_browse_database,
			u.slug,
			u.sla
		FROM
			users u
		INNER JOIN
			prospects pro ON pro.id = (
				SELECT
					id
				FROM
					prospects
				WHERE
					user_id = u.id
				ORDER BY
					id DESC
				LIMIT 1
			)
		`,
		func(rows *sql.Rows) {
			var err error
			// Loop through all rows
			for rows.Next() {

				// Create struct for the values
				account := accountsData{
					Language: accountsLanguage{},
					Contact:  contact{},
					LogoType: logos{},
					Messages: messages{
						Film:  filmDescription{},
						Index: indexDescription{},
					},
					Numbers: numbers{
						Films:      0,
						Tablets:    0,
						Views:      0,
						Sla:        false,
						TotalPrice: 0,
					},
				}

				// Scan all values
				rows.Scan(
					&account.Id,
					&account.Username,
					&account.CompanyName,
					&account.LogoType.Film,
					&account.LogoType.Index,
					&account.AccountType,
					&account.Messages.Film.En,
					&account.Messages.Film.Se,
					&account.Messages.Film.No,
					&account.Messages.Film.Fi,
					&account.Messages.Film.Fa,
					&account.Messages.Film.Ar,
					&account.Messages.Film.Hr,
					&account.Messages.Index.En,
					&account.Messages.Index.Se,
					&account.Messages.Index.No,
					&account.Messages.Index.Fi,
					&account.Messages.Index.Fa,
					&account.Messages.Index.Ar,
					&account.Messages.Index.Hr,
					&account.Language.Website,
					&account.Language.Film,
					&account.Language.Email,
					&account.BrowseDatabase,
					&account.Slug,
					&account.Sla,
				)

				account.Subscriptions, err = subscriptions.GetSubscriptionsModel(account.Id)
				if err != nil {
					continue
				}

				account.Numbers, err = getNumbers(account.Id)
				if err != nil {
					continue
				}

				if account.Id != 0 {
					result = append(result, account)
				}
			}
		})
	if err != nil {
		utility.CustomData{
			Data: map[string]interface{}{
				"errorMessage": err.Error(),
			},
		}.NotFound(w, req)
		return
	}

	utility.CustomData{Data: map[string]interface{}{
		"result": result,
	}}.OK(w, req)
}

func GetSingleAccount(w http.ResponseWriter, req *http.Request) {
	// Get permission of requesting user
	permission := authentication.GetPermissions(req, "accounts")

	if permission == 0 {
		utility.CustomData{}.Unauthorized(w, req)
		return
	}

	id, err := tools.GetIdFromUrl(req)
	if err != nil {
		utility.SendError(err, w, req)
		return
	}

	// Create struct for the values
	account := accountsData{
		Language: accountsLanguage{},
		Contact:  contact{},
		LogoType: logos{},
		Messages: messages{
			Film:  filmDescription{},
			Index: indexDescription{},
		},
		Numbers: numbers{
			Films:      0,
			Tablets:    0,
			Views:      0,
			Sla:        false,
			TotalPrice: 0,
		},
	}

	// Query database
	err = database.DBCon.QueryRow(
		`
		SELECT
			u.id,
			u.username,
			pro.hospital,
			u.company_logo,
			u.company_header,
			u.type,
			u.message_en,
			u.message_se,
			u.message_no,
			u.message_fi,
			u.message_fa,
			u.message_ar,
			u.message_hr,
			u.description_en,
			u.description_se,
			u.description_no,
			u.description_fi,
			u.description_fa,
			u.description_ar,
			u.description_hr,
			u.website_language,
			u.film_language,
			u.email_language,
			u.can_patients_browse_database,
			u.slug,
			u.sla
		FROM
			users u
		INNER JOIN
			prospects pro ON pro.id = (
				SELECT
					id
				FROM
					prospects
				WHERE
					user_id = u.id
				ORDER BY
					id DESC
				LIMIT 1
			)
		WHERE
			u.id=?
		`,
		id,
	).Scan(
		&account.Id,
		&account.Username,
		&account.CompanyName,
		&account.LogoType.Film,
		&account.LogoType.Index,
		&account.AccountType,
		&account.Messages.Film.En,
		&account.Messages.Film.Se,
		&account.Messages.Film.No,
		&account.Messages.Film.Fi,
		&account.Messages.Film.Fa,
		&account.Messages.Film.Ar,
		&account.Messages.Film.Hr,
		&account.Messages.Index.En,
		&account.Messages.Index.Se,
		&account.Messages.Index.No,
		&account.Messages.Index.Fi,
		&account.Messages.Index.Fa,
		&account.Messages.Index.Ar,
		&account.Messages.Index.Hr,
		&account.Language.Website,
		&account.Language.Film,
		&account.Language.Email,
		&account.BrowseDatabase,
		&account.Slug,
		&account.Sla,
	)

	if err != nil {
		utility.SendError(err, w, req)
		return
	}

	if account.Id == 0 {
		utility.CustomData{}.NotFound(w, req)
		return
	}

	account.Prospects, err = prospects.GetProspectsByUserId(account.Id)
	if err != nil {
		utility.SendError(err, w, req)
		return
	}

	// Get account numbers
	account.Numbers, err = getNumbers(account.Id)
	if err != nil {
		utility.SendError(err, w, req)
		return
	}

	// Get base64 encode of Film image
	if account.LogoType.Film.Valid {
		var fileName = getFileName(account.LogoType.Film.String)
		account.LogoType.FilmBase.String, err = tools.ConvImageToBase64("./images/logos/" + fileName)
		if err != nil {
			account.LogoType.FilmBase.Valid = false
		} else {
			account.LogoType.FilmBase.Valid = true
		}
	}

	// Get base64 encode of Index image
	if account.LogoType.Index.Valid {
		var fileName = getFileName(account.LogoType.Index.String)
		account.LogoType.IndexBase.String, err = tools.ConvImageToBase64("./images/logos/" + fileName)
		if err != nil {
			account.LogoType.IndexBase.Valid = false
		} else {
			account.LogoType.IndexBase.Valid = true
		}
	}

	account.Subscriptions, err = subscriptions.GetSubscriptionsModel(account.Id)

	utility.CustomData{Data: map[string]interface{}{
		"result": account,
	}}.OK(w, req)
}

func getFileName(path string) string {
	i := strings.Index(path, "/")
	for i > -1 {
		path = path[i+1:]
		i = strings.Index(path, "/")
	}

	return path
}

func getNumbers(userId int64) (numbers, error) {

	// Create object that is going to be returned
	Number := numbers{
		Films:      0,
		Tablets:    0,
		Views:      0,
		Sla:        false,
		TotalPrice: 0,
	}

	err := database.PerformQuery(
		`
		SELECT
			sub.views,
			sub.end_date,
			(
				subo.price +
				subo.price_en +
				subo.price_se +
				subo.price_no +
				subo.price_fi +
				subo.price_fa +
				subo.price_ar +
				subo.price_hr
			) AS 'price',
			subo.currency
		FROM
			subscriptions sub
		LEFT JOIN
			subscriptions_options subo ON sub.id = subo.subscription_id
		WHERE
			user_id=?
		`,
		func(rows *sql.Rows) {
			for rows.Next() {
				var (
					views      int64
					expiryDate int64
					price      int64
					currency   database.NullString
				)
				rows.Scan(
					&views,
					&expiryDate,
					&price,
					&currency,
				)

				if expiryDate < time.Now().Unix() {
					continue
				}

				Number.Views += views
				if Number.ExpDate == 0 || Number.ExpDate > expiryDate {
					Number.ExpDate = expiryDate
				}
				if currency.Valid {
					Number.Currency = currency.String
				}
				Number.TotalPrice += price
				Number.Films++
			}
		},
		userId,
	)
	if err != nil {
		return Number, err
	}

	return Number, nil
}

func UpdateAccount(w http.ResponseWriter, req *http.Request) {
	// Get permission of requesting user
	permission := authentication.GetPermissions(req, "accounts")

	if permission < 2 {
		utility.CustomData{}.Unauthorized(w, req)
	}

	id, err := tools.GetIdFromUrl(req)
	if err != nil {
		utility.SendError(err, w, req)
		return
	}

	// Create struct for the values
	account := accountsData{
		Language: accountsLanguage{},
		Contact:  contact{},
		LogoType: logos{},
		Messages: messages{
			Film:  filmDescription{},
			Index: indexDescription{},
		},
	}

	err = tools.DecodeJson(&account, req)
	if err != nil {
		utility.SendError(err, w, req)
		return
	}

	if account.LogoType.FilmBase.Valid {
		randString := tools.RandStringRunes(13)
		err = tools.ConvImageToBinary("./images/logos/"+randString+".png", account.LogoType.FilmBase.String)
		if err != nil {
			utility.SendError(err, w, req)
			return
		}

		account.LogoType.Film.String = randString + ".png"
		account.LogoType.Film.Valid = true
	}

	if account.LogoType.IndexBase.Valid {
		randString := tools.RandStringRunes(13)
		err = tools.ConvImageToBinary("./images/logos/"+randString+".png", account.LogoType.IndexBase.String)
		if err != nil {
			utility.SendError(err, w, req)
			return
		}

		account.LogoType.Index.String = randString + ".png"
		account.LogoType.Index.Valid = true
	}

	fmt.Println(
		account.Username,
		account.LogoType.Film,
		account.LogoType.Index,
		account.AccountType,
		account.Messages.Film.En,
		account.Messages.Film.Se,
		account.Messages.Film.No,
		account.Messages.Film.Fi,
		account.Messages.Film.Fa,
		account.Messages.Film.Ar,
		account.Messages.Film.Hr,
		account.Messages.Index.En,
		account.Messages.Index.Se,
		account.Messages.Index.No,
		account.Messages.Index.Fi,
		account.Messages.Index.Fa,
		account.Messages.Index.Ar,
		account.Messages.Index.Hr,
		account.Language.Website,
		account.Language.Film,
		account.Language.Email,
		account.BrowseDatabase,
		account.Slug,
		account.Sla,
		id,
	)

	err = database.PerformQuery(
		`
		UPDATE
			users
		SET
			username=?,
			company_logo=?,
			company_header=?,
			type=?,
			message_en=?,
			message_se=?,
			message_no=?,
			message_fi=?,
			message_fa=?,
			message_ar=?,
			message_hr=?,
			description_en=?,
			description_se=?,
			description_no=?,
			description_fi=?,
			description_fa=?,
			description_ar=?,
			description_hr=?,
			website_language=?,
			film_language=?,
			email_language=?,
			can_patients_browse_database=?,
			slug=?,
			sla=?
		WHERE
			id=?
		`,
		func(rows *sql.Rows) {},
		account.Username,
		account.LogoType.Film,
		account.LogoType.Index,
		account.AccountType,
		account.Messages.Film.En,
		account.Messages.Film.Se,
		account.Messages.Film.No,
		account.Messages.Film.Fi,
		account.Messages.Film.Fa,
		account.Messages.Film.Ar,
		account.Messages.Film.Hr,
		account.Messages.Index.En,
		account.Messages.Index.Se,
		account.Messages.Index.No,
		account.Messages.Index.Fi,
		account.Messages.Index.Fa,
		account.Messages.Index.Ar,
		account.Messages.Index.Hr,
		account.Language.Website,
		account.Language.Film,
		account.Language.Email,
		account.BrowseDatabase,
		account.Slug,
		account.Sla,
		id,
	)

	fmt.Println(err)

	if err != nil {
		utility.SendError(err, w, req)
		return
	}

	if account.Password != "" {
		hashedPassword, err := bcrypt.GenerateFromPassword([]byte(account.Password), bcrypt.DefaultCost)

		err = database.PerformQuery(
			`
			UPDATE
				users
			SET
				password=?
			WHERE
				id=?
			`,
			func(rows *sql.Rows) {},
			hashedPassword,
			account.Id,
		)
		if err != nil {
			utility.SendError(err, w, req)
			return
		}
	}

	utility.CustomData{}.OK(w, req)
}

func DeleteAccount(w http.ResponseWriter, req *http.Request) {
	// Get permission of requesting user
	permission := authentication.GetPermissions(req, "accounts")

	if permission < 2 {
		utility.CustomData{}.Unauthorized(w, req)
	}

	id, err := tools.GetIdFromUrl(req)
	if err != nil {
		utility.SendError(err, w, req)
		return
	}

	err = database.PerformQuery(
		`
		DELETE FROM
			users
		WHERE
			id=?
		`,
		func(rows *sql.Rows) {},
		id,
	)
	if err != nil {
		utility.SendError(err, w, req)
		return
	}

	utility.CustomData{}.OK(w, req)
}
