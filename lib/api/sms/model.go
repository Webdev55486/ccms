package sms

type entry struct {
	ID         int
	ReceiverID int
	MessageID  int
	UserID     int
	Time       string
	Days       string
	DaysJSON   []int
}

type message struct {
	ID      int
	Label   string
	Content string
}

type receiver struct {
	ID          int
	Firstname   string
	Lastname    string
	Phonenumber string
}
