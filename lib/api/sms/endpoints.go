package sms

import (
	"database/sql"
	"encoding/json"
	"fmt"
	"log"
	"strconv"
	"strings"
	"time"

	"github.com/MedFilm/ccms/lib/database"
	"github.com/roylee0704/gron/xtime"

	"github.com/MedFilm/ccms/lib/config"
	"github.com/roylee0704/gron"
	textmagic "github.com/textmagic/textmagic-rest-go"
)

var client *textmagic.Client

// InitCron function
func InitCron() {
	InitClient()
	c := gron.New()
	c.AddFunc(gron.Every(1*xtime.Minute), cronJobSend)
	c.AddFunc(gron.Every(1*xtime.Day), cronJobDelete)
	c.Start()
}

// InitClient function
func InitClient() {
	client = textmagic.NewClient(config.BuiltConfig.Sms.Username, config.BuiltConfig.Sms.Password)
}

// SendMessage function
func sendMessage(p textmagic.Params) {
	_, err := client.CreateMessage(p)
	if err != nil {
		log.Println("ERROR: " + err.Error())
	} else {
		log.Println("Sent text")
	}
}

// Delete receivers that are passed end date
func cronJobDelete() {
	fmt.Println("Deleting sms receivers who passed the deadline...")
	err := database.PerformQuery(`
		delete from sms_receiver
		where 
			date_format(current_timestamp, '%Y-%m-%d') > date_format(enddate, '%Y-%m-%d')
	`, func(rows *sql.Rows) {})
	if err != nil {
		log.Println(err.Error())
		return
	}
}

// Create statistics for sent message
func createStat(e entry) (id int64, err error) {
	res, err := database.DBCon.Exec(`
		insert into sms_stat(entry_id)
		values(?)
	`, e.ID)
	if err != nil {
		return
	}
	id, err = res.LastInsertId()
	return
}

// Send texts for entries that match date and time
func cronJobSend() {
	fmt.Println("Running sms reminder cronjob...")
	entries := make([]entry, 0)

	// SET TIMEZONE
	err := database.PerformQuery(`
		set time_zone = '+1:00'
	`, func(rows *sql.Rows) {})
	if err != nil {
		log.Println(err.Error())
		return
	}

	err = database.PerformQuery(`
		select 
			s.id,
			s.receiver_id,
			s.message_id,
			s.user_id,
			s.time,
			s.days
		from 
			sms_entry as s,
			sms_receiver as r
		where
			date_format(s.time, '%H:%i')=date_format(current_timestamp, '%H:%i') and
			r.id=s.receiver_id and
			date_format(current_timestamp, '%Y-%m-%d') >= date_format(r.startdate, '%Y-%m-%d') and
			date_format(current_timestamp, '%Y-%m-%d') <= date_format(r.enddate, '%Y-%m-%d')
	`, func(rows *sql.Rows) {
		for rows.Next() {
			e := entry{}
			err := rows.Scan(
				&e.ID,
				&e.ReceiverID,
				&e.MessageID,
				&e.UserID,
				&e.Time,
				&e.Days,
			)
			if err != nil {
				log.Println(err.Error())
				return
			}
			var arr []int
			err = json.Unmarshal([]byte(e.Days), &arr)
			if err != nil {
				log.Println(err.Error())
				return
			}
			e.DaysJSON = arr
			entries = append(entries, e)
		}
	})
	if err != nil {
		log.Println(err.Error())
	}

	for _, e := range entries {
		dayofweek := time.Now().Weekday() - 1
		if dayofweek == -1 {
			dayofweek = 6
		}
		if e.DaysJSON[dayofweek] == 1 {
			// SEND SMS
			m := message{}
			r := receiver{}

			err = database.PerformQuery(`
				select
					r.phonenumber,
					m.content
				from
					sms_receiver as r,
					sms_message as m
				where 
					r.id=? and m.id=?	
			`, func(rows *sql.Rows) {
				for rows.Next() {
					err := rows.Scan(
						&r.Phonenumber,
						&m.Content,
					)
					if err != nil {
						log.Println(err.Error())
						return
					}
				}
			}, e.ReceiverID, e.MessageID)

			if err != nil {
				log.Println(err.Error())
				return
			}

			// Create stat record for message
			id, err := createStat(e)
			if err != nil {
				log.Println(err.Error())
				return
			}
			// insert id into message
			content := strings.Replace(m.Content, "{id}", strconv.Itoa(int(id)), -1)

			p := map[string]string{
				"phones": r.Phonenumber,
				"text":   content,
			}
			// Send message
			go sendMessage(p)
		}
	}
}

func messageUrlEncode(p *textmagic.Params) {}

func validateParams(p textmagic.Params) bool {
	return true
}
