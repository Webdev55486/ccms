/**
 * medfilm-ccms
 * Copyright (C) Sunstate AB - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Simon Wikstrand <simon@sunstate.se>, 2016-12-27
 */

package prospects

import (
	"database/sql"
	"net/http"
	"strings"

	"github.com/MedFilm/ccms/lib/api/prospects/addresses"
	"github.com/MedFilm/ccms/lib/api/prospects/comment"
	"github.com/MedFilm/ccms/lib/api/prospects/contact"
	"github.com/MedFilm/ccms/lib/api/utility"
	"github.com/MedFilm/ccms/lib/authentication"
	"github.com/MedFilm/ccms/lib/database"
	"github.com/MedFilm/ccms/lib/tools"
)

type Country struct {
	Id      int64    `json:"id"`
	Name    string   `json:"name"`
	Active  bool     `json:"active"`
	Regions []Region `json:"regions"`
}

type Region struct {
	Id        int64      `json:"id"`
	CountryId int64      `json:"countryId"`
	Name      string     `json:"name"`
	Active    bool       `json:"active"`
	Prospects []Prospect `json:"prospect"`
}

type Prospect struct {
	Id             int64                 `json:"id"`
	Hospital       database.NullString   `json:"hospital"`
	CountryRegion  int64                 `json:"region"`
	Clinic         database.NullString   `json:"clinic"`
	CategoryName   database.NullString   `json:"categoryName"`
	Contact        []contact.Contact     `json:"contact"`
	Reminder       database.NullString   `json:"reminder"`
	ReminderTime   database.NullString   `json:"reminderTime"`
	ReminderText   database.NullString   `json:"reminderText"`
	MainContact    database.NullString   `json:"mainContact"`
	RefNumber      database.NullString   `json:"refNumber"`
	VatNumber      database.NullString   `json:"vatNumber"`
	RegionalOffice bool                  `json:"regionalOffice"`
	PhoneFirst     database.NullString   `json:"phoneFirst"`
	PhoneSecond    database.NullString   `json:"phoneSecond"`
	Addresses      addresses.BaseAddress `json:"addresses"`
	AccountableId  database.NullInt      `json:"accountableId"`
	Active         bool                  `json:"active"`
	Accountable    database.NullString   `json:"accountable"`
	Colour         database.NullString   `json:"colour"`
	Comments       []comment.Comment     `json:"comments"`
	Updated        string                `json:"updated"`
	Created        string                `json:"created"`
}

type KAM struct {
	Id   int64  `json:"id"`
	Name string `json:"name"`
}

type Category struct {
	Id   int64  `json:"id"`
	Name string `json:"name"`
}

func GetProspects(w http.ResponseWriter, req *http.Request) {

	// Get permission of requesting user
	permission := authentication.GetPermissions(req, "prospects")
	if permission == 0 {
		utility.CustomData{}.Unauthorized(w, req)
		return
	}

	// Initiate top layer struct
	// var resultCountries []Country
	resultCountries := make([]Country, 0)

	err := database.PerformQuery(
		`
		SELECT
			co.id,
			co.name,
			co.active,
			cr.id,
			cr.name,
			cr.active,
			pro.id,
			pro.active,
			pro.hospital,
			pro.clinic,
			pro.colour,
			pro.main_contact,
			pro.ref_number,
			pro.vat_number,
			pro.regional_office,
			pro.phone_first,
			pro.phone_second,
			pro.categoryName,
			emp.id,
			emp.given_name,
			pro.reminder,
			pro.reminder_time,
			pro.reminder_text,
			pro.updated,
			pro.created
		FROM
			prospects pro
		INNER JOIN
			employees emp ON pro.accountable = emp.id
		RIGHT OUTER JOIN
			country_region cr ON pro.country_region = cr.id
		RIGHT OUTER JOIN
			countries co ON cr.country_id = co.id
		WHERE
			pro.user_id IS NULL
		ORDER BY
			co.name ASC, cr.name ASC, pro.hospital ASC
		`,
		func(rows *sql.Rows) {
			var err error
			// Fetch and assign all countries
			for rows.Next() {

				country := Country{}
				region := Region{}

				// Create a struct for prospect, as every row have a new prospect
				prospect := Prospect{}

				rows.Scan(
					&country.Id,
					&country.Name,
					&country.Active,
					&region.Id,
					&region.Name,
					&region.Active,
					&prospect.Id,
					&prospect.Active,
					&prospect.Hospital,
					&prospect.Clinic,
					&prospect.Colour,
					&prospect.MainContact,
					&prospect.RefNumber,
					&prospect.VatNumber,
					&prospect.RegionalOffice,
					&prospect.PhoneFirst,
					&prospect.PhoneSecond,
					&prospect.CategoryName,
					&prospect.AccountableId,
					&prospect.Accountable,
					&prospect.Reminder,
					&prospect.ReminderTime,
					&prospect.ReminderText,
					&prospect.Updated,
					&prospect.Created,
				)

				// Figure out if country, region or prospect is active or archived
				if !country.Active || (region.Id != 0 && !region.Active) || (prospect.Id != 0 && !prospect.Active) {
					continue
				}

				if prospect.Id != 0 {
					prospect.Comments, err = comment.GetCommentByProspectId(prospect.Id)
					if err != nil {
						utility.SendError(err, w, req)
						return
					}

					prospect.Contact, err = contact.GetCommentsByProspect(prospect.Id)
					if err != nil {
						utility.SendError(err, w, req)
						return
					}

					prospect.Addresses, err = addresses.GetAddressesByProspect(prospect.Id)
					if err != nil {
						utility.SendError(err, w, req)
						return
					}
				}

				/**
				 * This will create a nested struct array and append existing structs instead
				 * of creating the same ones for every country and region. If structs has already
				 * been created in the array it will append to it, otherwise append new structs.
				 */
				exist := false
				for cIndx, cVal := range resultCountries {
					if cVal.Id == country.Id {
						// Country exists
						exist = true

						// Check if region exists
						regionExist := false
						for rIndx, rVal := range cVal.Regions {
							// If region was already found, just append prospect to it
							if rVal.Id == region.Id {
								resultCountries[cIndx].Regions[rIndx].Prospects = append(resultCountries[cIndx].Regions[rIndx].Prospects, prospect)
								regionExist = true
								break
							}
						}

						// If region did not exist append new region and prospect to previously created country
						if !regionExist {
							if prospect.Id != 0 {
								region.Prospects = append(region.Prospects, prospect)
							}
							resultCountries[cIndx].Regions = append(resultCountries[cIndx].Regions, region)
						}

						break
					}
				}

				// If country did not exist, append new country, region and prospect
				if !exist {
					if prospect.Id != 0 {
						region.Prospects = append(region.Prospects, prospect)
					}

					if region.Id != 0 {
						country.Regions = append(country.Regions, region)
					}

					resultCountries = append(resultCountries, country)
				}
			}
		},
	)

	if err != nil {
		utility.SendError(err, w, req)
		return
	}

	utility.CustomData{Data: map[string]interface{}{
		"result": resultCountries,
	}}.OK(w, req)
}

func GetProspectsByUserId(id int64) ([]Country, error) {

	// Initiate top layer struct
	var resultCountries []Country

	region := Region{
		Id:   0,
		Name: "Information",
	}

	err := database.PerformQuery(
		`
		SELECT
			pro.id,
			pro.active,
			pro.hospital,
			pro.clinic,
			pro.colour,
			pro.main_contact,
			pro.ref_number,
			pro.vat_number,
			pro.regional_office,
			pro.phone_first,
			pro.phone_second,
			pro.categoryName,
			emp.id,
			emp.given_name,
			pro.reminder,
			pro.reminder_time,
			pro.reminder_text,
			pro.updated,
			pro.created
		FROM
			prospects pro
		LEFT OUTER JOIN
			employees emp ON pro.accountable = emp.id
		WHERE
			pro.user_id = ? AND pro.active = 1
		`,
		func(rows *sql.Rows) {
			var err error
			// Fetch and assign all countries
			for rows.Next() {

				// Create a struct for prospect, as every row have a new prospect
				prospect := Prospect{}

				rows.Scan(
					&prospect.Id,
					&prospect.Active,
					&prospect.Hospital,
					&prospect.Clinic,
					&prospect.Colour,
					&prospect.MainContact,
					&prospect.RefNumber,
					&prospect.VatNumber,
					&prospect.RegionalOffice,
					&prospect.PhoneFirst,
					&prospect.PhoneSecond,
					&prospect.CategoryName,
					&prospect.AccountableId,
					&prospect.Accountable,
					&prospect.Reminder,
					&prospect.ReminderTime,
					&prospect.ReminderText,
					&prospect.Updated,
					&prospect.Created,
				)

				if prospect.Id != 0 {
					prospect.Comments, err = comment.GetCommentByProspectId(prospect.Id)
					if err != nil {
						return
					}

					prospect.Contact, err = contact.GetCommentsByProspect(prospect.Id)
					if err != nil {
						return
					}

					prospect.Addresses, err = addresses.GetAddressesByProspect(prospect.Id)
					if err != nil {
						return
					}
				}

				region.Prospects = append(region.Prospects, prospect)
			}
		},
		id,
	)

	if err != nil {
		return resultCountries, err
	}

	country := Country{
		Id:   0,
		Name: "Information",
	}

	country.Regions = append(country.Regions, region)

	resultCountries = append(resultCountries, country)

	return resultCountries, nil
}

func GetKAM(w http.ResponseWriter, req *http.Request) {

	// Get permission of requesting user
	permission := authentication.GetPermissions(req, "prospects")

	if permission == 0 {
		utility.CustomData{}.Unauthorized(w, req)
		return
	}

	// Fill user struct
	var users []KAM

	err := database.PerformQuery(
		`
		SELECT
			emp.id,
			emp.given_name
		FROM
			employees emp
		INNER JOIN
			employee_permission ep ON emp.id = ep.emp_id
		WHERE
			ep.permission = 'prospects' AND ep.level > 0
		`,
		func(rows *sql.Rows) {
			for rows.Next() {
				var singleUser = KAM{}
				rows.Scan(&singleUser.Id, &singleUser.Name)
				users = append(users, singleUser)
			}
		},
	)
	if err != nil {
		utility.CustomData{
			Data: map[string]interface{}{
				"errorMessage": err.Error(),
			},
		}.NotFound(w, req)
		return
	}

	utility.CustomData{Data: map[string]interface{}{
		"result": users,
	}}.OK(w, req)
}

func GetCategories(w http.ResponseWriter, req *http.Request) {

	// Get permission of requesting user
	permission := authentication.GetPermissions(req, "prospects")

	if permission == 0 {
		utility.CustomData{}.Unauthorized(w, req)
		return
	}

	// Fill user struct
	var categories []Category

	err := database.PerformQuery("SELECT id, name FROM film_subject WHERE type = 'category'", func(rows *sql.Rows) {
		for rows.Next() {
			var singleCategory = Category{}
			rows.Scan(&singleCategory.Id, &singleCategory.Name)
			categories = append(categories, singleCategory)
		}
	})
	if err != nil {
		utility.CustomData{
			Data: map[string]interface{}{
				"errorMessage": err.Error(),
			},
		}.NotFound(w, req)
		return
	}

	utility.CustomData{Data: map[string]interface{}{
		"result": categories,
	}}.OK(w, req)
}

func UpdateProspects(w http.ResponseWriter, req *http.Request) {
	// Get permission of requesting user
	permission := authentication.GetPermissions(req, "prospects")
	if permission < 2 {
		utility.CustomData{}.Unauthorized(w, req)
		return
	}

	prospect := Prospect{
		Contact: []contact.Contact{},
	}

	// Get user from request
	err := tools.DecodeJson(&prospect, req)
	if err != nil {
		utility.SendError(err, w, req)
		return
	}

	prospect.Id, err = tools.GetIdFromUrl(req)

	// Insert NULL if string is empty
	if prospect.Reminder.String == "" {
		prospect.Reminder.Valid = false
	}

	err = database.PerformQuery(
		`
		UPDATE
			prospects
			SET
				hospital=?,
				clinic=?,
				categoryName=?,
				accountable=?,
				main_contact=?,
				ref_number=?,
				vat_number=?,
				colour=?,
				reminder=?,
				reminder_text=?,
				reminder_time=?,
				phone_first=?,
				phone_second=?
		WHERE
			id=?
		`,
		func(rows *sql.Rows) {},
		prospect.Hospital,
		prospect.Clinic,
		prospect.CategoryName,
		prospect.AccountableId,
		prospect.MainContact,
		prospect.RefNumber,
		prospect.VatNumber,
		prospect.Colour,
		prospect.Reminder,
		prospect.ReminderText,
		prospect.ReminderTime,
		prospect.PhoneFirst,
		prospect.PhoneSecond,
		prospect.Id,
	)
	for _, singleContact := range prospect.Contact {
		err = contact.InsertContact(singleContact)
		if err != nil {
			utility.SendError(err, w, req)
			return
		}
	}

	for key, singleAddress := range prospect.Addresses {
		singleAddress.Type = key
		singleAddress.ProspectId = &prospect.Id
		err = addresses.InsertAddress(singleAddress)
		if err != nil {
			utility.SendError(err, w, req)
			return
		}
	}

	if err != nil {
		utility.SendError(err, w, req)
		return
	}

	// Return result
	utility.CustomData{}.OK(w, req)
}

func InsertProspect(w http.ResponseWriter, req *http.Request) {

	// Get permission of requesting user
	permission := authentication.GetPermissions(req, "prospects")
	if permission < 2 {
		utility.CustomData{}.Unauthorized(w, req)
		return
	}

	prospect := Prospect{}

	// Get user from request
	err := tools.DecodeJson(&prospect, req)
	if err != nil {
		utility.SendError(err, w, req)
		return
	}

	err = database.PerformQuery("INSERT INTO prospects (hospital, clinic, categoryName, country_region, accountable, regional_office) VALUES(?, ?, ?, ?, ?, ?)", func(rows *sql.Rows) {},
		prospect.Hospital,
		prospect.Clinic,
		prospect.CategoryName,
		prospect.CountryRegion,
		prospect.AccountableId,
		prospect.RegionalOffice,
	)
	if err != nil {
		utility.SendError(err, w, req)
		return
	}

	// Return result
	utility.CustomData{}.OK(w, req)
}

func InsertCountry(w http.ResponseWriter, req *http.Request) {

	// Get permission of requesting user
	permission := authentication.GetPermissions(req, "prospects")
	if permission < 2 {
		utility.CustomData{}.Unauthorized(w, req)
		return
	}

	var country Country
	// Get user from request
	err := tools.DecodeJson(&country, req)
	if err != nil {
		utility.SendError(err, w, req)
		return
	}

	err = database.PerformQuery("INSERT INTO countries (name) VALUES(?)", func(rows *sql.Rows) {}, country.Name)
	if err != nil {
		utility.SendError(err, w, req)
		return
	}

	// Return result
	utility.CustomData{}.OK(w, req)
}

func InsertRegion(w http.ResponseWriter, req *http.Request) {

	// Get permission of requesting user
	permission := authentication.GetPermissions(req, "prospects")
	if permission < 2 {
		utility.CustomData{}.Unauthorized(w, req)
		return
	}

	var region Region

	// Get user from request
	err := tools.DecodeJson(&region, req)
	if err != nil {
		utility.SendError(err, w, req)
		return
	}

	err = database.PerformQuery("INSERT INTO country_region (name, country_id) VALUES(?, ?)", func(rows *sql.Rows) {}, region.Name, region.CountryId)
	if err != nil {
		utility.SendError(err, w, req)
		return
	}
	// Return result
	utility.CustomData{}.OK(w, req)
}

func DeleteCountry(w http.ResponseWriter, req *http.Request) {

	// Get permission of requesting user
	permission := authentication.GetPermissions(req, "prospects")

	// Require edit level
	if permission != 2 {
		utility.CustomData{}.Unauthorized(w, req)
		return
	}

	var country Country
	var err error

	country.Id, err = tools.GetIdFromUrl(req)
	if err != nil {
		utility.SendError(err, w, req)
		return
	}

	err = database.PerformQuery("UPDATE countries SET active = 0 WHERE id=?", func(rows *sql.Rows) {}, country.Id)
	if err != nil {
		utility.SendError(err, w, req)
		return
	}

	// Return result
	utility.CustomData{}.OK(w, req)
}

func DeleteRegion(w http.ResponseWriter, req *http.Request) {

	// Get permission of requesting user
	permission := authentication.GetPermissions(req, "prospects")

	// Require edit level
	if permission != 2 {
		utility.CustomData{}.Unauthorized(w, req)
		return
	}

	var region Region
	var err error

	region.Id, err = tools.GetIdFromUrl(req)
	if err != nil {
		utility.SendError(err, w, req)
		return
	}

	err = database.PerformQuery("UPDATE country_region SET active = 0 WHERE id=?", func(rows *sql.Rows) {}, region.Id)
	if err != nil {
		utility.SendError(err, w, req)
		return
	}

	// Return result
	utility.CustomData{}.OK(w, req)
}

func DeleteProspect(w http.ResponseWriter, req *http.Request) {

	// Get permission of requesting user
	permission := authentication.GetPermissions(req, "prospects")
	if permission < 2 {
		utility.CustomData{}.Unauthorized(w, req)
		return
	}

	// Get id of prospect
	Id, err := tools.GetIdFromUrl(req)

	err = database.PerformQuery("UPDATE prospects SET active=? WHERE id=?", func(rows *sql.Rows) {}, 0, Id)
	if err != nil {
		utility.SendError(err, w, req)
		return
	}

	// Return result
	utility.CustomData{}.OK(w, req)
}

func ConvertProspect(w http.ResponseWriter, req *http.Request) {
	// Get permission of requesting user
	permission := authentication.GetPermissions(req, "prospects")
	if permission < 2 {
		utility.CustomData{}.Unauthorized(w, req)
		return
	}

	// Get id of prospect
	Id, err := tools.GetIdFromUrl(req)

	country := Country{}
	region := Region{}

	// Create a struct for prospect, as every row have a new prospect
	prospect := Prospect{}

	err = database.DBCon.QueryRow(
		`
		SELECT
		co.id,
			co.name,
			co.active,
			cr.id,
			cr.name,
			cr.active,
			pro.id,
			pro.active,
			pro.hospital,
			pro.clinic,
			pro.colour,
			pro.main_contact,
			pro.ref_number,
			pro.vat_number,
			pro.regional_office,
			pro.phone_first,
			pro.phone_second,
			pro.categoryName,
			emp.id,
			emp.given_name,
			pro.reminder,
			pro.reminder_time,
			pro.reminder_text,
			pro.updated,
			pro.created
		FROM
		prospects pro
		INNER JOIN
		employees emp ON pro.accountable = emp.id
		RIGHT OUTER JOIN
		country_region cr ON pro.country_region = cr.id
		RIGHT OUTER JOIN
		countries co ON cr.country_id = co.id
		WHERE
		pro.id = ?
		ORDER BY
		co.name ASC, cr.name ASC, pro.hospital ASC
		`,
		Id,
	).Scan(
		&country.Id,
		&country.Name,
		&country.Active,
		&region.Id,
		&region.Name,
		&region.Active,
		&prospect.Id,
		&prospect.Active,
		&prospect.Hospital,
		&prospect.Clinic,
		&prospect.Colour,
		&prospect.MainContact,
		&prospect.RefNumber,
		&prospect.VatNumber,
		&prospect.RegionalOffice,
		&prospect.PhoneFirst,
		&prospect.PhoneSecond,
		&prospect.CategoryName,
		&prospect.AccountableId,
		&prospect.Accountable,
		&prospect.Reminder,
		&prospect.ReminderTime,
		&prospect.ReminderText,
		&prospect.Updated,
		&prospect.Created,
	)

	if err != nil {
		utility.SendError(err, w, req)
		return
	}

	var usrId int64
	err = database.PerformQuery(
		`
		SELECT
			user_id
		FROM
			prospects
		WHERE
			hospital = ? AND user_id IS NOT NULL
		`,
		func(rows *sql.Rows) {
			for rows.Next() {
				rows.Scan(&usrId)
			}
		},
		prospect.Hospital,
	)

	if err != nil {
		utility.SendError(err, w, req)
		return
	}

	if usrId == 0 {
		query, err := database.DBCon.Exec(
			`
			INSERT INTO
				users (username, type, slug)
			VALUES (?, ?, ?)
			`,
			strings.Replace(prospect.Hospital.String, " ", "", -1),
			"customer",
			strings.Replace(prospect.Hospital.String, " ", "", -1),
		)

		if err != nil {
			utility.SendError(err, w, req)
			return
		}

		usrId, err = query.LastInsertId()
		if err != nil {
			utility.SendError(err, w, req)
			return
		}
	}

	err = database.PerformQuery(
		`
		UPDATE
			prospects
		SET
			user_id = ?
		WHERE
			id = ?
		`,
		func(rows *sql.Rows) {},
		usrId,
		Id,
	)

	if err != nil {
		utility.SendError(err, w, req)
		return
	}

	utility.CustomData{
		Data: map[string]interface{}{
			"accountId": usrId,
		},
	}.OK(w, req)
}
