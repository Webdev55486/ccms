package addresses

import (
	"database/sql"
	"github.com/MedFilm/ccms/lib/database"
)

type BaseAddress map[string]Address

type Address struct {
	Id            int64   `json:"id"`
	Type          string  `json:"type"`
	ProspectId    *int64  `json:"prospectId"`
	CompanyFirst  *string `json:"companyFirst"`
	CompanySecond *string `json:"companySecond"`
	AddressFirst  *string `json:"addressFirst"`
	AddressSecond *string `json:"addressSecond"`
	City          *string `json:"city"`
	PostalCode    *string `json:"postalCode"`
	State         *string `json:"state"`
	Country       *string `json:"country"`
	Updated       string  `json:"updated"`
	Created       string  `json:"created"`
}

func GetAddressesByProspect(id int64) (BaseAddress, error) {
	result := BaseAddress{
		"visiting": Address{},
		"billing":  Address{},
	}

	err := database.PerformQuery(
		`
		SELECT
			id,
			prospect_id,
			type,
			company_first,
			company_second,
			address_first,
			address_second,
			city,
			postal_code,
			state,
			country,
			updated,
			created
		FROM
			addresses
		WHERE
			prospect_id=? AND type != ''
		`,
		func(rows *sql.Rows) {
			for rows.Next() {
				var digestAddress Address
				err := rows.Scan(
					&digestAddress.Id,
					&digestAddress.ProspectId,
					&digestAddress.Type,
					&digestAddress.CompanyFirst,
					&digestAddress.CompanySecond,
					&digestAddress.AddressFirst,
					&digestAddress.AddressSecond,
					&digestAddress.City,
					&digestAddress.PostalCode,
					&digestAddress.State,
					&digestAddress.Country,
					&digestAddress.Updated,
					&digestAddress.Created,
				)
				if err != nil {
					continue
				}

				result[digestAddress.Type] = digestAddress
			}
		},
		id,
	)
	if err != nil {
		return result, err
	}

	return result, nil
}

func InsertAddress(address Address) error {

	if address.Id == 0 {
		err := database.PerformQuery(
			"INSERT INTO addresses (prospect_id, type, company_first, company_second, address_first, address_second, city, postal_code, state, country) VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?, ?)",
			func(rows *sql.Rows) {},
			address.ProspectId,
			address.Type,
			address.CompanyFirst,
			address.CompanySecond,
			address.AddressFirst,
			address.AddressSecond,
			address.City,
			address.PostalCode,
			address.State,
			address.Country,
		)
		if err != nil {
			return err
		}

		return nil
	}

	err := database.PerformQuery(
		"UPDATE addresses SET prospect_id=?, type=?, company_first=?, company_second=?, address_first=?, address_second=?, city=?, postal_code=?, state=?, country=? WHERE id=?",
		func(rows *sql.Rows) {},
		address.ProspectId,
		address.Type,
		address.CompanyFirst,
		address.CompanySecond,
		address.AddressFirst,
		address.AddressSecond,
		address.City,
		address.PostalCode,
		address.State,
		address.Country,
		address.Id,
	)

	if err != nil {
		return err
	}

	return nil
}
