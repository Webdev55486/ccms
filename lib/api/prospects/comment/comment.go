/**
 * medfilm-ccms
 * Copyright (C) Sunstate AB - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Simon Wikstrand <simon@sunstate.se>, 2016-12-28
 */

package comment

import (
	"database/sql"
	"errors"
	"net/http"

	"github.com/MedFilm/ccms/lib/api/utility"
	"github.com/MedFilm/ccms/lib/authentication"
	"github.com/MedFilm/ccms/lib/database"
	"github.com/MedFilm/ccms/lib/tools"
)

type Comment struct {
	Id          int64  `json:"id"`
	ProspectId  *int64 `json:"prospectId"`
	EmployeeId  int64  `json:"employeeId"`
	Author      string `json:"author"`
	Description string `json:"description"`
	Updated     string `json:"updated"`
	Created     string `json:"created"`
}

func GetCommentByProspectId(id int64) ([]Comment, error) {
	var result []Comment

	err := database.PerformQuery(
		`
		SELECT
			com.id,
			com.prospect_id,
			emp.id,
			emp.given_name,
			com.description,
			com.updated,
			com.created
		FROM
			comments com
		INNER JOIN
			employees emp ON com.employee_id = emp.id
		WHERE
			com.prospect_id=?
		`,
		func(rows *sql.Rows) {
			for rows.Next() {
				var digestComment Comment
				err := rows.Scan(
					&digestComment.Id,
					&digestComment.ProspectId,
					&digestComment.EmployeeId,
					&digestComment.Author,
					&digestComment.Description,
					&digestComment.Updated,
					&digestComment.Created,
				)
				if err != nil {
					continue
				}
				result = append(result, digestComment)
			}
		},
		id,
	)
	if err != nil {
		return result, err
	}

	return result, nil
}

func InsertComment(w http.ResponseWriter, req *http.Request) {

	// Get permission of requesting user
	permission := authentication.GetPermissions(req, "prospects")
	if permission < 2 {
		utility.CustomData{}.Unauthorized(w, req)
		return
	}

	var comment Comment

	// Get user from request
	err := tools.DecodeJson(&comment, req)
	if err != nil {
		utility.SendError(err, w, req)
		return
	}

	if comment.Description == "" {
		utility.SendError(errors.New("You cannot add an empty comment"), w, req)
		return
	}

	err = database.PerformQuery(
		"INSERT INTO comments (prospect_id, employee_id, description) VALUES(?, ?, ?)",
		func(rows *sql.Rows) {},
		comment.ProspectId,
		comment.EmployeeId,
		comment.Description,
	)
	if err != nil {
		utility.SendError(err, w, req)
		return
	}

	// Return result
	utility.CustomData{}.OK(w, req)
}

// RemoveComment removes a prospect comments with provided id
func RemoveComment(w http.ResponseWriter, r *http.Request) {
	if permission := authentication.GetPermissions(r, "prospects"); permission < 2 {
		utility.CustomData{}.Unauthorized(w, r)
		return
	}

	id, err := tools.GetIdFromUrl(r)
	if err != nil {
		utility.SendError(err, w, r)
		return
	}

	err = database.PerformQuery(`
		delete from comments where id=? 	
	`, func(rows *sql.Rows) {}, id)

	if err != nil {
		utility.SendError(err, w, r)
		return
	}

	utility.CustomData{}.OK(w, r)
}

// UpdateComment updates the description of a prospect comment
func UpdateComment(w http.ResponseWriter, r *http.Request) {
	if permission := authentication.GetPermissions(r, "prospects"); permission < 2 {
		utility.CustomData{}.Unauthorized(w, r)
		return
	}

	var comment Comment

	if err := tools.DecodeJson(&comment, r); err != nil {
		utility.SendError(err, w, r)
		return
	}

	err := database.PerformQuery(`
		update comments set description=? where id=?
	`, func(rows *sql.Rows) {}, comment.Description, comment.Id)

	if err != nil {
		utility.SendError(err, w, r)
		return
	}

	utility.CustomData{}.OK(w, r)

}
