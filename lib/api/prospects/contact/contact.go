/**
 * ccms
 * Copyright (C) Sunstate AB - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Simon Wikstrand <simon@sunstate.se>, 2017-01-04
 */

package contact

import (
	"database/sql"
	"fmt"
	"github.com/MedFilm/ccms/lib/api/utility"
	"github.com/MedFilm/ccms/lib/authentication"
	"github.com/MedFilm/ccms/lib/database"
	"github.com/MedFilm/ccms/lib/tools"
	"net/http"
)

type Contact struct {
	Id          int64   `json:"id"`
	ProspectId  *int64  `json:"prospectId"`
	Name        *string `json:"name"`
	Position    *string `json:"position"`
	Email       *string `json:"email"`
	PhoneFirst  *string `json:"phoneFirst"`
	PhoneSecond *string `json:"phoneSecond"`
	Updated     string  `json:"updated"`
	Created     string  `json:"created"`
}

func GetCommentsByProspect(id int64) ([]Contact, error) {
	var result []Contact

	err := database.PerformQuery(
		`
		SELECT
			id,
			prospect_id,
			name,
			position,
			email,
			phone_first,
			phone_second,
			updated,
			created
		FROM
			contacts
		WHERE
			prospect_id=?
		`,
		func(rows *sql.Rows) {
			for rows.Next() {
				var digestContact Contact
				err := rows.Scan(
					&digestContact.Id,
					&digestContact.ProspectId,
					&digestContact.Name,
					&digestContact.Position,
					&digestContact.Email,
					&digestContact.PhoneFirst,
					&digestContact.PhoneSecond,
					&digestContact.Updated,
					&digestContact.Created,
				)
				if err != nil {
					fmt.Println(err.Error())
					continue
				}
				result = append(result, digestContact)
			}
		},
		id,
	)

	if err != nil {
		return result, err
	}

	return result, nil
}

func PrepareContact(w http.ResponseWriter, req *http.Request) {
	// Get permission of requesting user
	permission := authentication.GetPermissions(req, "prospects")
	if permission < 2 {
		utility.CustomData{}.Unauthorized(w, req)
		return
	}

	id, err := tools.GetIdFromUrl(req)
	if err != nil {
		utility.SendError(err, w, req)
		return
	}

	result, err := database.DBCon.Exec(
		"INSERT INTO contacts (prospect_id) VALUES(?)",
		id,
	)

	if err != nil {
		utility.SendError(err, w, req)
		return
	}

	id, err = result.LastInsertId()
	if err != nil {
		utility.SendError(err, w, req)
		return
	}

	utility.CustomData{
		Data: map[string]interface{}{
			"contactId": id,
		},
	}.OK(w, req)
}

func InsertContact(contact Contact) error {

	if contact.Name != nil && *contact.Name == "" {
		contact.Name = nil
	}

	if contact.Position != nil && *contact.Position == "" {
		contact.Position = nil
	}

	if contact.Email != nil && *contact.Email == "" {
		contact.Email = nil
	}

	if contact.PhoneFirst != nil && *contact.PhoneFirst == "" {
		contact.PhoneFirst = nil
	}

	if contact.PhoneSecond != nil && *contact.PhoneSecond == "" {
		contact.PhoneSecond = nil
	}

	if contact.Name == nil && contact.Position == nil && contact.Email == nil && contact.PhoneSecond == nil {
		if contact.Id != 0 {
			err := database.PerformQuery(
				"DELETE FROM contacts WHERE id=?",
				func(rows *sql.Rows) {},
				contact.Id,
			)
			if err != nil {
				return err
			}
		}

		return nil
	}

	// If no id is set, create new contact
	if contact.Id == 0 {
		_, err := database.DBCon.Exec(
			"INSERT INTO contacts (prospect_id, name, position, email, phone_first, phone_second) VALUES(?, ?, ?, ?, ?, ?)",
			contact.ProspectId,
			contact.Name,
			contact.Position,
			contact.Email,
			contact.PhoneFirst,
			contact.PhoneSecond,
		)

		if err != nil {
			return err
		}

		return nil
	}

	err := database.PerformQuery(
		"UPDATE contacts SET prospect_id=?, name=?, position=?, email=?, phone_first=?, phone_second=? WHERE id=?",
		func(rows *sql.Rows) {},
		contact.ProspectId,
		contact.Name,
		contact.Position,
		contact.Email,
		contact.PhoneFirst,
		contact.PhoneSecond,
		contact.Id,
	)

	if err != nil {
		return err
	}

	return nil
}
