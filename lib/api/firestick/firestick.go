package firestick

import (
	"database/sql"
	"errors"
	"crypto/md5"
	"encoding/hex"
	"io/ioutil"
	"net/http"
	"strconv"
	"time"
	"fmt"


	"github.com/MedFilm/ccms/lib/api/utility"
	"github.com/MedFilm/ccms/lib/authentication"
	"github.com/MedFilm/ccms/lib/database"
	"github.com/MedFilm/ccms/lib/tools"
	"github.com/gorilla/mux"
)
const filePath string = "E:/workspace/go_project/src/github.com/MedFilm/ccms/uploads/"
// const filePath string = "/files"

type Video struct {
	ID     int64  `json:"id"`
	FireId string `json:"stick_id"`
	FilmId int64  `json:"film_id"`
}

type Film struct {
	ID       int64  `json:"id"`
	FireId   string `json:"stick_id"`
	FilmName string `json:"film_name"`
}

type FilmFile struct {
	ID       int64  `json:"id"`
	FilmId   int64 `json:"film_id"`
	FileName string `json:"filename"`
}

func GetVideo(w http.ResponseWriter, req *http.Request) {

	// Get permission of requesting user
	permission := authentication.GetPermissions(req, "admin")

	if permission == 0 {
		utility.CustomData{}.Unauthorized(w, req)
		return
	}

	// Fill user struct
	var films []Film

	err := database.PerformQuery("SELECT firesticks.id, firesticks.stick_id, film.name as film_name FROM firesticks INNER JOIN film ON firesticks.film_id=film.id", func(rows *sql.Rows) {
		for rows.Next() {
			var singleVideo Film

			rows.Scan(&singleVideo.ID, &singleVideo.FireId, &singleVideo.FilmName)

			films = append(films, singleVideo)
		}
	})

	if err != nil {
		utility.CustomData{
			Data: map[string]interface{}{
				"errorMessage": err.Error(),
			},
		}.NotFound(w, req)
		return
	}

	utility.CustomData{Data: map[string]interface{}{
		"result": films,
	}}.OK(w, req)
}

func SaveVideoFile(w http.ResponseWriter, req *http.Request) {
	// Get permission of requesting user
	// vars := mux.Vars(req)
	// file, handle, err := strconv.ParseInt(vars["id"], 10, 64)
	file, handle, err := req.FormFile("stick_video")
	if err != nil {
		utility.CustomData{
			Data: map[string]interface{}{
				"errorMessage": err.Error(),
			},
		}.NotFound(w, req)
		return
	}
	defer file.Close()
	data, err := ioutil.ReadAll(file)
	if err != nil {
		utility.CustomData{
			Data: map[string]interface{}{
				"errorMessage": err.Error(),
			},
		}.NotFound(w, req)
		return
	}

	err = ioutil.WriteFile("./"+handle.Filename, data, 0666)
	if err != nil {
		utility.CustomData{
			Data: map[string]interface{}{
				"errorMessage": err.Error(),
			},
		}.NotFound(w, req)
		return
	}

	utility.CustomData{Data: map[string]interface{}{
		"result": file,
	}}.OK(w, req)
}

func GetSingleVideo(w http.ResponseWriter, req *http.Request) {

	// Get permission of requesting user
	permission := authentication.GetPermissions(req, "admin")

	if permission == 0 {
		utility.CustomData{}.Unauthorized(w, req)
		return
	}

	// Get variables from URL
	vars := mux.Vars(req)

	// Convert ID to int64
	id, err := strconv.ParseInt(vars["id"], 10, 64)
	if err != nil {
		utility.CustomData{
			Data: map[string]interface{}{
				"errorMessage": err.Error(),
			},
		}.NotFound(w, req)
		return
	}

	// If it's zero, error
	if id == 0 {
		utility.CustomData{
			Data: map[string]interface{}{
				"errorMessage": "Invalid ID",
			},
		}.PreconditionFailed(w, req)
		return
	}

	var video Video

	video.ID = id

	err = database.DBCon.QueryRow(
		"SELECT stick_id, film_id FROM firesticks WHERE id=?",
		id,
	).Scan(&video.FireId, &video.FilmId)

	if err != nil {
		utility.CustomData{
			Data: map[string]interface{}{
				"errorMessage": err.Error(),
			},
		}.NotFound(w, req)
		return
	}

	if err != nil {
		utility.CustomData{
			Data: map[string]interface{}{
				"errorMessage": err.Error(),
			},
		}.InternalServerError(w, req)
		return
	}

	// Return result
	utility.CustomData{Data: map[string]interface{}{
		"result": video,
	}}.OK(w, req)
}

func SaveVideo(w http.ResponseWriter, req *http.Request) {

	// Get permission of requesting user
	permission := authentication.GetPermissions(req, "admin")

	// Require edit level
	if permission != 2 {
		utility.CustomData{}.Unauthorized(w, req)
		return
	}

	var video Video
	getVideoId(&video, req)

	// Get user from request
	err := tools.DecodeJson(&video, req)

	if video.ID == 0 {
		err = insertVideo(video)
		if err != nil {
			utility.CustomData{
				Data: map[string]interface{}{
					"errorMessage": err.Error(),
				},
			}.InternalServerError(w, req)
			return
		}
	} else {
		err = updateVideo(video)
		if err != nil {
			utility.CustomData{
				Data: map[string]interface{}{
					"errorMessage": err.Error(),
				},
			}.InternalServerError(w, req)
			return
		}
	}

	// Return result
	utility.CustomData{Data: map[string]interface{}{
		"stick_id": video.FireId,
	}}.OK(w, req)
}

func DeleteVideo(w http.ResponseWriter, req *http.Request) {

	// Get permission of requesting user
	permission := authentication.GetPermissions(req, "admin")

	// Require edit level
	if permission != 2 {
		utility.CustomData{}.Unauthorized(w, req)
		return
	}

	var video Video

	getVideoId(&video, req)

	// Get user from request
	err := tools.DecodeJson(&video, req)
	if err != nil {
		utility.CustomData{
			Data: map[string]interface{}{
				"errorMessage": err.Error(),
			},
		}.InternalServerError(w, req)
		return
	}

	err = deleteUser(video)
	if err != nil {
		utility.CustomData{
			Data: map[string]interface{}{
				"errorMessage": err.Error(),
			},
		}.InternalServerError(w, req)
		return
	}

	// Return result
	utility.CustomData{}.OK(w, req)
}

func deleteUser(video Video) error {
	err := database.PerformQuery(
		"DELETE FROM firesticks WHERE id=?",
		func(rows *sql.Rows) {},
		video.ID,
	)
	if err != nil {
		return err
	}
	return nil
}

func insertVideo(video Video) error {
	var err error

	if len(video.FireId) < 1 {
		return errors.New("Please insert correct data")
	}

	stmt, err := database.DBCon.Prepare("INSERT INTO firesticks (stick_id, film_id) VALUES(?, ?)")
	if err != nil {
		return err
	}

	res, err := stmt.Exec(video.FireId, video.FilmId)
	if err != nil {
		return err
	}

	video.ID, err = res.LastInsertId()
	if err != nil {
		return err
	}

	return nil
}

func updateVideo(video Video) error {

	var err error

	if len(video.FireId) < 1 {
		return errors.New("Please insert correct data")
	}

	err = database.PerformQuery(
		"UPDATE firesticks SET stick_id=?,film_id=? WHERE id=?",
		func(rows *sql.Rows) {},
		video.FireId,
		video.FilmId,
		video.ID,
	)
	if err != nil {
		return err
	}

	return nil
}

func getVideoId(video *Video, req *http.Request) {
	// Check for id in params
	vars := mux.Vars(req)
	if val, ok := vars["id"]; ok {
		video.ID, _ = strconv.ParseInt(val, 10, 64)
	}
}

func getVarFromRequest(req *http.Request, key string) (string, error) {
	vars := mux.Vars(req)
	if val, ok := vars[key]; ok {
		return val, nil
	}
	return "", errors.New("Could not find value with specified key")
}

func GetSingleFilmApp(w http.ResponseWriter, req *http.Request)  {
	stickId, err := getVarFromRequest(req, "stick_id")
	clientToken, err := getVarFromRequest(req, "token")
	if err != nil {
		utility.SendError(err, w, req)
		return
	}
	loc, _ := time.LoadLocation("Europe/Stockholm")
    now := time.Now().In(loc).Format("01-02-2006")
	hasher := md5.New()
    hasher.Write([]byte(now))
    backendToken := hex.EncodeToString(hasher.Sum(nil))

	if clientToken != backendToken {
		utility.CustomData{
			Data: map[string]interface{}{
				"error": "authentication",
			},
		}.InternalServerError(w, req)
		return
	}

	var video Video

	video.FireId = stickId

	err = database.DBCon.QueryRow(
		"SELECT id, film_id FROM firesticks WHERE stick_id=?",
		stickId,
	).Scan(&video.ID, &video.FilmId)

	if err != nil {
		utility.SendError(err, w, req)
		return
	}

	var file FilmFile

	file.FilmId = video.FilmId

	err = database.DBCon.QueryRow(
		"SELECT id, filename FROM film_files WHERE film_id=? AND name=? ORDER BY language LIMIT 1",
		video.FilmId,
		"highResolution",
	).Scan(&file.ID, &file.FileName)

	path := fmt.Sprintf("%s%s", filePath, file.FileName)
	finalVideoFile, err := ioutil.ReadFile(path)
	w.Header().Set("Content-Type", "video/mp4")
	w.Write(finalVideoFile)
}
