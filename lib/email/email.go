/**
 * ccms
 * Copyright (C) Sunstate AB 2017 - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Simon Wikstrand <simon@sunstate.se>, 2017-03-23
 */
package email

import (
	"bytes"
	"fmt"
	"github.com/MedFilm/ccms/lib/config"
	"github.com/keighl/postmark"
	"html/template"
	"os"
)

var client *postmark.Client

func InitPostmark() {
	client = postmark.NewClient(config.BuiltConfig.Email.ServerToken, config.BuiltConfig.Email.AccountToken)
}

func parseTemplate(fileName string, data interface{}) (string, error) {
	dir, err := os.Getwd()
	if err != nil {
		fmt.Println(err.Error())
		return "", err
	}

	t, err := template.ParseFiles(dir + "/email/" + fileName)
	if err != nil {
		fmt.Println(err.Error())
		return "", err
	}

	buf := new(bytes.Buffer)
	if err = t.Execute(buf, data); err != nil {
		fmt.Println(err.Error())
		return "", err
	}

	return buf.String(), nil
}
