package email

import (
	"database/sql"
	"fmt"
	"strconv"
	"time"

	"github.com/MedFilm/ccms/lib/config"
	"github.com/MedFilm/ccms/lib/database"
	"github.com/keighl/postmark"
	"github.com/roylee0704/gron"
	"github.com/roylee0704/gron/xtime"
)

type reminder struct {
	Hospital *string
	Clinic   *string
	Time     *string
	Date     *string
	Text     *string
	Updated  *string
	Created  *string
	Email    *string
}

func InitCron() {
	fetchReminders()
	c := gron.New()
	c.AddFunc(gron.Every(1*xtime.Minute), fetchReminders)
	c.Start()
}

func fetchReminders() {

	fmt.Println("Initiating Cron Job...")

	err := database.PerformQuery(
		`
		SELECT
		  pro.hospital,
		  pro.clinic,
		  pro.reminder,
		  pro.reminder_time,
		  pro.reminder_text,
		  pro.updated,
		  pro.created,
		  emp.email
		FROM
		  prospects pro
		  INNER JOIN
		  employees emp ON pro.accountable = emp.id
		WHERE
		  pro.active = 1 AND DATE(pro.reminder) = CURDATE()
		`,
		func(rows *sql.Rows) {

			loc, _ := time.LoadLocation("Europe/Stockholm")

			aheadTime := time.Now().In(loc).Add(30 * time.Minute)
			var currentTime string
			if len(strconv.Itoa(aheadTime.Minute())) > 1 {
				currentTime = strconv.Itoa(aheadTime.Hour()) + ":" + strconv.Itoa(aheadTime.Minute())
			} else {
				currentTime = strconv.Itoa(aheadTime.Hour()) + ":" + strconv.Itoa(aheadTime.Minute()) + "0"
			}

			// Fetch and assign all countries
			for rows.Next() {
				reminder := reminder{}

				rows.Scan(
					&reminder.Hospital,
					&reminder.Clinic,
					&reminder.Date,
					&reminder.Time,
					&reminder.Text,
					&reminder.Updated,
					&reminder.Created,
					&reminder.Email,
				)

				if reminder.Time == nil {
					fmt.Println("PANIC AVIOIDED, NIL POINTER REFERENCE")
					continue
				}

				fmt.Println(reminder.Time, currentTime)
				if *reminder.Time == "" && currentTime == "05:00" {
					sendReminder(reminder)
					continue
				}

				if *reminder.Time == currentTime {
					sendReminder(reminder)
				}
			}
		},
	)

	if err != nil {
		fmt.Printf("Database Fetch Error: %s", err.Error())
		return
	}
}

func sendReminder(reminder reminder) {
	temp, err := parseTemplate("reminder.html", reminder)
	if err != nil {
		fmt.Println(err.Error())
		return
	}
	email := postmark.Email{
		From:       config.BuiltConfig.Email.From,
		To:         *reminder.Email,
		Subject:    "Reminder",
		HtmlBody:   temp,
		TrackOpens: true,
	}

	_, err = client.SendEmail(email)
	if err != nil {
		fmt.Println(err.Error())
	}
}
