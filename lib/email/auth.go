package email

import (
	"fmt"
	"github.com/MedFilm/ccms/lib/config"
	"github.com/keighl/postmark"
)

func SendResetPassword(to string, hash string) {
	temp, err := parseTemplate("passwordReset.html", struct {
		Url  string
		Hash string
	}{
		Url:  config.BuiltConfig.Application.Url,
		Hash: hash,
	})

	if err != nil {
		fmt.Println(err.Error())
		return
	}

	email := postmark.Email{
		From:       config.BuiltConfig.Email.From,
		To:         to,
		Subject:    "Reminder",
		HtmlBody:   temp,
		TrackOpens: true,
	}

	_, err = client.SendEmail(email)
	if err != nil {
		fmt.Println(err.Error())
	}
}
