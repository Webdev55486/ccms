package routes

import (
	"net/http"

	"github.com/MedFilm/ccms/lib/api/accounts"
	"github.com/MedFilm/ccms/lib/api/accounts/devices"
	"github.com/MedFilm/ccms/lib/api/accounts/subscriptions"
	"github.com/MedFilm/ccms/lib/api/admin"
	"github.com/MedFilm/ccms/lib/api/auth"
	"github.com/MedFilm/ccms/lib/api/employees"
	"github.com/MedFilm/ccms/lib/api/employees/meeting"
	"github.com/MedFilm/ccms/lib/api/files"
	"github.com/MedFilm/ccms/lib/api/firestick"
	"github.com/MedFilm/ccms/lib/api/libraries"
	"github.com/MedFilm/ccms/lib/api/prospects"
	"github.com/MedFilm/ccms/lib/api/prospects/comment"
	"github.com/MedFilm/ccms/lib/api/prospects/contact"
	"github.com/MedFilm/ccms/lib/api/quotes"
	"github.com/MedFilm/ccms/lib/api/utility"
	"github.com/MedFilm/ccms/lib/api/website"
	"github.com/MedFilm/ccms/lib/authentication"
	"github.com/gorilla/mux"
	"github.com/urfave/negroni"
)

func ApiRoutes(r *mux.Router) {

	// Create a subrouter of the /api prefix
	s := r.PathPrefix("/api").Subrouter()

	// Hook on /auth
	authRoute(s)
	accountsRoute(s)
	adminRoute(s)
	firestickRoute(s)
	prospectRoute(s)
	websiteRoute(s)
	employeesRoute(s)
	libraryRoute(s)
	file(s)
	device(s)
	todoRoute(s)
	salesCommRoute(s)
	drivinlogRoute(s)
	meetingsRoute(s)
	pdf(s)

	// Catch all unmatched requests under /api
	r.HandleFunc("/api/{rest:.*}", utility.CustomData{}.NotFound)
}

func authRoute(r *mux.Router) {
	s := r.PathPrefix("/auth").Subrouter()

	// New login request, sign a token
	s.HandleFunc("/login", auth.Login).Methods("POST")
	s.HandleFunc("/password", auth.ResetPassword).Methods("POST")
	s.HandleFunc("/password/reset", auth.SetPassword).Methods("POST")

	// Validate a token
	s.Handle("/validate", negroni.New(
		negroni.HandlerFunc(authentication.ValidateToken),
		negroni.Wrap(http.HandlerFunc(auth.ValidateToken)),
	)).Methods("GET")

	// Validate a token
	s.Handle("/validate/password", negroni.New(
		negroni.HandlerFunc(authentication.ValidateToken),
		negroni.Wrap(http.HandlerFunc(auth.ValidatePassword)),
	)).Methods("POST")

	// Reset password
	s.HandleFunc("/password", auth.ResetPassword).Methods("POST")
}

func accountsRoute(r *mux.Router) {

	// Get all accounts
	r.Handle("/accounts", negroni.New(
		negroni.HandlerFunc(authentication.ValidateToken),
		negroni.Wrap(http.HandlerFunc(accounts.GetAccounts)),
	)).Methods("GET")

	// Get specific account
	r.Handle("/accounts/{id}", negroni.New(
		negroni.HandlerFunc(authentication.ValidateToken),
		negroni.Wrap(http.HandlerFunc(accounts.GetSingleAccount)),
	)).Methods("GET")

	// Update account
	r.Handle("/accounts/{id}", negroni.New(
		negroni.HandlerFunc(authentication.ValidateToken),
		negroni.Wrap(http.HandlerFunc(accounts.UpdateAccount)),
	)).Methods("PUT")

	// Delete account
	r.Handle("/accounts/{id}", negroni.New(
		negroni.HandlerFunc(authentication.ValidateToken),
		negroni.Wrap(http.HandlerFunc(accounts.DeleteAccount)),
	)).Methods("DELETE")

	// Insert account
	/*
		r.Handle("/accounts", negroni.New(
			negroni.HandlerFunc(authentication.ValidateToken),
			negroni.Wrap(http.HandlerFunc(accounts.InsertAccount)),
		)).Methods("POST")
	*/

	s := r.PathPrefix("/accounts").Subrouter()

	s.Handle("/subscriptions/{id}", negroni.New(
		negroni.HandlerFunc(authentication.ValidateToken),
		negroni.Wrap(http.HandlerFunc(subscriptions.GetSubscriptions)),
	)).Methods("GET")

	s.Handle("/subscriptions/{id}/binding", negroni.New(
		negroni.HandlerFunc(authentication.ValidateToken),
		negroni.Wrap(http.HandlerFunc(subscriptions.CreateNewLanguageBinding)),
	)).Methods("POST")

	s.Handle("/subscriptions/{subscriptionId}/binding/{languageId}", negroni.New(
		negroni.HandlerFunc(authentication.ValidateToken),
		negroni.Wrap(http.HandlerFunc(subscriptions.DeleteLanguageBinding)),
	)).Methods("DELETE")

	s.Handle("/subscription/{id}", negroni.New(
		negroni.HandlerFunc(authentication.ValidateToken),
		negroni.Wrap(http.HandlerFunc(subscriptions.UpdateSubscription)),
	)).Methods("PUT")

	s.Handle("/subscription/{id}", negroni.New(
		negroni.HandlerFunc(authentication.ValidateToken),
		negroni.Wrap(http.HandlerFunc(subscriptions.DeleteSubscription)),
	)).Methods("DELETE")

	s.Handle("/subscription", negroni.New(
		negroni.HandlerFunc(authentication.ValidateToken),
		negroni.Wrap(http.HandlerFunc(subscriptions.InsertSubscription)),
	)).Methods("POST")

	// Find override permits
	s.Handle("/subscription/permits/{override}", negroni.New(
		negroni.HandlerFunc(authentication.ValidateToken),
		negroni.Wrap(http.HandlerFunc(accounts.GetPermits)),
	)).Methods("GET")
}

func adminRoute(r *mux.Router) {
	s := r.PathPrefix("/admin").Subrouter()

	// Get all admin accounts
	s.Handle("/accounts", negroni.New(
		negroni.HandlerFunc(authentication.ValidateToken),
		negroni.Wrap(http.HandlerFunc(admin.GetUser)),
	)).Methods("GET")

	// Save admin account
	s.Handle("/accounts", negroni.New(
		negroni.HandlerFunc(authentication.ValidateToken),
		negroni.Wrap(http.HandlerFunc(admin.SaveUser)),
	)).Methods("POST")

	// Get specific account
	s.Handle("/accounts/{id}", negroni.New(
		negroni.HandlerFunc(authentication.ValidateToken),
		negroni.Wrap(http.HandlerFunc(admin.GetSingleUser)),
	)).Methods("GET")

	// Update specific account
	s.Handle("/accounts/{id}", negroni.New(
		negroni.HandlerFunc(authentication.ValidateToken),
		negroni.Wrap(http.HandlerFunc(admin.SaveUser)),
	)).Methods("PUT")

	// Update specific account
	s.Handle("/accounts/{id}", negroni.New(
		negroni.HandlerFunc(authentication.ValidateToken),
		negroni.Wrap(http.HandlerFunc(admin.DeleteUser)),
	)).Methods("DELETE")
}

func firestickRoute(r *mux.Router) {
	s := r.PathPrefix("/firestick").Subrouter()

	// Get all admin accounts
	s.Handle("/videos", negroni.New(
		negroni.HandlerFunc(authentication.ValidateToken),
		negroni.Wrap(http.HandlerFunc(firestick.GetVideo)),
	)).Methods("GET")

	// Save admin account
	s.Handle("/videos", negroni.New(
		negroni.HandlerFunc(authentication.ValidateToken),
		negroni.Wrap(http.HandlerFunc(firestick.SaveVideo)),
	)).Methods("POST")

	s.Handle("/videos/file/{id}", negroni.New(
		negroni.HandlerFunc(authentication.ValidateToken),
		negroni.Wrap(http.HandlerFunc(firestick.SaveVideoFile)),
	)).Methods("POST")

	// Get specific account
	s.Handle("/videos/{id}", negroni.New(
		negroni.HandlerFunc(authentication.ValidateToken),
		negroni.Wrap(http.HandlerFunc(firestick.GetSingleVideo)),
	)).Methods("GET")

	// Update specific account
	s.Handle("/videos/{id}", negroni.New(
		negroni.HandlerFunc(authentication.ValidateToken),
		negroni.Wrap(http.HandlerFunc(firestick.SaveVideo)),
	)).Methods("PUT")

	// Update specific account
	s.Handle("/videos/{id}", negroni.New(
		negroni.HandlerFunc(authentication.ValidateToken),
		negroni.Wrap(http.HandlerFunc(firestick.DeleteVideo)),
	)).Methods("DELETE")
	//get film from app
	s.Handle("/api/getfilm/{stick_id}/{token}", negroni.New(
		negroni.Wrap(http.HandlerFunc(firestick.GetSingleFilmApp)),
	)).Methods("GET")
}

func prospectRoute(r *mux.Router) {

	// Get all prospect data
	r.Handle("/prospects", negroni.New(
		negroni.HandlerFunc(authentication.ValidateToken),
		negroni.Wrap(http.HandlerFunc(prospects.GetProspects)),
	)).Methods("GET")

	// Insert prospect
	r.Handle("/prospects", negroni.New(
		negroni.HandlerFunc(authentication.ValidateToken),
		negroni.Wrap(http.HandlerFunc(prospects.InsertProspect)),
	)).Methods("POST")

	// Update prospect
	r.Handle("/prospects/{id}", negroni.New(
		negroni.HandlerFunc(authentication.ValidateToken),
		negroni.Wrap(http.HandlerFunc(prospects.UpdateProspects)),
	)).Methods("PUT")

	r.Handle("/prospects/{id}/convert", negroni.New(
		negroni.HandlerFunc(authentication.ValidateToken),
		negroni.Wrap(http.HandlerFunc(prospects.ConvertProspect)),
	)).Methods("PUT")

	// Delete prospect
	r.Handle("/prospects/{id}", negroni.New(
		negroni.HandlerFunc(authentication.ValidateToken),
		negroni.Wrap(http.HandlerFunc(prospects.DeleteProspect)),
	)).Methods("DELETE")

	s := r.PathPrefix("/prospects").Subrouter()

	s.Handle("/prepare/contact/{id}", negroni.New(
		negroni.HandlerFunc(authentication.ValidateToken),
		negroni.Wrap(http.HandlerFunc(contact.PrepareContact)),
	)).Methods("POST")

	// Get KAM(employees)
	s.Handle("/kam", negroni.New(
		negroni.HandlerFunc(authentication.ValidateToken),
		negroni.Wrap(http.HandlerFunc(prospects.GetKAM)),
	)).Methods("GET")

	// Get all available categories
	s.Handle("/categories", negroni.New(
		negroni.HandlerFunc(authentication.ValidateToken),
		negroni.Wrap(http.HandlerFunc(prospects.GetCategories)),
	)).Methods("GET")

	// Add country
	s.Handle("/country", negroni.New(
		negroni.HandlerFunc(authentication.ValidateToken),
		negroni.Wrap(http.HandlerFunc(prospects.InsertCountry)),
	)).Methods("POST")

	// Delete country
	s.Handle("/country/{id}", negroni.New(
		negroni.HandlerFunc(authentication.ValidateToken),
		negroni.Wrap(http.HandlerFunc(prospects.DeleteCountry)),
	)).Methods("DELETE")

	// Add region
	s.Handle("/region", negroni.New(
		negroni.HandlerFunc(authentication.ValidateToken),
		negroni.Wrap(http.HandlerFunc(prospects.InsertRegion)),
	)).Methods("POST")

	// Delete region
	s.Handle("/region/{id}", negroni.New(
		negroni.HandlerFunc(authentication.ValidateToken),
		negroni.Wrap(http.HandlerFunc(prospects.DeleteRegion)),
	)).Methods("DELETE")

	// Post comment
	s.Handle("/comments", negroni.New(
		negroni.HandlerFunc(authentication.ValidateToken),
		negroni.Wrap(http.HandlerFunc(comment.InsertComment)),
	)).Methods("POST")

	// Delete comment
	s.Handle("/comments/{id}", negroni.New(
		negroni.HandlerFunc(authentication.ValidateToken),
		negroni.Wrap(http.HandlerFunc(comment.RemoveComment)),
	)).Methods("DELETE")

	// Update comment
	s.Handle("/comments/update", negroni.New(
		negroni.HandlerFunc(authentication.ValidateToken),
		negroni.Wrap(http.HandlerFunc(comment.UpdateComment)),
	)).Methods("PUT")
}

func websiteRoute(r *mux.Router) {
	s := r.PathPrefix("/website").Subrouter()

	s.Handle("/article/imageset/{id}", negroni.New(
		negroni.HandlerFunc(authentication.ValidateToken),
		negroni.Wrap(http.HandlerFunc(website.GetImageSet)),
	)).Methods("GET")

	// Get News
	s.Handle("/article/{language}/{type}", negroni.New(
		negroni.HandlerFunc(authentication.ValidateToken),
		negroni.Wrap(http.HandlerFunc(website.GetArticles)),
	)).Methods("GET")

	s.Handle("/article/{id}", negroni.New(
		negroni.HandlerFunc(authentication.ValidateToken),
		negroni.Wrap(http.HandlerFunc(website.GetArticle)),
	)).Methods("GET")

	s.Handle("/article", negroni.New(
		negroni.HandlerFunc(authentication.ValidateToken),
		negroni.Wrap(http.HandlerFunc(website.SaveArticle)),
	)).Methods("POST")

	s.Handle("/article/imageset/{id}", negroni.New(
		negroni.HandlerFunc(authentication.ValidateToken),
		negroni.Wrap(http.HandlerFunc(website.SaveImageSet)),
	)).Methods("POST")

	s.Handle("/static/{language}/{id}", negroni.New(
		negroni.HandlerFunc(authentication.ValidateToken),
		negroni.Wrap(http.HandlerFunc(website.GetStaticContent)),
	)).Methods("GET")

	s.Handle("/static/{language}/{id}", negroni.New(
		negroni.HandlerFunc(authentication.ValidateToken),
		negroni.Wrap(http.HandlerFunc(website.SetStaticContent)),
	)).Methods("POST")

	s.Handle("/image", negroni.New(
		negroni.HandlerFunc(authentication.ValidateToken),
		negroni.Wrap(http.HandlerFunc(website.UploadImage)),
	)).Methods("POST")

	s.Handle("/image/{id}", negroni.New(
		negroni.Wrap(http.HandlerFunc(website.GetImage)),
	)).Methods("GET")

	s.Handle("/language", negroni.New(
		negroni.HandlerFunc(authentication.ValidateToken),
		negroni.Wrap(http.HandlerFunc(website.GetLanguageFile)),
	)).Methods("GET")

	s.Handle("/language", negroni.New(
		negroni.HandlerFunc(authentication.ValidateToken),
		negroni.Wrap(http.HandlerFunc(website.SaveLanguageFile)),
	)).Methods("PUT")

	s.Handle("/logotype", negroni.New(
		negroni.HandlerFunc(authentication.ValidateToken),
		negroni.Wrap(http.HandlerFunc(website.GetLogotypes)),
	)).Methods("GET")

	s.Handle("/logotype", negroni.New(
		negroni.HandlerFunc(authentication.ValidateToken),
		negroni.Wrap(http.HandlerFunc(website.SaveLogotypes)),
	)).Methods("PUT")
}

func employeesRoute(r *mux.Router) {
	s := r.PathPrefix("/employees").Subrouter()

	// Get Systemimprovements
	s.Handle("/systemimprovements", negroni.New(
		negroni.HandlerFunc(authentication.ValidateToken),
		negroni.Wrap(http.HandlerFunc(employees.GetSuggestions)),
	)).Methods("GET")

	// Insert Systemimprovement
	s.Handle("/systemimprovements", negroni.New(
		negroni.HandlerFunc(authentication.ValidateToken),
		negroni.Wrap(http.HandlerFunc(employees.InsertSuggestion)),
	)).Methods("POST")

	// Delete Systemimprovement
	s.Handle("/systemimprovements/{id}", negroni.New(
		negroni.HandlerFunc(authentication.ValidateToken),
		negroni.Wrap(http.HandlerFunc(employees.DeleteSuggestion)),
	)).Methods("DELETE")

	// Get timelogs for specified year, month and employee
	s.Handle("/timelog/{employeeId}/{year}/{month}", negroni.New(
		negroni.HandlerFunc(authentication.ValidateToken),
		negroni.Wrap(http.HandlerFunc(employees.GetTimelogs)),
	)).Methods("GET")

	// Update timelog
	s.Handle("/timelog/{id}", negroni.New(
		negroni.HandlerFunc(authentication.ValidateToken),
		negroni.Wrap(http.HandlerFunc(employees.UpdateTimelog)),
	)).Methods("PUT")

	// Get summary
	s.Handle("/timelog/summary/{year}/{month}/{employeeId}", negroni.New(
		negroni.HandlerFunc(authentication.ValidateToken),
		negroni.Wrap(http.HandlerFunc(employees.GetSummary)),
	)).Methods("GET")
}

func libraryRoute(r *mux.Router) {
	s := r.PathPrefix("/libraries").Subrouter()

	s.Handle("", negroni.New(
		negroni.HandlerFunc(authentication.ValidateToken),
		negroni.Wrap(http.HandlerFunc(libraries.GetLibraries)),
	)).Methods("GET")

	s.Handle("/library", negroni.New(
		negroni.HandlerFunc(authentication.ValidateToken),
		negroni.Wrap(http.HandlerFunc(libraries.NewLibrary)),
	)).Methods("POST")

	s.Handle("/library/{id}", negroni.New(
		negroni.HandlerFunc(authentication.ValidateToken),
		negroni.Wrap(http.HandlerFunc(libraries.GetLibrary)),
	)).Methods("GET")

	s.Handle("/library/{id}", negroni.New(
		negroni.HandlerFunc(authentication.ValidateToken),
		negroni.Wrap(http.HandlerFunc(libraries.SaveLibrary)),
	)).Methods("PUT")

	s.Handle("/library/{id}", negroni.New(
		negroni.HandlerFunc(authentication.ValidateToken),
		negroni.Wrap(http.HandlerFunc(libraries.RemoveLibrary)),
	)).Methods("DELETE")

	s.Handle("/library/{id}/language", negroni.New(
		negroni.HandlerFunc(authentication.ValidateToken),
		negroni.Wrap(http.HandlerFunc(libraries.NewLibraryLanguage)),
	)).Methods("POST")

	s.Handle("/library/{id}/language", negroni.New(
		negroni.HandlerFunc(authentication.ValidateToken),
		negroni.Wrap(http.HandlerFunc(libraries.RemoveFilmLanguage)),
	)).Methods("DELETE")

	s.Handle("/films", negroni.New(
		negroni.HandlerFunc(authentication.ValidateToken),
		negroni.Wrap(http.HandlerFunc(libraries.GetFilms)),
	)).Methods("GET")

	s.Handle("/film/{id}", negroni.New(
		negroni.HandlerFunc(authentication.ValidateToken),
		negroni.Wrap(http.HandlerFunc(libraries.GetFilm)),
	)).Methods("GET")

	s.Handle("/film", negroni.New(
		negroni.HandlerFunc(authentication.ValidateToken),
		negroni.Wrap(http.HandlerFunc(libraries.NewFilm)),
	)).Methods("POST")

	s.Handle("/film/{id}", negroni.New(
		negroni.HandlerFunc(authentication.ValidateToken),
		negroni.Wrap(http.HandlerFunc(libraries.SaveFilm)),
	)).Methods("PUT")

	s.Handle("/film/{id}", negroni.New(
		negroni.HandlerFunc(authentication.ValidateToken),
		negroni.Wrap(http.HandlerFunc(libraries.RemoveFilm)),
	)).Methods("DELETE")

	s.Handle("/film/{id}/language", negroni.New(
		negroni.HandlerFunc(authentication.ValidateToken),
		negroni.Wrap(http.HandlerFunc(libraries.NewFilmLanguage)),
	)).Methods("POST")
}

func file(r *mux.Router) {
	s := r.PathPrefix("/files").Subrouter()

	s.Handle("/{name}/film/{id}/language/{language}", negroni.New(
		negroni.HandlerFunc(authentication.ValidateToken),
		negroni.Wrap(http.HandlerFunc(files.PrepareFileForFilm)),
	)).Methods("POST")

	s.Handle("/{name}/subscription/{id}/language/{language}", negroni.New(
		negroni.HandlerFunc(authentication.ValidateToken),
		negroni.Wrap(http.HandlerFunc(files.PrepareFileForSubscriptionFilm)),
	)).Methods("POST")

	s.Handle("/{id}", negroni.New(
		negroni.HandlerFunc(authentication.ValidateToken),
		negroni.Wrap(http.HandlerFunc(files.UploadFile)),
	)).Methods("PUT")
}

func device(r *mux.Router) {
	s := r.PathPrefix("/devices").Subrouter()

	s.Handle("/tablet/apk", negroni.New(
		negroni.HandlerFunc(authentication.ParseHeadersFromRedirect),
		negroni.HandlerFunc(authentication.ValidateToken),
		negroni.Wrap(http.HandlerFunc(devices.DownloadAPK)),
	)).Methods("GET")

	s.Handle("/tablet/{id}", negroni.New(
		negroni.HandlerFunc(authentication.ValidateToken),
		negroni.Wrap(http.HandlerFunc(devices.GetTablets)),
	)).Methods("GET")

	s.Handle("/tablet", negroni.New(
		negroni.HandlerFunc(authentication.ValidateToken),
		negroni.Wrap(http.HandlerFunc(devices.AddTablet)),
	)).Methods("POST")

	s.Handle("/manifest/{id}", negroni.New(
		negroni.HandlerFunc(authentication.ValidateToken),
		negroni.Wrap(http.HandlerFunc(devices.GetManifest)),
	)).Methods("GET")

	s.Handle("/manifest/subscription/{id}", negroni.New(
		negroni.HandlerFunc(authentication.ValidateToken),
		negroni.Wrap(http.HandlerFunc(devices.GetManifestSubscriptions)),
	)).Methods("GET")

	s.Handle("/manifest/subscription/{id}", negroni.New(
		negroni.HandlerFunc(authentication.ValidateToken),
		negroni.Wrap(http.HandlerFunc(devices.DeleteManifestSubscription)),
	)).Methods("DELETE")

	s.Handle("/manifest/subscription", negroni.New(
		negroni.HandlerFunc(authentication.ValidateToken),
		negroni.Wrap(http.HandlerFunc(devices.AddManifestSubscription)),
	)).Methods("POST")

	s.Handle("/manifest/version/{id}", negroni.New(
		negroni.HandlerFunc(authentication.ValidateToken),
		negroni.Wrap(http.HandlerFunc(devices.UpdateManifest)),
	)).Methods("PUT")
}

func todoRoute(r *mux.Router) {
	s := r.PathPrefix("/todo").Subrouter()

	// GET
	s.Handle("/all", negroni.New(
		negroni.HandlerFunc(authentication.ValidateToken),
		negroni.Wrap(http.HandlerFunc(employees.GetTodos)),
	)).Methods("GET")

	// POST
	s.Handle("/item", negroni.New(
		negroni.HandlerFunc(authentication.ValidateToken),
		negroni.Wrap(http.HandlerFunc(employees.InsertTodo)),
	)).Methods("POST")

	s.Handle("/group", negroni.New(
		negroni.HandlerFunc(authentication.ValidateToken),
		negroni.Wrap(http.HandlerFunc(employees.InsertTodoGroup)),
	)).Methods("POST")

	s.Handle("/category", negroni.New(
		negroni.HandlerFunc(authentication.ValidateToken),
		negroni.Wrap(http.HandlerFunc(employees.InsertTodoCategory)),
	)).Methods("POST")

	// PUT
	s.Handle("/item/{id}", negroni.New(
		negroni.HandlerFunc(authentication.ValidateToken),
		negroni.Wrap(http.HandlerFunc(employees.UpdateTodo)),
	)).Methods("PUT")

	s.Handle("/group/{id}", negroni.New(
		negroni.HandlerFunc(authentication.ValidateToken),
		negroni.Wrap(http.HandlerFunc(employees.UpdateGroup)),
	)).Methods("PUT")

	// DELETE
	s.Handle("/item/{id}", negroni.New(
		negroni.HandlerFunc(authentication.ValidateToken),
		negroni.Wrap(http.HandlerFunc(employees.DeleteTodo)),
	)).Methods("DELETE")

	s.Handle("/group/{id}", negroni.New(
		negroni.HandlerFunc(authentication.ValidateToken),
		negroni.Wrap(http.HandlerFunc(employees.DeleteTodoGroup)),
	)).Methods("DELETE")

	s.Handle("/category/{id}", negroni.New(
		negroni.HandlerFunc(authentication.ValidateToken),
		negroni.Wrap(http.HandlerFunc(employees.DeleteTodoCategory)),
	)).Methods("DELETE")
}
func salesCommRoute(r *mux.Router) {
	r.Handle("/salescommission", negroni.New(
		negroni.HandlerFunc(authentication.ValidateToken),
		negroni.Wrap(http.HandlerFunc(employees.ReadSalesCommissions)),
	)).Methods("GET")

	r.Handle("/salescommission", negroni.New(
		negroni.HandlerFunc(authentication.ValidateToken),
		negroni.Wrap(http.HandlerFunc(employees.CreateSalesCommission)),
	)).Methods("POST")

	r.Handle("/salescommission/country", negroni.New(
		negroni.HandlerFunc(authentication.ValidateToken),
		negroni.Wrap(http.HandlerFunc(employees.CreateCountry)),
	)).Methods("POST")

	r.Handle("/salescommission", negroni.New(
		negroni.HandlerFunc(authentication.ValidateToken),
		negroni.Wrap(http.HandlerFunc(employees.UpdateSalesCommission)),
	)).Methods("PUT")
}
func pdf(r *mux.Router) {
	r.Handle("/htmltopdf", negroni.New(
		negroni.HandlerFunc(authentication.ValidateToken),
		negroni.Wrap(http.HandlerFunc(quotes.MakePDF)),
	)).Methods("POST")
}
func drivinlogRoute(r *mux.Router) {
	r.Handle("/drivinglog", negroni.New(
		negroni.HandlerFunc(authentication.ValidateToken),
		negroni.Wrap(http.HandlerFunc(employees.ReadDrivingLogs)),
	)).Methods("GET")

	r.Handle("/drivinglog/person", negroni.New(
		negroni.HandlerFunc(authentication.ValidateToken),
		negroni.Wrap(http.HandlerFunc(employees.CreateDrivinglogPerson)),
	)).Methods("POST")

	r.Handle("/drivinglog", negroni.New(
		negroni.HandlerFunc(authentication.ValidateToken),
		negroni.Wrap(http.HandlerFunc(employees.CreateDrivinglog)),
	)).Methods("POST")

	r.Handle("/drivinglog", negroni.New(
		negroni.HandlerFunc(authentication.ValidateToken),
		negroni.Wrap(http.HandlerFunc(employees.UpdateDrivinglog)),
	)).Methods("PUT")
}
func meetingsRoute(r *mux.Router) {
	r.Handle("/meeting", negroni.New(
		negroni.HandlerFunc(authentication.ValidateToken),
		negroni.Wrap(http.HandlerFunc(meeting.Get)),
	)).Methods("GET")

	r.Handle("/meeting", negroni.New(
		negroni.HandlerFunc(authentication.ValidateToken),
		negroni.Wrap(http.HandlerFunc(meeting.Post)),
	)).Methods("POST")

	r.Handle("/meeting", negroni.New(
		negroni.HandlerFunc(authentication.ValidateToken),
		negroni.Wrap(http.HandlerFunc(meeting.Put)),
	)).Methods("PUT")

	r.Handle("/meeting/{id}", negroni.New(
		negroni.HandlerFunc(authentication.ValidateToken),
		negroni.Wrap(http.HandlerFunc(meeting.Delete)),
	)).Methods("DELETE")
}
