/**
 * medfilm-ccms
 * Copyright (C) Sunstate AB - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Simon Wikstrand <simon@sunstate.se>, 2016-12-15
 */

package routes

import (
	"github.com/gorilla/mux"
	"github.com/unrolled/render"
	"net/http"
)

func Routes(r *mux.Router) {

	r.HandleFunc("/", FrontPage)
	r.PathPrefix("/resources/").Handler(http.FileServer(http.Dir("")))
	r.HandleFunc("/{rest:.*}", FrontPage)

}

func FrontPage(w http.ResponseWriter, req *http.Request) {

	r := render.New(render.Options{
		Directory: "templates",
		Delims: render.Delims{
			Left:  "{-",
			Right: "-}",
		},
	})

	r.HTML(w, http.StatusOK, "index", nil)
}
