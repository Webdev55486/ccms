/**
 * medfilm-ccms
 * Copyright (C) Sunstate AB - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Simon Wikstrand <simon@sunstate.se>, 2016-12-16
 */

package authentication

import (
	"crypto/rand"
	"crypto/rsa"
	"crypto/x509"
	"encoding/pem"
	"errors"
	"fmt"
	"io/ioutil"
	"net/http"
	"os"
	"time"

	"github.com/MedFilm/ccms/lib/api/utility"
	"github.com/MedFilm/ccms/lib/config"
	"github.com/dgrijalva/jwt-go"
	"github.com/dgrijalva/jwt-go/request"
)

type UserCredentials struct {
	Email    string `json:"email"`
	Password string `json:"password"`
}

type TokenResponse struct {
	Token string `json:"token"`
}

// Stored in memory for signing and verification process
var privateKey *rsa.PrivateKey
var publicBlock pem.Block

func generateKeys() error {
	reader := rand.Reader

	key, err := rsa.GenerateKey(reader, 4096)
	if err != nil {
		return err
	}

	err = savePrivatePEMKey(key)

	return err
}

func savePrivatePEMKey(key *rsa.PrivateKey) error {
	outFile, err := os.Create("keys/" + config.BuiltConfig.Security.PrivateKey)
	if err != nil {
		return err
	}

	defer outFile.Close()
	var blockKey = &pem.Block{
		Type:  "PRIVATE KEY",
		Bytes: x509.MarshalPKCS1PrivateKey(key),
	}

	err = pem.Encode(outFile, blockKey)

	return err
}

func InitKeys() error {

	// Check if keys exists, if not generate new ones.
	if _, err := os.Stat("keys/" + config.BuiltConfig.Security.PrivateKey); os.IsNotExist(err) {
		err = generateKeys()
		if err != nil {
			return err
		}
	}

	// Read key from file to []byte
	byteKey, err := ioutil.ReadFile("keys/" + config.BuiltConfig.Security.PrivateKey)
	if err != nil {
		return err
	}

	// Decode PEM format
	privateBlock, rest := pem.Decode(byteKey)
	if len(rest) > 0 {
		return errors.New("Extra data found in private key")
	}

	// Parse byte array to rsa class
	privateKey, err = x509.ParsePKCS1PrivateKey(privateBlock.Bytes)
	if err != nil {
		return err
	}

	pub := privateKey.PublicKey
	pub_der, err := x509.MarshalPKIXPublicKey(&pub)
	if err != nil {
		return err
	}

	publicBlock = pem.Block{
		Type:    "PUBLIC KEY",
		Headers: nil,
		Bytes:   pub_der,
	}

	return nil
}

func SignToken(userData UserCredentials) (string, error) {

	// Validate user credentials against database
	user, err := validateLoginCredentials(userData)
	if err != nil {
		return "", errors.New("Authentication Failed")
	}

	signer := jwt.New(jwt.SigningMethodRS512)
	claims := make(jwt.MapClaims)

	// Todo: Correct claims
	claims["iss"] = "MedFilm"
	claims["iat"] = time.Now().Unix()
	claims["exp"] = time.Now().Add(time.Second * config.BuiltConfig.Application.SessionTimeout).Unix()
	claims["cui"] = user

	signer.Claims = claims

	return signer.SignedString(privateKey)
}

func ValidatePassword(userData UserCredentials) error {
	_, err := validateLoginCredentials(userData)
	if err != nil {
		return err
	}

	return nil
}
func ParseHeadersFromRedirect(w http.ResponseWriter, r *http.Request, next http.HandlerFunc) {
	authorization := r.URL.Query().Get("Authorization")
	contentType := r.URL.Query().Get("Content-Type")

	if len(authorization) == 0 || len(contentType) == 0 {
		utility.CustomData{}.Unauthorized(w, r)
		return
	}

	r.Header.Set("Authorization", authorization)
	r.Header.Set("Content-Type", contentType)

	r.URL.Query().Del("Authorization")
	r.URL.Query().Del("Content-Type")

	next(w, r)
}
func ValidateToken(w http.ResponseWriter, req *http.Request, next http.HandlerFunc) {

	token, _ := request.ParseFromRequest(req, request.AuthorizationHeaderExtractor, func(token *jwt.Token) (interface{}, error) {

		// Validate signing method to prevent malicious users to use method: none
		if _, ok := token.Method.(*jwt.SigningMethodRSA); !ok {
			return nil, fmt.Errorf("Unexpected signing method: %v", token.Header["alg"])
		}

		return jwt.ParseRSAPublicKeyFromPEM(pem.EncodeToMemory(&publicBlock))
	})

	// If no token was sent, return unauthorized
	if token == nil {
		utility.CustomData{}.Unauthorized(w, req)
		return
	}

	if _, ok := token.Claims.(jwt.MapClaims); ok && token.Valid {
		next(w, req)
		return
	}

	utility.CustomData{}.Unauthorized(w, req)
}

func GetPermissions(req *http.Request, parameter string) float64 {

	token, _ := request.ParseFromRequest(req, request.AuthorizationHeaderExtractor, func(token *jwt.Token) (interface{}, error) {

		// Validate signing method to prevent malicious users to use method: none
		if _, ok := token.Method.(*jwt.SigningMethodRSA); !ok {
			return nil, fmt.Errorf("Unexpected signing method: %v", token.Header["alg"])
		}

		return jwt.ParseRSAPublicKeyFromPEM(pem.EncodeToMemory(&publicBlock))
	})

	// If no token was sent, return unauthorized
	if token == nil {
		return 0
	}

	if _, ok := token.Claims.(jwt.MapClaims); ok && token.Valid {

		// Typecast cui
		result := token.Claims.(jwt.MapClaims)["cui"].(map[string]interface{})

		// Typecast permissions
		claims := result["permissions"].(map[string]interface{})

		// If the parameter doesn't exist, or is zero, return zero.
		if claims[parameter] == nil || !(claims[parameter].(float64) > 0) {
			return 0
		}

		// Return real value
		return claims[parameter].(float64)
	}

	return 0
}
