/**
 * medfilm-ccms
 * Copyright (C) Sunstate AB - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Simon Wikstrand <simon@sunstate.se>, 2016-12-19
 */

package authentication

import (
	"database/sql"
	"github.com/MedFilm/ccms/lib/database"
	"golang.org/x/crypto/bcrypt"
)

type User struct {
	ID         int              `json:"id"`
	Email      string           `json:"email"`
	Name       string           `json:"name"`
	Permission map[string]int64 `json:"permissions"`
}

func validateLoginCredentials(cred UserCredentials) (User, error) {

	// Fill user struct
	var user User

	// Fill password separately
	var password string

	err := database.DBCon.QueryRow(
		"SELECT id, given_name, email, password FROM employees WHERE email=?",
		cred.Email,
	).Scan(
		&user.ID,
		&user.Name,
		&user.Email,
		&password,
	)

	if err != nil {
		return user, err
	}

	err = bcrypt.CompareHashAndPassword([]byte(password), []byte(cred.Password))
	if err != nil {
		return user, err
	}

	user.Permission = make(map[string]int64)
	err = database.PerformQuery(
		"SELECT permission, level FROM employee_permission WHERE emp_id=?",
		func(rows *sql.Rows) {
			for rows.Next() {
				var (
					permission string
					level      int64
				)

				rows.Scan(&permission, &level)
				user.Permission[permission] = level
			}
		},
		user.ID,
	)
	if err != nil {
		return user, err
	}

	return user, nil
}
