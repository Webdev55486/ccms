/**
 * medfilm-ccms
 * Copyright (C) Sunstate AB - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Simon Wikstrand <simon@sunstate.se>, 2016-12-15
 */

package database

import (
	"database/sql"
	"encoding/json"
	"fmt"
	"strconv"
	"strings"

	"github.com/MedFilm/ccms/lib/config"
	_ "github.com/go-sql-driver/mysql"
)

var DBCon *sql.DB

type NullString struct {
	sql.NullString
}

type NullInt struct {
	sql.NullInt64
}

type NullBool struct {
	sql.NullBool
}

type NullFloat struct {
	sql.NullFloat64
}

func (r *NullString) UnmarshalJSON(data []byte) error {
	// Unmarshalling into a pointer will let us detect null
	var x *string
	if err := json.Unmarshal(data, &x); err != nil {
		return err
	}
	if x != nil {
		r.Valid = true
		r.String = *x
	} else {
		r.Valid = false
	}
	return nil
}

func (r NullString) MarshalJSON() ([]byte, error) {
	if r.Valid {

		if r.String != "" {
			return json.Marshal(r.String)
		}
	}

	return json.Marshal(nil)
}

func (r *NullBool) UnmarshalJSON(data []byte) error {
	// Unmarshalling into a pointer will let us detect null
	var x *bool
	if err := json.Unmarshal(data, &x); err != nil {
		return err
	}
	if x != nil {
		r.Valid = true
		r.Bool = *x
	} else {
		r.Valid = false
	}
	return nil
}

func (r NullBool) MarshalJSON() ([]byte, error) {
	if r.Valid {
		return json.Marshal(r.Bool)
	}

	return json.Marshal(nil)
}

func (r *NullInt) UnmarshalJSON(data []byte) error {
	// Unmarshalling into a pointer will let us detect null
	var x *int64
	if err := json.Unmarshal(data, &x); err != nil {
		return err
	}
	if x != nil {
		r.Valid = true
		r.Int64 = *x
	} else {
		r.Valid = false
	}
	return nil
}

func (r NullInt) MarshalJSON() ([]byte, error) {
	if r.Valid {
		return json.Marshal(r.Int64)
	}

	return json.Marshal(nil)
}

func (r *NullFloat) UnmarshalJSON(data []byte) error {
	// Unmarshalling into a pointer will let us detect null
	var x *float64
	if err := json.Unmarshal(data, &x); err != nil {
		return err
	}
	if x != nil {
		r.Valid = true
		r.Float64 = *x
	} else {
		r.Valid = false
	}
	return nil
}

func (r NullFloat) MarshalJSON() ([]byte, error) {
	if r.Valid {
		return json.Marshal(r.Float64)
	}

	return json.Marshal(0.00)
}

func StartDriver() error {
	var err error
	importedConfig := config.BuiltConfig.Database

	// Join several configuration strings to "user:password@tcp(host:xxxx)/dbname"
	DBCon, err = sql.Open("mysql", strings.Join([]string{
		importedConfig.User,
		":",
		importedConfig.Pass,
		"@",
		"tcp",
		"(",
		importedConfig.Host,
		":",
		strconv.Itoa(importedConfig.Port),
		")",
		"/",
		importedConfig.Db,
	}, ""))

	if err != nil {
		return err
	}

	// Open doesn't open a connection. Validate DSN data:
	err = DBCon.Ping()
	if err != nil {
		return err
	}

	DBCon.SetMaxOpenConns(10)

	return nil
}
func PerformQuery(q string, op func(*sql.Rows), args ...interface{}) error {
	rows, err := DBCon.Query(q, args...)
	if err == sql.ErrNoRows {
		rows.Close()
		return nil
	}
	if err != nil {
		fmt.Println(err.Error())
		return err
	}
	defer rows.Close()
	op(rows)
	return nil
}

func PerformStmt(q string, op func(sql.Rows), stmt ...string) {

}
