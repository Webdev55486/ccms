/**
 * medfilm-ccms
 * Copyright (C) Sunstate AB - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Simon Wikstrand <simon@sunstate.se>, 2016-12-15
 */

package tools

import (
	"bufio"
	"encoding/base64"
	"encoding/json"
	"errors"
	"github.com/gorilla/mux"
	"io"
	"math/rand"
	"net/http"
	"os"
	"strconv"
)

func InterceptHeaders(rw http.ResponseWriter, r *http.Request, next http.HandlerFunc) {
	rw.Header().Set("Protocol", r.Proto)
	rw.Header().Set("Creator", "Sunstate")
	next(rw, r)
}

func DecodeJson(entity interface{}, req *http.Request) error {

	// Decode json
	decoder := json.NewDecoder(req.Body)

	// Decode json to user struct
	err := decoder.Decode(&entity)
	if err != nil && err != io.EOF {
		return err
	}

	// Close stream from body when done
	defer req.Body.Close()
	return nil
}

func GetIdFromUrl(req *http.Request) (int64, error) {

	// Check for id in params
	vars := mux.Vars(req)
	if val, ok := vars["id"]; ok {
		Id, err := strconv.ParseInt(val, 10, 64)
		return Id, err
	}

	return 0, errors.New("Could not convert or find id")
}

var letterRunes = []rune("abcdefgh0123456789")

// Randomize a string
func RandStringRunes(n int) string {
	b := make([]rune, n)
	for i := range b {
		b[i] = letterRunes[rand.Intn(len(letterRunes))]
	}
	return string(b)
}

// Convert image binary to base64
func ConvImageToBase64(path string) (string, error) {
	if path == "" {
		return "", errors.New("No path given")
	}

	imgFile, err := os.Open(path)
	if err != nil {
		return "", err
	}

	defer imgFile.Close()

	// Create new buffer on image size
	fInfo, _ := imgFile.Stat()
	buf := make([]byte, fInfo.Size())

	// Read content to buffer
	fReader := bufio.NewReader(imgFile)
	fReader.Read(buf)

	imgBase64Str := base64.StdEncoding.EncodeToString(buf)

	return imgBase64Str, nil
}

func ConvImageToBinary(path string, baseStr string) error {
	sDec, _ := base64.StdEncoding.DecodeString(baseStr)

	f, err := os.Create(path)
	if err != nil {
		return err
	}

	defer f.Close()

	f.Write(sDec)
	if err != nil {
		return err
	}

	return nil
}
