/**
 * medfilm-ccms
 * Copyright (C) Sunstate AB - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Simon Wikstrand <simon@sunstate.se>, 2016-12-15
 */

package config

import (
	"time"

	"github.com/BurntSushi/toml"
)

type config struct {
	Application application `toml:"application"`
	Database    database    `toml:"database"`
	Security    security    `toml:"security"`
	Email       email       `toml:"email"`
	Sms         sms         `toml:"sms"`
}

type sms struct {
	Username string
	Password string
}

type application struct {
	Name           string
	Version        string
	SessionTimeout time.Duration
	Publish        string
	Url            string
}

type database struct {
	Host string
	Port int
	Db   string
	User string
	Pass string
}

type security struct {
	Secret     string
	PrivateKey string
	PublicKey  string
}

type email struct {
	ServerToken  string
	AccountToken string
	From         string
}

var BuiltConfig config

func NewConfig() error {
	if _, err := toml.DecodeFile("config/config.toml", &BuiltConfig); err != nil {
		return err
	}

	return nil
}
