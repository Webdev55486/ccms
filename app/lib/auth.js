/**
 * medfilm-ccms
 * Copyright (C) Sunstate AB - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Simon Wikstrand <simon@sunstate.se>, 2016-12-14
 */

/**
 * This is a function that handles authentication.
 */

import Vue from 'vue'
import VueResource from 'vue-resource'
import jwtDecode from 'jwt-decode'

Vue.use(VueResource)

const auth = new Vue({
  data () {
    return {
      ready: false,
      authHeader: {
        'Authorization': null,
        'Content-Type': 'application/json'
      },
      user: {
        id: null,
        name: null,
        email: null,
        permissions: {
          accounts: 0,
          prospects: 0,
          website: 0,
          employees: 0,
          admin: 0
        }
      },
      authenticated: false,
      token: null
    }
  },
  created () {
    this.token = typeof window.localStorage.getItem('token') !== 'undefined' ? window.localStorage.getItem('token') : null
  },
  methods: {

    /**
     * Initiate this function, make sure to use this before calling any function in
     * this class
     * @returns {Promise}
     */
    init () {
      let that = this
      return new Promise(function (resolve) {
        if (that.ready) {
          return resolve(that)
        }

        that.checkAuth().then(function () {
          that.ready = true
          return resolve(that)
        })
      })
    },

    parseUser () {
      let claims = jwtDecode(this.token)
      Object.assign(this.user, claims['cui'])
    },

    /**
     *
     * @param credentials
     * @returns {Promise}
     */
    login (credentials) {
      let that = this

      return new Promise(function (resolve, reject) {
        that.$http.post('/api/auth/login', credentials).then(
          // Successful response
          (response) => {
            if (response.data.status !== 200) {
              return resolve(false)
            }

            that.token = response.data.data.Token
            window.localStorage.setItem('token', response.data.data.Token)
            that.authenticated = true
            that.parseUser()
            that.$emit('login-successful')
            return resolve(true)
          },
          // Failed response
          (response) => {
            return reject(response.status)
          }
        )
      })
    },

    logout () {
      window.localStorage.removeItem('token')
      this.$emit('force-logout')
    },

    /**
     *
     * @returns {Promise}
     */
    checkAuth () {
      let that = this

      return new Promise(function (resolve, reject) {
        if (that.token == null) {
          return resolve(false)
        }

        that.authHeader.Authorization = 'Bearer ' + that.token

        that.$http.get('/api/auth/validate', {
          headers: that.authHeader
        }).then(
          (response) => {
            if (response.data.status !== 200) {
              that.authenticated = false
              window.localStorage.removeItem('token')
              that.$emit('force-logout')
              return resolve(false)
            }

            that.authenticated = true
            that.parseUser()
            return resolve(true)
          }, (response) => {
            reject(response.status)
          })
      })
    },

    /**
     * This function will validate if the user is authorized
     * if not, redirect the user to /login
     * @param to
     * @param _from
     * @param next
     */
    requireAuth (to, _from, next) {
      this.checkAuth().then(function (resp) {
        if (!resp) {
          return next({
            path: '/login',
            query: {
              redirect: to.fullPath
            }
          })
        }
        return next()
      })
    },

    afterAuth (_to, from, next) {
      if (this.authenticated) {
        return next(from.path)
      }

      return next()
    },

    requireAdminAuth (to, _from, next) {
      let that = this
      that.checkAuth().then(function (resp) {
        if (!resp || that.user.admin === 0) {
          return next({
            path: '/accounts',
            query: {
              redirect: to.fullPath
            }
          })
        }

        return next()
      })
    }
  },
  watch: {
    'token': {
      handler () {
        this.authHeader.Authorization = 'Bearer ' + this.token
      }
    }
  }
})

export default auth
