import Vue from 'vue'

// Used to build html strings for 
// generating pdfs from html
const HtmlParser = new Vue({
  data: function () {
    return {
      tree: {},
      root: false
    }
  },
  methods: {
    // Creates a tree
    from: function (root) {
      this.root = root
      this.tree = {}

      this.tree = this._parseTree(root)
      return this.tree
    },
    // Deep parse tree from root
    _parseTree: function (element) {
      // Element is undefined
      if (!element) return ''

      // Build html string
      const build = () => {
        // Collect variables
        const tagname = element.tagName
        const attributes = this._parseAttrs(element)
        const style = this._parseStyle(element)
        const children = []
        const text = element.innerHTML[0] !== '<' ? element.innerHTML : ''

        // Iterate over children
        for (let i = 0; i < element.children.length; i++) {
          const child = element.children[i]
          children[i] = this._parseTree(child)
        }

        // Build string
        return `<${tagname} ${attributes} style="${style}">${text}${children.join('')}</${tagname}>`
      }

      // Return string
      return build()
    },
    // Get styling of passed element
    _parseStyle: function (element) {
      const styling = []
      const style = window.getComputedStyle(element)
      // Iterate over style keys
      for (const key in element.style) {
        const value = style.getPropertyValue(key)
        const kw = `${key}:${value}`

        if (value.length !== 0) styling.push(kw)
      }
      // Join and return
      return styling.join(';')
    },
    // Get all attributes of passed element
    _parseAttrs: function (element) {
      const attributes = []
      // Iterate over attributes
      for (const key in element.attributes) {
        const kw = `${key}="${element.getAttribute(key)}"`
        attributes.push(kw)
      }
      return attributes.join(' ')
    }
  }
})

export default HtmlParser