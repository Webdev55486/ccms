/**
 * medfilm-ccms
 * Copyright (C) Sunstate AB - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Simon Wikstrand <simon@sunstate.se>, 2016-12-14
 */

import VueRouter from 'vue-router'

// Auth Library
import auth from './auth.js'

/**
 * All router components are loaded here
 */
// Generic components
const login = (resolve) => {
  return require(['./../components/content/login.vue'], resolve)
}
const password = (resolve) => {
  return require(['./../components/content/password.vue'], resolve)
}
const reset = (resolve) => {
  return require(['./../components/content/reset.vue'], resolve)
}

// Accounts
const accounts = (resolve) => {
  return require(['./../components/content/accounts.vue'], resolve)
}
const accountsAdd = (resolve) => {
  return require(['./../components/content/accounts/view/settings.vue'], resolve)
}
const accountsView = (resolve) => {
  return require(['./../components/content/accounts/view.vue'], resolve)
}
const accountsViewSubscriptions = (resolve) => {
  return require(['./../components/content/accounts/view/subscriptions.vue'], resolve)
}
const accountsViewSubscriptionsEdit = (resolve) => {
  return require(['./../components/content/accounts/view/subscriptions/edit.vue'], resolve)
}
const accountsViewDevices = (resolve) => {
  return require(['./../components/content/accounts/view/devices.vue'], resolve)
}
const accountsViewStatistics = (resolve) => {
  return require(['./../components/content/accounts/view/statistics.vue'], resolve)
}
const accountsViewAgreements = (resolve) => {
  return require(['./../components/content/accounts/view/agreements.vue'], resolve)
}
const accountsViewChanges = (resolve) => {
  return require(['../components/content/accounts/view/changes.vue'], resolve)
}
const accountsViewHistory = (resolve) => {
  return require(['./../components/content/accounts/view/history.vue'], resolve)
}
const accountsViewSettings = (resolve) => {
  return require(['./../components/content/accounts/view/settings.vue'], resolve)
}
const accountsViewInformation = (resolve) => {
  return require(['./../components/content/accounts/view/information.vue'], resolve)
}
// import accountsAdd from './components/content/accounts/addEdit.vue'
// import accountsEdit from './components/content/accounts/edit.vue'
// import accountsViewDevices from './components/content/accounts/view/devices.vue'
// import accountsViewDevicesAdd from './components/content/accounts/view/devices/addEdit.vue'
// import accountsViewDevicesEdit from './components/content/accounts/view/devices/edit.vue'
// import accountsViewSubscriptions from './components/content/accounts/view/subscriptions.vue'
// import accountsViewSubscriptionsAdd from './components/content/accounts/view/subscriptions/addEdit.vue'
// import accountsViewSubscriptionsEdit from './components/content/accounts/view/subscriptions/edit.vue'

// Prospects
const prospects = (resolve) => {
  return require(['./../components/content/prospects.vue'], resolve)
}
const prospectsAdd = (resolve) => {
  return require(['./../components/content/prospects/add.vue'], resolve)
}

// Website
const website = (resolve) => {
  return require(['./../components/content/website.vue'], resolve)
}
const websiteArticleList = (resolve) => {
  return require(['./../components/content/website/articleList.vue'], resolve)
}
const websiteArticle = (resolve) => {
  return require(['./../components/content/website/article.vue'], resolve)
}
const websiteContentEdit = (resolve) => {
  return require(['./../components/content/website/contentEdit.vue'], resolve)
}
const websiteLanguageFile = (resolve) => {
  return require(['./../components/content/website/languageFile.vue'], resolve)
}
const websiteLogoSort = (resolve) => {
  return require(['./../components/content/website/logotypeSort.vue'], resolve)
}

// Libraries
const libraries = (resolve) => {
  return require(['./../components/content/libraries.vue'], resolve)
}

const librariesLibrary = (resolve) => {
  return require(['./../components/content/libraries/category.vue'], resolve)
}

const librariesFilm = (resolve) => {
  return require(['./../components/content/libraries/film.vue'], resolve)
}

// Employees
const employees = (resolve) => {
  return require(['./../components/content/employees.vue'], resolve)
}

// Admin
const admin = (resolve) => {
  return require(['./../components/content/admin.vue'], resolve)
}
const adminAddEdit = (resolve) => {
  return require(['../components/content/admin/addEdit.vue'], resolve)
}
const firestick = (resolve) => {
  return require(['./../components/content/firestick.vue'], resolve)
}
const firestickAddEdit = (resolve) => {
  return require(['../components/content/firestick/addEdit.vue'], resolve)
}

const routes = [
  {
    path: '/',
    redirect () {
      if (auth.authenticated) {
        return {
          path: '/accounts'
        }
      }
      return {
        path: '/login'
      }
    }
  },
  {
    path: '/accounts',
    component: accounts,
    beforeEnter: auth.requireAuth
  },
  {
    path: '/accounts/add',
    name: 'accountsAdd',
    component: accountsAdd,
    beforeEnter: auth.requireAuth
  },
  {
    path: '/accounts/:id',
    component: accountsView,
    beforeEnter: auth.requireAuth,
    children: [
      {
        path: 'subscriptions',
        name: 'accountsViewSubscriptions',
        component: accountsViewSubscriptions
      },
      {
        path: 'subscriptions/add',
        name: 'accountsViewSubscriptionsAdd',
        component: accountsViewSubscriptionsEdit
      },
      {
        path: 'subscriptions/:sub',
        name: 'accountsViewSubscriptionsEdit',
        component: accountsViewSubscriptionsEdit
      },
      {
        path: 'devices',
        name: 'accountsViewDevices',
        component: accountsViewDevices
      },
      {
        path: 'statistics',
        name: 'accountsViewStatistics',
        component: accountsViewStatistics
      },
      {
        path: 'agreements',
        name: 'accountsViewAgreements',
        component: accountsViewAgreements
      },
      {
        path: 'changes',
        name: 'accountsViewChanges',
        component: accountsViewChanges
      },
      {
        path: 'history',
        name: 'accountsViewHistory',
        component: accountsViewHistory
      },
      {
        path: 'settings',
        name: 'accountsViewSettings',
        component: accountsViewSettings
      },
      {
        path: 'information',
        name: 'accountsViewInformation',
        component: accountsViewInformation
      }
    ]
  },
  {
    path: '/prospects',
    component: prospects,
    beforeEnter: auth.requireAuth
  },
  {
    path: '/prospects/add',
    component: prospectsAdd,
    beforeEnter: auth.requireAuth
  },
  {
    path: '/website',
    component: website,
    beforeEnter: auth.requireAuth,
    children: [
      {
        path: '',
        name: 'websiteArticleList',
        component: websiteArticleList
      },
      {
        path: 'article/:id',
        name: 'websiteArticle',
        component: websiteArticle
      },
      {
        path: 'contentEdit',
        name: 'websiteContentEdit',
        component: websiteContentEdit
      },
      {
        path: 'file',
        name: 'languageFileEdit',
        component: websiteLanguageFile
      },
      {
        path: 'logotype',
        name: 'websiteLogoSort',
        component: websiteLogoSort
      }
    ]
  },
  {
    path: '/libraries',
    component: libraries,
    beforeEnter: auth.requireAuth
  },
  {
    path: '/libraries/category/:id',
    component: librariesLibrary,
    beforeEnter: auth.requireAuth
  },
  {
    path: '/libraries/film/:id',
    component: librariesFilm,
    beforeEnter: auth.requireAuth
  },
  {
    path: '/firestick',
    component: firestick,
    beforeEnter: auth.requireAdminAuth
  },
  {
    path: '/firestick/add',
    component: firestickAddEdit,
    beforeEnter: auth.requireAdminAuth
  },
  {
    path: '/firestick/:id',
    name: 'videoEdit',
    component: firestickAddEdit,
    beforeEnter: auth.requireAdminAuth
  },
  {
    path: '/employees',
    component: employees,
    beforeEnter: auth.requireAuth
  },
  {
    path: '/password',
    component: password,
    beforeEnter: auth.afterAuth
  },
  {
    path: '/reset/:id',
    component: reset,
    beforeEnter: auth.afterAuth
  },
  {
    path: '/login',
    component: login,
    beforeEnter: auth.afterAuth
  },
  {
    path: '/admin',
    component: admin,
    beforeEnter: auth.requireAdminAuth
  },
  {
    path: '/admin/add',
    component: adminAddEdit,
    beforeEnter: auth.requireAdminAuth
  },
  {
    path: '/admin/:id',
    name: 'adminEdit',
    component: adminAddEdit,
    beforeEnter: auth.requireAdminAuth
  },
  {
    path: '*',
    redirect: {
      path: '/'
    }
  }
]

/**
 * This function handle all routing of views
 * @type {VueRouter}
 */
export const router = new VueRouter({
  mode: 'history',
  routes: routes
})
