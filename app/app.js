/**
 * medfilm-ccms
 * Copyright (C) Sunstate AB - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Simon Wikstrand <simon@sunstate.se>, 2016-12-12
 */

import Vue from 'vue'
import VueRouter from 'vue-router'
import VueResource from 'vue-resource'
import VueFroala from 'vue-froala-wysiwyg'
import App from './components/app.vue'
import { router } from './lib/router.js'
import Auth from './lib/auth.js'

Vue.use(VueRouter)
Vue.use(VueResource)
Vue.use(VueFroala)

require('font-awesome')

// Initialize the auth object before loading the main Vue app.
Auth.init().then(function () {
  // Initialize the main Vue app
  new Vue({
    router,
    el: '#app',
    render: (h) => h(App)
  })
})
