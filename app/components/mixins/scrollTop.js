/**
 * ccms
 * Copyright (C) Sunstate AB - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Simon Wikstrand <simon@sunstate.se>, 2017-01-20
 */

export default {
  mounted () {
    window.requestAnimFrame = (function () {
      return window.requestAnimationFrame ||
        window.webkitRequestAnimationFrame ||
        window.mozRequestAnimationFrame ||
        function (callback) {
          window.setTimeout(callback, 1000 / 60)
        }
    })()
  },
  methods: {
    scrollTo (position, duration, animation) {
      let scrollY = window.scrollY || document.documentElement.scrollTop
      let scrollTargetY = position || 0
      duration = duration || 2000
      animation = animation || 'easeOutSine'
      let currentTime = 0

      // Min time .1 max time .8 seconds
      let time = window.Math.max(0.1, window.Math.min(window.Math.abs(scrollY - scrollTargetY) / duration, 0.8))

      let easingEquations = {
        easeOutSine: function (pos) {
          return Math.sin(pos * (Math.PI / 2))
        },
        easeInOutSine: function (pos) {
          return (-0.5 * (Math.cos(Math.PI * pos) - 1))
        },
        easeInOutQuint: function (pos) {
          if ((pos /= 0.5) < 1) {
            return 0.5 * Math.pow(pos, 5)
          }
          return 0.5 * (Math.pow((pos - 2), 5) + 2)
        }
      }

      function tick () {
        currentTime += 1 / 60

        let p = currentTime / time
        let t = easingEquations[animation](p)

        if (p < 1) {
          window.requestAnimFrame(tick)

          window.scrollTo(0, scrollY + ((scrollTargetY - scrollY) * t))
          return
        }

        window.scrollTo(0, scrollTargetY)
      }

      tick()
    }
  }
}
