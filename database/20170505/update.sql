CREATE TABLE page_content
(
  id INT(11) UNSIGNED PRIMARY KEY NOT NULL AUTO_INCREMENT,
  identifier VARCHAR(50) NOT NULL,
  language VARCHAR(2) NOT NULL,
  content LONGTEXT,
  created TIMESTAMP DEFAULT CURRENT_TIMESTAMP NOT NULL,
  updated TIMESTAMP ON UPDATE CURRENT_TIMESTAMP DEFAULT CURRENT_TIMESTAMP NOT NULL
);