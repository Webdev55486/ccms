CREATE UNIQUE INDEX employees_email_uindex ON employees (email);

ALTER TABLE employee_permission ADD CONSTRAINT employee_permission_emp_id_permission_pk UNIQUE (emp_id, permission);

CREATE TABLE countries
(
  id INT(11) unsigned PRIMARY KEY NOT NULL AUTO_INCREMENT,
  name VARCHAR(255) NOT NULL,
  created TIMESTAMP DEFAULT CURRENT_TIMESTAMP NOT NULL
);

CREATE TABLE country_region
(
  id INT(11) UNSIGNED PRIMARY KEY NOT NULL AUTO_INCREMENT,
  country_id INT(11) UNSIGNED NOT NULL,
  name VARCHAR(255) NOT NULL,
  created TIMESTAMP DEFAULT CURRENT_TIMESTAMP NOT NULL,
  CONSTRAINT country_region_countries_id_fk FOREIGN KEY (country_id) REFERENCES countries (id) ON DELETE CASCADE ON UPDATE CASCADE
);

CREATE TABLE prospects
(
  id INT(11) unsigned PRIMARY KEY NOT NULL AUTO_INCREMENT,
  country_region INT(11) unsigned NOT NULL,
  hospital VARCHAR(255),
  clinic VARCHAR(255) NOT NULL,
  category INT(10) unsigned NOT NULL,
  contact_person VARCHAR(255),
  contact_phone VARCHAR(30),
  contact_email VARCHAR(255),
  current_status TEXT,
  reminder TIMESTAMP DEFAULT CURRENT_TIMESTAMP NULL DEFAULT NULL,
  accountable INT(11) unsigned,
  updated TIMESTAMP DEFAULT CURRENT_TIMESTAMP NOT NULL,
  created TIMESTAMP DEFAULT CURRENT_TIMESTAMP NOT NULL,
  CONSTRAINT prospects_categories_id_fk FOREIGN KEY (category) REFERENCES categories (id) ON UPDATE CASCADE
);
CREATE INDEX prospects_categories_id_fk ON prospects (category);
CREATE INDEX prospects_country_region_id_fk ON prospects (country_region);
CREATE INDEX prospects_employees_id_fk ON prospects (accountable);


CREATE TABLE comments
(
  id INT(11) UNSIGNED PRIMARY KEY NOT NULL AUTO_INCREMENT,
  prospect_id INT(11) UNSIGNED,
  user_id INT(11) UNSIGNED,
  employee_id INT(11) UNSIGNED NOT NULL,
  description TEXT NOT NULL,
  updated TIMESTAMP ON UPDATE CURRENT_TIMESTAMP DEFAULT current_timestamp NOT NULL,
  created TIMESTAMP DEFAULT CURRENT_TIMESTAMP NOT NULL,
  CONSTRAINT comments_employees_id_fk FOREIGN KEY (employee_id) REFERENCES employees (id),
  CONSTRAINT comments_users_id_fk FOREIGN KEY (user_id) REFERENCES users (id) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT comments_prospects_id_fk FOREIGN KEY (prospect_id) REFERENCES prospects (id) ON DELETE CASCADE ON UPDATE CASCADE
);