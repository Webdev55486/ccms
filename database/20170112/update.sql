CREATE TABLE subscriptions_options
(
  id INT(11) unsigned PRIMARY KEY NOT NULL AUTO_INCREMENT,
  subscription_id INT(10) unsigned NOT NULL,
  price INT(11),
  price_en INT(11),
  price_se INT(11),
  price_no INT(11),
  price_fi INT(11),
  price_fa INT(11),
  price_ar INT(11),
  price_hr INT(11),
  currency TEXT,
  updated TIMESTAMP DEFAULT CURRENT_TIMESTAMP NOT NULL,
  created TIMESTAMP DEFAULT CURRENT_TIMESTAMP NOT NULL,
  CONSTRAINT subscriptions_options_subscriptions_id_fk FOREIGN KEY (subscription_id) REFERENCES subscriptions (id) ON DELETE CASCADE ON UPDATE CASCADE
);
CREATE INDEX subscriptions_options_subscriptions_id_fk ON subscriptions_options (subscription_id);

ALTER TABLE subscriptions_options ADD CONSTRAINT subscriptions_options_subscription_id_pk UNIQUE (subscription_id);

ALTER TABLE users MODIFY message_no TEXT;
ALTER TABLE users MODIFY message_fi TEXT;
ALTER TABLE users MODIFY message_fa TEXT;
ALTER TABLE users MODIFY message_ar TEXT;
ALTER TABLE users MODIFY message_hr TEXT;

ALTER TABLE users MODIFY description_en TEXT;
ALTER TABLE users MODIFY description_se TEXT;
ALTER TABLE users MODIFY description_no TEXT;
ALTER TABLE users MODIFY description_fi TEXT;
ALTER TABLE users MODIFY description_fa TEXT;
ALTER TABLE users MODIFY description_ar TEXT;
ALTER TABLE users MODIFY description_hr TEXT;

ALTER TABLE users ALTER COLUMN api_id SET DEFAULT '2DX23Ml5CftRR5X';
ALTER TABLE users ALTER COLUMN api_secret SET DEFAULT '';

ALTER TABLE users ALTER COLUMN company_logo SET DEFAULT '';
ALTER TABLE users ALTER COLUMN company_header SET DEFAULT '';
ALTER TABLE users MODIFY company_header VARCHAR(255) DEFAULT '';