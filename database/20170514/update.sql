# Create new table
create table film_subject
(
  id int(11) UNSIGNED auto_increment
    primary key,
  name varchar(255) not null,
  type varchar(50) default 'library' null,
  parent_id int(11) UNSIGNED null,
  created TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
  updated TIMESTAMP ON UPDATE CURRENT_TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
  constraint film_subject__id_fk
  foreign key (parent_id) references film_subject (id)
    on update cascade on delete cascade
);

DROP TABLE quotes;

# Create index
create index film_subject__id_fk on film_subject (parent_id);

# Insert from old tables to new table
INSERT INTO film_subject (name, type)
    SELECT name, 'library'
      FROM libraries;

INSERT into film_subject (name, type, parent_id)
  SELECT
    cat.name,
    'category',
    fs.id
  FROM categories cat
  INNER JOIN
    libraries lb ON cat.library_id = lb.id
  LEFT OUTER JOIN
    film_subject fs ON lb.name = fs.name;

# Change all prospects
ALTER TABLE prospects DROP FOREIGN KEY prospects_libraries_id_fk;

UPDATE prospects pt
  SET pt.category = (
    SELECT fs.id
    FROM libraries lb
    INNER JOIN
      film_subject fs ON lb.name = fs.name
    WHERE
      lb.id = pt.category
    LIMIT 1
  );

ALTER TABLE prospects MODIFY category INT(11) unsigned;

ALTER TABLE prospects ADD CONSTRAINT prospects_film_subject_id_fk FOREIGN KEY (category) REFERENCES film_subject(id) ON UPDATE CASCADE ON DELETE RESTRICT;

ALTER TABLE film DROP FOREIGN KEY film_ibfk_1;
ALTER TABLE film DROP INDEX library_id;

ALTER TABLE film CHANGE category_id subject_id INT(11) unsigned NOT NULL;

UPDATE film fm
  SET fm.subject_id = (
    SELECT fs.id
    FROM categories cat
      INNER JOIN
      film_subject fs ON cat.name = fs.name
    WHERE
      cat.id = fm.subject_id
    LIMIT 1
  );

ALTER TABLE film ADD CONSTRAINT film_film_subject_id_fk FOREIGN KEY (subject_id) REFERENCES film_subject(id) ON UPDATE CASCADE ON DELETE RESTRICT;

DROP TABLE categories;
DROP TABLE libraries;