create table todo_category (
    id int(10) unsigned not null primary key auto_increment,
    label varchar(255) not null
);

create table todo_group (
    id int(10) unsigned not null primary key auto_increment,
    label varchar(255) not null,
    priority int(10) unsigned not null,
    category_id int(10) unsigned,
    foreign key (category_id) references todo_category(id)
    on delete cascade on update cascade
);

create table todo_item (
    id int(10) unsigned not null primary key auto_increment,
    label varchar(255) not null,
    content text,
    group_id int(10) unsigned,
    color varchar(255),
    isOpen bool default 0,
    priority int(10) unsigned not null,
    foreign key (group_id) references todo_group(id)
    on delete cascade on update cascade
);