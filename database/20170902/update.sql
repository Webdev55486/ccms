create table drivinglog_person (
    id int not null primary key auto_increment,

    name varchar(255) not null,

    createdAt timestamp not null default current_timestamp,
    updatedAt timestamp not null default current_timestamp on update current_timestamp
);
create table drivinglog (
    id int not null primary key auto_increment,
    drivinglog_person_id int not null,

    type enum('Company Car', 'Private Car'),
    date varchar(255),
    travel_route varchar(255),
    errand varchar(255),
    mileage_start int,
    mileage_end int,

    key fk_drivinglog_person_id (drivinglog_person_id),
    constraint fk_drivinglog_person_id foreign key(drivinglog_person_id) references drivinglog_person(id),

    createdAt timestamp not null default current_timestamp,
    updatedAt timestamp not null default current_timestamp on update current_timestamp
);