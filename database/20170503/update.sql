create table system_improvements (
    id integer not null primary key AUTO_INCREMENT,
    heading varchar(255) not null,
    content LONGTEXT not null,
    author varchar(255) not null,
    createdAt date not null
)