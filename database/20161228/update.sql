ALTER TABLE prospects ADD active BIT(1) DEFAULT b'1' NOT NULL;
ALTER TABLE prospects
  MODIFY COLUMN active BIT(1) NOT NULL DEFAULT b'1' AFTER accountable;

ALTER TABLE country_region ADD active BIT(1) DEFAULT b'1' NOT NULL;
ALTER TABLE country_region
  MODIFY COLUMN active BIT(1) NOT NULL DEFAULT b'1' AFTER country_id;

ALTER TABLE countries ADD active BIT(1) DEFAULT b'1' NOT NULL;
ALTER TABLE countries
  MODIFY COLUMN active BIT(1) NOT NULL DEFAULT b'1' AFTER name;