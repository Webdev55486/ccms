ALTER TABLE subscriptions MODIFY accesscode VARCHAR(30);
ALTER TABLE subscriptions MODIFY film_file_override VARCHAR(255);

UPDATE
  users
SET
  company_header = REPLACE(company_header, '/var/www/html/uploads/logos/', '');

UPDATE
  users
SET
  company_logo = REPLACE(company_logo, '/var/www/html/uploads/logos/', '');