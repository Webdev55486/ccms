CREATE TABLE employee_reset
(
  id INT(11) unsigned PRIMARY KEY NOT NULL AUTO_INCREMENT,
  employee_id INT(11) unsigned NOT NULL,
  hash VARCHAR(255) NOT NULL,
  updated TIMESTAMP DEFAULT CURRENT_TIMESTAMP NOT NULL,
  created TIMESTAMP DEFAULT CURRENT_TIMESTAMP NOT NULL,
  CONSTRAINT emplyee_reset_employees_id_fk FOREIGN KEY (employee_id) REFERENCES employees (id) ON DELETE CASCADE ON UPDATE CASCADE
);
CREATE INDEX emplyee_reset_employees_id_fk ON employee_reset (employee_id);