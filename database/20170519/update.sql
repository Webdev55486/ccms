ALTER TABLE film_languages ADD description LONGTEXT NULL;
ALTER TABLE film_languages
  MODIFY COLUMN description LONGTEXT AFTER name;