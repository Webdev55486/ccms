CREATE TABLE employees
(
  id INT(11) unsigned PRIMARY KEY NOT NULL AUTO_INCREMENT,
  email VARCHAR(255) NOT NULL,
  password TEXT,
  given_name VARCHAR(255) NOT NULL,
  administrator BIT(1) DEFAULT b'0',
  updated TIMESTAMP DEFAULT CURRENT_TIMESTAMP NOT NULL,
  created TIMESTAMP DEFAULT CURRENT_TIMESTAMP NOT NULL
);

CREATE TABLE login_session
(
  id BIGINT(20) UNSIGNED PRIMARY KEY NOT NULL AUTO_INCREMENT,
  user_id INT(11) UNSIGNED NOT NULL,
  session_key VARCHAR(255) NOT NULL,
  updated TIMESTAMP ON UPDATE CURRENT_TIMESTAMP DEFAULT CURRENT_TIMESTAMP NOT NULL,
  created TIMESTAMP DEFAULT CURRENT_TIMESTAMP NOT NULL,
  CONSTRAINT login_session_employees_id_fk FOREIGN KEY (user_id) REFERENCES employees (id) ON DELETE CASCADE ON UPDATE CASCADE
);