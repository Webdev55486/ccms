ALTER TABLE film_languages ADD original TINYINT(1) DEFAULT 1 NOT NULL;
ALTER TABLE film_languages
  MODIFY COLUMN original TINYINT(1) NOT NULL DEFAULT 1 AFTER film_id;

CREATE TABLE subscription_film_language_binding
(
  id INT(11) UNSIGNED PRIMARY KEY NOT NULL AUTO_INCREMENT,
  subscription_id INT(11) UNSIGNED NOT NULL,
  film_id INT(11) UNSIGNED NOT NULL,
  created TIMESTAMP DEFAULT CURRENT_TIMESTAMP NOT NULL,
  updated TIMESTAMP ON UPDATE CURRENT_TIMESTAMP DEFAULT CURRENT_TIMESTAMP NOT NULL
);

ALTER TABLE subscription_film_language_binding CHANGE film_id film_language_id INT(11) unsigned NOT NULL;
ALTER TABLE subscription_film_language_binding
  ADD CONSTRAINT subscription_film_language_binding_film_languages_id_fk
FOREIGN KEY (film_language_id) REFERENCES film_languages (id) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE subscription_film_language_binding
  ADD CONSTRAINT subscription_film_language_binding_subscriptions_id_fk
FOREIGN KEY (subscription_id) REFERENCES subscriptions (id) ON DELETE CASCADE ON UPDATE CASCADE;