drop table meeting;
drop table meeting_country;

create table meeting (
  id int not null primary key auto_increment,
  clinic varchar(255) not null,
  country varchar(100) not null,
  date date not null,
  kam varchar(255),
  booked_by varchar(255) not null default "ProToSell",
  completed boolean default 0,
  canceled boolean default 0,
  comments text
);