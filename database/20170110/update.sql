ALTER TABLE prospects DROP FOREIGN KEY prospects_categories_id_fk;
ALTER TABLE prospects
  ADD CONSTRAINT prospects_libraries_id_fk
FOREIGN KEY (category) REFERENCES libraries (id) ON UPDATE CASCADE;