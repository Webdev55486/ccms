ALTER TABLE prospects DROP FOREIGN KEY prospects_film_subject_id_fk;
ALTER TABLE prospects
ADD CONSTRAINT prospects_film_subject_id_fk
FOREIGN KEY (category) REFERENCES film_subject (id) ON DELETE SET NULL ON UPDATE CASCADE;