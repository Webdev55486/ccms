create table timelog (
  id integer not null auto_increment primary key,
  employee int(11) unsigned not null,
  type integer not null,
  year integer not null,
  month integer not null,
  day integer not null,
  start time not null,
  break time not null,
  end time not null,
  note text,
  updatedAt date,
  foreign key(employee) references employees(id)
);