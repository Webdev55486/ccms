ALTER TABLE prospects ADD categoryName VARCHAR(50) NULL;
ALTER TABLE prospects
  MODIFY COLUMN categoryName VARCHAR(50) AFTER category;

describe prospects;

UPDATE
  prospects p
SET
  p.categoryName = (
      SELECT
        l.name
      FROM
        film_subject l
      WHERE
        p.category = l.id
  );

ALTER TABLE prospects DROP FOREIGN KEY prospects_libraries_id_fk;
ALTER TABLE prospects DROP category;