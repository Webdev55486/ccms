create table meeting_country (
    id int not null primary key auto_increment,
    name varchar(100) not null,

    createdAt timestamp not null default current_timestamp,
    updatedAt timestamp default current_timestamp on update current_timestamp
);

create table meeting (
    id int not null primary key auto_increment,
    meeting_country_id int not null,
    parent_id int,

    planned_date date not null,
    clinic varchar(100),
    completed boolean not null default 0,
    moved_to_date date,
    comment varchar(255),
    kam varchar(255) not null,

    key fk_meeting_country_id (meeting_country_id),
    key fk_parent_id (parent_id),
    constraint fk_parent_id foreign key(parent_id) references meeting(id),
    constraint fk_meeting_country_id foreign key(meeting_country_id) references meeting_country(id),

    createdAt timestamp not null default current_timestamp,
    updatedAt timestamp default current_timestamp on update current_timestamp
);