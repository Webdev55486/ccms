create table salescommission_country (
    id int not null primary key auto_increment,
    name varchar(255) not null
);

create table salescommission (
    id int not null primary key auto_increment,
    country_id int not null,

    date varchar(50),
    hospital varchar(150),
    items varchar(150),
    order_value int,
    kam varchar(255),
    assist varchar(255),
    paid_kam boolean,
    paid_assist boolean,

    key fk_country_id (country_id),
    constraint fk_country_id foreign key(country_id) references salescommission_country(id),

    createdAt timestamp not null default current_timestamp,
    updatedAt timestamp not null default current_timestamp on update current_timestamp
); 