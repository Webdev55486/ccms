ALTER TABLE film ADD created TIMESTAMP DEFAULT CURRENT_TIMESTAMP NOT NULL;
ALTER TABLE film ADD updated TIMESTAMP ON UPDATE CURRENT_TIMESTAMP DEFAULT CURRENT_TIMESTAMP NULL;
ALTER TABLE film CHANGE file file_hash VARCHAR(255);
ALTER TABLE film CHANGE is_hidden is_public TINYINT(1) NOT NULL DEFAULT '0';
ALTER TABLE film MODIFY views INT(11) UNSIGNED NOT NULL DEFAULT '0';
ALTER TABLE film MODIFY id INT(11) unsigned NOT NULL AUTO_INCREMENT;
ALTER TABLE film MODIFY views INT(11) UNSIGNED NOT NULL DEFAULT '0';
ALTER TABLE film DROP subtitle;

UPDATE film SET is_public = (CASE is_public WHEN 1 THEN 0 ELSE 1 END);

INSERT INTO employee_permission
(emp_id, permission, level)
SELECT
  id, 'libraries', 0
FROM
  employees;

CREATE TABLE film_languages
(
  id INT(11) UNSIGNED PRIMARY KEY NOT NULL AUTO_INCREMENT,
  film_id INT(11) UNSIGNED NOT NULL,
  language VARCHAR(2) NOT NULL,
  name VARCHAR(100),
  created TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
  updated TIMESTAMP ON UPDATE CURRENT_TIMESTAMP DEFAULT CURRENT_TIMESTAMP NOT NULL,
  CONSTRAINT film_languages_film_id_fk
  FOREIGN KEY (film_id) REFERENCES film (id) ON DELETE CASCADE ON UPDATE CASCADE
);

CREATE TABLE film_subject_languages
(
  id INT(11) UNSIGNED PRIMARY KEY NOT NULL AUTO_INCREMENT,
  film_subject_id INT(11) UNSIGNED NOT NULL,
  language VARCHAR(2) NOT NULL,
  name VARCHAR(100),
  created TIMESTAMP DEFAULT CURRENT_TIMESTAMP NOT NULL,
  updated TIMESTAMP ON UPDATE CURRENT_TIMESTAMP DEFAULT CURRENT_TIMESTAMP NOT NULL,
  CONSTRAINT film_subject_languages_film_subject_id_fk FOREIGN KEY (film_subject_id) REFERENCES film_subject (id) ON DELETE CASCADE ON UPDATE CASCADE
);

INSERT INTO film_languages (film_id, language, name)
    SELECT
      id,
      'en',
      name
    FROM
      film;

INSERT INTO film_subject_languages (film_subject_id, language, name)
    SELECT
      id,
      'en',
      name
    FROM
      film_subject;

ALTER TABLE film ADD hash VARCHAR(255) AFTER name;
ALTER TABLE film_subject ADD hash VARCHAR(255) AFTER name;

UPDATE film SET hash = (SELECT uuid());
UPDATE film_subject SET hash = (SELECT uuid());
