create table sms_stat (
  id int primary key auto_increment,
  entry_id int(11) not null,
  link_clicked tinyint default 0,
  key fk_entry_id (entry_id),
  constraint fk_entry_id foreign key(entry_id) references sms_entry (id) on delete cascade on update cascade
);
