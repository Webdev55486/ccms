ALTER TABLE prospects ADD colour VARCHAR(7) NULL;
ALTER TABLE prospects ADD reminder_text TEXT NULL;
ALTER TABLE prospects
  MODIFY COLUMN reminder_text TEXT AFTER reminder,
  MODIFY COLUMN colour VARCHAR(7) AFTER accountable;

ALTER TABLE prospects DROP contact_person;
ALTER TABLE prospects DROP contact_phone;
ALTER TABLE prospects DROP contact_email;
ALTER TABLE prospects DROP current_status;

CREATE TABLE contacts
(
  id INT(11) UNSIGNED PRIMARY KEY NOT NULL AUTO_INCREMENT,
  prospect_id INT(11) UNSIGNED,
  user_id INT(10) UNSIGNED,
  name VARCHAR(255),
  position VARCHAR(255),
  email VARCHAR(255),
  phone VARCHAR(255),
  updated TIMESTAMP ON UPDATE CURRENT_TIMESTAMP DEFAULT CURRENT_TIMESTAMP NOT NULL,
  created TIMESTAMP DEFAULT CURRENT_TIMESTAMP NOT NULL,
  CONSTRAINT contacts_prospects_id_fk FOREIGN KEY (prospect_id) REFERENCES prospects (id) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT contacts_users_id_fk FOREIGN KEY (user_id) REFERENCES users (id) ON DELETE CASCADE ON UPDATE CASCADE
);