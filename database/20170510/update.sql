CREATE TABLE articles
(
  id INT(11) UNSIGNED PRIMARY KEY NOT NULL AUTO_INCREMENT,
  published TINYINT(1) DEFAULT 1 NOT NULL,
  type VARCHAR(50) NOT NULL,
  language VARCHAR(2) NOT NULL,
  title VARCHAR(150),
  slug VARCHAR(150),
  description TEXT,
  blurb TEXT,
  text TEXT,
  thumbnail VARCHAR(255),
  header VARCHAR(255),
  created TIMESTAMP DEFAULT CURRENT_TIMESTAMP NOT NULL,
  updated TIMESTAMP ON UPDATE CURRENT_TIMESTAMP DEFAULT CURRENT_TIMESTAMP NOT NULL
) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

INSERT INTO articles (published, type, language, title, slug, description, blurb, text, thumbnail, created)
    SELECT n.is_published, 'news', ni.language, ni.title, ni.slug, ni.description, ni.blurb, ni.text, ni.image, n.published_date
      FROM news n
      INNER JOIN
        news_i18n ni ON n.id = ni.news_id;

DROP TABLE news_i18n;
DROP TABLE news;

INSERT INTO articles (published, type, language, title, slug, description, blurb, text, thumbnail, created)
  SELECT n.is_published, 'case-studies', ni.language, ni.title, ni.slug, ni.description, ni.blurb, ni.text, ni.image, n.published_date
  FROM cases n
    INNER JOIN
    cases_i18n ni ON n.id = ni.case_id;

DROP TABLE cases_i18n;
DROP TABLE cases;

UPDATE articles SET header = REPLACE(thumbnail, 'thumb_', 'header_')