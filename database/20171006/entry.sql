create table sms_entry(
    id int not null primary key auto_increment,
    receiver_id int, 
    message_id int not null,
    user_id int(10) unsigned not null,

    time time not null,
    days varchar(40) not null default "[1,1,1,1,1,1,1]",
    is_template boolean not null default false,
    template_label varchar(100),

    key fk_receiver_id (receiver_id),
    constraint fk_receiver_id foreign key (receiver_id) references sms_receiver(id) on update cascade on delete cascade,

    key fk_message_id (message_id),
    constraint fk_message_id foreign key (message_id) references sms_message(id) on update cascade on delete cascade,

    key fk_entry_user_id (user_id),
    constraint fk_entry_user_id foreign key (user_id) references users(id) on update cascade on delete cascade,

    created_at timestamp default current_timestamp,
    updated_at timestamp default current_timestamp on update current_timestamp
);