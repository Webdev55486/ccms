create table sms_message(
    id int not null primary key auto_increment, 
    user_id int(10) unsigned not null,

    label varchar(100) not null,
    content text not null,

    key fk_user_id (user_id),
    constraint fk_user_id foreign key(user_id) references users(id) on update cascade on delete cascade,

    created_at timestamp default current_timestamp,
    updated_at timestamp default current_timestamp on update current_timestamp
);