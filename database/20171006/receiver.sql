create table sms_receiver(
    id int not null primary key auto_increment, 
    user_id int(10) unsigned not null,

    firstname varchar(100) not null,
    lastname varchar(100) not null,
    phonenumber varchar(40) not null,

    startdate date not null,
    enddate date not null,

    key fk_receiver_user_id (user_id),
    constraint fk_receiver_user_id foreign key(user_id) references users(id) on update cascade on delete cascade,

    created_at timestamp default current_timestamp,
    updated_at timestamp default current_timestamp on update current_timestamp 
);