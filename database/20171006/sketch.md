# Sms reminder usecase

## Flow 
1. User creates text message to be sent
2. User creates atleast 1 template (Optional)
3. User creates atleast 1 receiver from template or from scratch
4. User commits receiver 

| Receiver |  | | |
| -- | -- | -- | -- |
| **name** | **type** | **constraints** | **example** |
| id | int | primary key, AI | 1 |
| user_id | int | foreign key, not null | 5 |
| firstname | varchar(100) | not null | "Andreas" |
| lastname | varchar(100) | not null | "Malmqvist" |
| phonenumber | varchar(40) | not null | "+46763168232"|
| start_date | date | not null | 20171020 |
| end_date | date | not null | 20171120 |

| Message |  | | |
| -- | -- | -- | -- |
| **name** | **type** | **constraints** | **example** |
| id | int | primary key, AI | 1 |
| user_id | int | foreign key, not null | 5 |
| label | varchar(100) | not null | "First text" | 
| content | text | not null | "Hello world" | 

| Entry |  | | |
| -- | -- | -- | -- |
| **name** | **type** | **constraints** | **example** |
| id | int | primary key, AI | 1 |
| receiver_id | int | foreign key | 2 |
| message_id | int | foreign key, not null | 3 |
| user_id | int | foreign key, not null | 5 |
| time | time | not null | 13:00 |
| days | varchar(40) | not null, default -> | "[1,1,1,1,1,1,1]" |
| is_template | boolean | not null, default -> | false |
| template_label | varchar(100) | | NULL |
