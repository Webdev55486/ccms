ALTER TABLE countries MODIFY active TINYINT(1) NOT NULL DEFAULT 1;
ALTER TABLE country_region MODIFY active TINYINT(1) NOT NULL DEFAULT 1;
ALTER TABLE prospects MODIFY active TINYINT(1) NOT NULL DEFAULT 1;