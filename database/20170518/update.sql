# Tablet
create table tablet (
  id int(10) unsigned primary key auto_increment not null,
  user_id int(10) unsigned  not null,
  tablet_name varchar(50) not null,
  token varchar(50) not null,
  createdAt datetime default current_timestamp,
  updatedAt datetime on update current_timestamp,
  foreign key(user_id) references users(id)
);

# Manifest
create table manifest (
  id int(10) unsigned primary key auto_increment not null,
  tablet_id int(10) unsigned not null,
  version int(10) unsigned not null default 1,
  createdAt datetime default current_timestamp,
  updatedAt datetime on update current_timestamp,
  foreign key(tablet_id) references tablet(id)
);

# Manifest subscriptions
create table manifest_subscription (
  id int(10) unsigned primary key auto_increment not null,
  manifest_id int(10) unsigned not null,
  subscription_id int(10) unsigned not null,
  createdAt datetime default current_timestamp,
  updatedAt datetime on update current_timestamp,
  foreign key(manifest_id) references manifest(id),
  foreign key(subscription_id) references subscriptions(id)
);