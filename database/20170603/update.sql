ALTER TABLE prospects ADD user_id INT(11) UNSIGNED NULL;
ALTER TABLE prospects
  ADD CONSTRAINT prospects_users_id_fk
FOREIGN KEY (user_id) REFERENCES users (id) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE prospects
  MODIFY COLUMN user_id INT(11) UNSIGNED AFTER id;
ALTER TABLE prospects MODIFY country_region INT(11) unsigned;

ALTER TABLE contacts DROP FOREIGN KEY contacts_users_id_fk;
DROP INDEX contacts_users_id_fk ON contacts;
ALTER TABLE contacts DROP user_id;

ALTER TABLE addresses DROP FOREIGN KEY addresses_users_id_fk;
DROP INDEX addresses_users_id_fk ON addresses;
ALTER TABLE addresses DROP user_id;

ALTER TABLE prospects MODIFY country_region INT(11) unsigned;

INSERT INTO
  prospects (user_id, hospital, vat_number)
SELECT
  id, company_name, business_code
FROM
  users;

ALTER TABLE users DROP business_code;

INSERT INTO
  contacts (prospect_id, name, email, phone_first)
SELECT
  pro.id,
  u.name,
  u.email,
  u.phone
FROM
  users u
INNER JOIN
  prospects pro ON u.id = pro.user_id;

ALTER TABLE users DROP email;
ALTER TABLE users DROP name;
ALTER TABLE users DROP phone;

INSERT INTO
  addresses(prospect_id, type, company_first, address_first, city, postal_code, country)
SELECT
  pro.id,
  'billing',
  u.company_name,
  u.street_address,
  u.city,
  u.postcode,
  u.country
FROM
  users u
INNER JOIN
  prospects pro ON u.id = pro.user_id;

INSERT INTO
  addresses(prospect_id, type, company_first, address_first, city, postal_code, country)
  SELECT
    pro.id,
    'visiting',
    u.company_name,
    u.street_address,
    u.city,
    u.postcode,
    u.country
  FROM
    users u
    INNER JOIN
    prospects pro ON u.id = pro.user_id;

ALTER TABLE users DROP street_address;
ALTER TABLE users DROP postcode;
ALTER TABLE users DROP city;
ALTER TABLE users DROP country;
ALTER TABLE users DROP company_name;

ALTER TABLE users ALTER COLUMN website_language SET DEFAULT 'se';
ALTER TABLE users ALTER COLUMN film_language SET DEFAULT 'se';
ALTER TABLE users ALTER COLUMN email_language SET DEFAULT 'se';