ALTER TABLE prospects ADD regional_office TINYINT(1) DEFAULT 0 NOT NULL;
ALTER TABLE prospects
  MODIFY COLUMN regional_office TINYINT(1) NOT NULL DEFAULT 0 AFTER colour;

ALTER TABLE prospects MODIFY clinic VARCHAR(255);
ALTER TABLE prospects MODIFY category INT(10) unsigned;

ALTER TABLE prospects ADD ref_number VARCHAR(50) NULL;
ALTER TABLE prospects
  MODIFY COLUMN ref_number VARCHAR(50) AFTER main_contact;