ALTER TABLE prospects ADD main_contact VARCHAR(255) NULL;
ALTER TABLE prospects ADD vat_number VARCHAR(50) NULL;

ALTER TABLE prospects
  MODIFY COLUMN main_contact VARCHAR(255) AFTER accountable,
  MODIFY COLUMN vat_number VARCHAR(50) AFTER main_contact;

CREATE TABLE addresses
(
  id INT(11) UNSIGNED PRIMARY KEY NOT NULL AUTO_INCREMENT,
  prospect_id INT(11) UNSIGNED,
  user_id INT(11) UNSIGNED,
  type VARCHAR(30) NOT NULL,
  company_first VARCHAR(100),
  company_second VARCHAR(100),
  address_first VARCHAR(100),
  address_second VARCHAR(100),
  city VARCHAR(100),
  postal_code VARCHAR(100),
  state VARCHAR(100),
  country VARCHAR(100),
  updated TIMESTAMP ON UPDATE CURRENT_TIMESTAMP DEFAULT CURRENT_TIMESTAMP NOT NULL,
  created TIMESTAMP DEFAULT CURRENT_TIMESTAMP NOT NULL,
  CONSTRAINT addresses_prospects_id_fk FOREIGN KEY (prospect_id) REFERENCES prospects (id) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT addresses_users_id_fk FOREIGN KEY (user_id) REFERENCES users (id) ON DELETE CASCADE ON UPDATE CASCADE
);

ALTER TABLE prospects ADD phone_second VARCHAR(40) NULL;
ALTER TABLE prospects ADD phone_first VARCHAR(40) NULL;
ALTER TABLE prospects
  MODIFY COLUMN created TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP AFTER phone_second,
  MODIFY COLUMN updated TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP AFTER phone_second,
  MODIFY COLUMN active TINYINT(1) NOT NULL DEFAULT '1' AFTER phone_second;

ALTER TABLE contacts ADD phone_second VARCHAR(100) NULL;
ALTER TABLE contacts CHANGE phone phone_first VARCHAR(100);
ALTER TABLE contacts
  MODIFY COLUMN phone_second VARCHAR(100) AFTER phone_first;

ALTER TABLE prospects ADD reminder_time VARCHAR(10) NULL;
ALTER TABLE prospects
  MODIFY COLUMN reminder_time VARCHAR(10) AFTER reminder;