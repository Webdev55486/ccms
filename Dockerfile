FROM scratch

EXPOSE 80

# Make dir for keys
COPY empty /keys

# Make dir for uploads (all film, thumbnail and subtitles locations)
COPY empty /uploads

# Make dir for images (headers and thumbnails for case studies and news)
COPY empty /images
COPY empty /images/casestudies
COPY empty /images/news

# Main binary
COPY ccms /ccms

# Dependency binaries
COPY wkhtmltopdf /wkhtmltopdf

COPY templates/ templates/
COPY email/ email/
COPY resources/ resources/
COPY config/config.toml config/config.toml

ADD zoneinfo.tar.gz /
ADD ca-certificates.crt /etc/ssl/certs/

CMD ["./ccms"]