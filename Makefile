.PHONY: default build-local build push

default: 
	make build

build:
	CGO_ENABLED=0 GOOS=linux go build -a -installsuffix cgo -o ccms .
	npm run build
	docker build -t registry.medfilm.se/ccms:latest .

build-local:
	CGO_ENABLED=0 GOOS=linux go build -a -installsuffix cgo -o ccms .
	docker build -t ccms:latest .

push:
	docker push registry.medfilm.se/ccms:latest
