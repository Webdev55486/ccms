/**
 * medfilm-ccms
 * Copyright (C) Sunstate AB - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Simon Wikstrand <simon@sunstate.se>, 2016-12-12
 */

package main

import (
	"fmt"
	"net/http"

	"github.com/MedFilm/ccms/lib/api/sms"
	"github.com/MedFilm/ccms/lib/authentication"
	"github.com/MedFilm/ccms/lib/config"
	"github.com/MedFilm/ccms/lib/database"
	"github.com/MedFilm/ccms/lib/email"
	"github.com/MedFilm/ccms/lib/routes"
	"github.com/MedFilm/ccms/lib/tools"
	"github.com/gorilla/mux"
	"github.com/urfave/negroni"
)

func main() {

	// Generate configuration
	err := config.NewConfig()

	if err != nil {
		fmt.Println(err)
		return
	}

	fmt.Println("Configuration Imported Successfully")

	// Start a generic database pool
	err = database.StartDriver()

	if err != nil {
		fmt.Println(err)
		return
	}

	fmt.Println("Database connection established")

	err = authentication.InitKeys()

	if err != nil {
		fmt.Println(err)
		return
	}

	fmt.Println("Loaded private and public keys")

	r := mux.NewRouter()
	routes.ApiRoutes(r)
	routes.Routes(r)

	n := negroni.Classic()

	// Recovery, send 500 if something goes wrong
	n.Use(negroni.NewRecovery())

	// Add headers to request
	n.Use(negroni.HandlerFunc(tools.InterceptHeaders))

	// Use Gorilla Mux routes
	n.UseHandler(r)

	email.InitPostmark()

	// Initiate a cron job that runs daily
	email.InitCron()
	sms.InitCron()

	http.ListenAndServe(config.BuiltConfig.Application.Publish, n)
}
