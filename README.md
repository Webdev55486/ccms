# MedFilm CCMS

This project consist of MedFilms Customer & Content Management System. It's built with Vue.js, Golang and uses MySQL as database.
The frontend is all built with JavaScript, HTML and SCSS and is rendered with Javascript. The front-end will talk
with the back-end via an API built in Golang. Once sources are compiled you can deploy this software with Docker.

[Project Documentation](https://github.com/MedFilm/ccms/tree/master/docs)

Requirements: Node.js, NPM, Golang

Original build versions Node v7.3.0, NPM 3.10.10, Go 1.8beta2

## Initial Setup
```bash
# compile glide
go get github.com/Masterminds/glide

# install dependencies
npm install
glide install

# Make a copy of config template and edit it's content
cp config/config.template.toml config/config.toml

# Create directories required for dynamic content
mkdir images
mkdir keys
mkdir uploads
```

## Build Setup

```bash
# serve with hot reload at localhost:8080
npm run dev

# build and run api server
go build
./ccms

# build for production with minification
npm run build

# run unit tests
npm run unit

# run all tests
npm test

# build docker container
## Compile static binary in go
CGO_ENABLED=0 GOOS=linux go build -a -installsuffix cgo -o ccms .
## Build docker container with static binary
docker build -t registry.medfilm.se/ccms:tag .

# push docker container
docker push registry.medfilm.se/ccms:tag
```

#### Deployment References
[docker](https://docs.docker.com/)

#### Frontend References
[Vue.js](http://vuejs.org/)

[Sizzle](https://sizzlejs.com/)

[Velocityjs](http://velocityjs.org/)

#### API References
[Gorilla Mux](https://github.com/gorilla/mux)

[Unrolled Render](https://github.com/unrolled/render)

[Urfave Negroni](https://github.com/urfave/negroni)

### Migration Notes breaking changes
1. Do breaking SQL migrations
2. Move images/news and images/casestudies to images/article and change all names to have header_ and thumb_