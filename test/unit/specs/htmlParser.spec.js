// import Vue from 'vue'
import htmlParser from '../../../app/lib/htmlParser'

suite('htmlParser', () => {
  test('happypath', () => {
    const root = document.createElement('div')
    const tree = htmlParser.methods.from(root)
    const output = '<DIV classname="" style=""></DIV>'
    
    expect(typeof tree).toBe('string')
    expect(tree).toBe(output)
  })
})