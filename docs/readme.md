# Project Documentation

SQL query for getting a list of prospects, addresses and contacts
```mysql
SELECT
  pro.id as 'id',
  adr.id as 'adrId',
  pro.hospital as 'Sjukhus',
  pro.clinic as 'Klinik',
  adr.company_first as 'Företagsnamn',
  adr.address_first as 'Address',
  adr.city as 'Stad',
  adr.postal_code as 'Postkod',
  cr.name as 'Region',
  con.name as 'Kontaktnamn',
  con.position as 'Position',
  con.phone_first as 'Telefon 1',
  con.phone_second as 'Telefon 2'
FROM
  (
    SELECT
      adr1.id,
      adr1.company_first,
      adr1.address_first,
      adr1.city,
      adr1.postal_code,
      adr1.state,
      adr1.prospect_id
    FROM
      addresses adr1
    LEFT JOIN
      addresses adr2 ON (adr1.prospect_id = adr2.prospect_id AND adr1.id < adr2.id)
    WHERE
      adr1.type = 'visiting' AND adr2.id IS NULL
  )
as adr
RIGHT OUTER JOIN
  (
    SELECT
      *
    FROM
      prospects
    WHERE active = 1
  )
  as pro ON adr.prospect_id = pro.id
LEFT OUTER JOIN
    contacts as con ON pro.id = con.prospect_id
LEFT JOIN
    country_region cr ON pro.country_region = cr.id
INTO OUTFILE '/var/lib/mysql-files/file.csv'
    CHARACTER SET utf8
    FIELDS TERMINATED BY ','
```